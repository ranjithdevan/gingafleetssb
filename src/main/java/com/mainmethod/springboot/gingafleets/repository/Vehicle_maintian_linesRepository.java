package com.mainmethod.springboot.gingafleets.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mainmethod.springboot.gingafleets.model.Vehicle_maintain_lines;
@Repository
public interface Vehicle_maintian_linesRepository extends JpaRepository<Vehicle_maintain_lines, Integer> {

}
