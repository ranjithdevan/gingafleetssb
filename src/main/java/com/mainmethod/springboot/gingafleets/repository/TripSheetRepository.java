package com.mainmethod.springboot.gingafleets.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.mainmethod.springboot.gingafleets.model.Cust_selection;
import com.mainmethod.springboot.gingafleets.model.Load_manifest;



@Service
@Repository
public interface TripSheetRepository extends JpaRepository<Load_manifest, Integer> {
	
	@Query("SELECT e FROM Load_manifest e where  e.company_code = 'A000198'")
	List<Load_manifest> findByCompany();
	
	/*@Query(value = "SELECT * FROM load_manifest WHERE manifest_date BETWEEN :date1 AND :date2 And company_code='A000198' ", nativeQuery=true)
	public List<Load_manifest> findingByDate(@Param("date1") String date1, @Param("date2") String date2);
	
	@Query("SELECT e FROM load_manifest e WHERE e.vehicle_no=:vehicle_no")
	public List<Load_manifest> findingByVehicleNOAndtype(@Param("vehicle_no") String vehicle_no);*/
  
	
	
	//@Query(value = "SELECT * FROM load_manifest WHERE manifest_date BETWEEN :date1 AND :date2 ", nativeQuery=true)
//	@Query("SELECT e FROM Load_manifest e where  e.company_code = 'A000198'")
	//public List<Load_manifest> findingByDate();
}
