package com.mainmethod.springboot.gingafleets.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mainmethod.springboot.gingafleets.model.Load_manifest;
import com.mainmethod.springboot.gingafleets.model.Vehicle;


@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, Integer> {
	
	@Query("SELECT e FROM Vehicle e where  e.company_code = 'A000198'")
	List<Vehicle> findByCompany();
	
	@Query("SELECT e FROM Vehicle e where  e.company_code=:company_code And e.vehicle_no = :vehicle_no")
	public Vehicle findingbycompanyCodeandvehicle_NO(@Param("company_code") String company_code,@Param("vehicle_no") String vehicle_no);

}
