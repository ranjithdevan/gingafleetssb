package com.mainmethod.springboot.gingafleets.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mainmethod.springboot.gingafleets.model.LoginBean;

@Repository
public interface LoginRepository extends JpaRepository<LoginBean, Integer> {
	


	@Query(value="select l from LoginBean l where l.username=:username and l.password=:password and l.activatestatus='Activate'")
	List<LoginBean> findByUserNameAndPassword(String username, String password);

}
