package com.mainmethod.springboot.gingafleets.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mainmethod.springboot.gingafleets.model.Cust_selection;

@Repository
public interface SelectionRetireveRepository extends JpaRepository<Cust_selection, Integer> {

	@Query("SELECT e FROM Cust_selection e where  e.company_code = 'A000198'")
	List<Cust_selection> findBycompany();
	
	
	
	@Query("SELECT e FROM Cust_selection e where e.group_name = :group_name And e.company_code=:company_code And e.sub_group = :sub_group And e.svalue = :svalue")
	public Cust_selection findingByGroupNameCompanyCodeAndSubgropAndSvalue(@Param("group_name")String group_name,@Param("company_code") String company_code,@Param("sub_group") String sub_group,@Param("svalue") String svalue);
     
	
}
