package com.mainmethod.springboot.gingafleets.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.mainmethod.springboot.gingafleets.dto.Cust_selectionField;


@Repository
public interface SelectionFieldRepository extends JpaRepository<Cust_selectionField, Integer>{
   
	@Query(value = "SELECT * FROM live_saas_design.form_template where menu_id=1009", nativeQuery = true)
	List<Cust_selectionField> findAllByMenuId();
}
