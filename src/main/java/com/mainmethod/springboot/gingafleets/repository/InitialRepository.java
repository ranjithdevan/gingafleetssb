package com.mainmethod.springboot.gingafleets.repository;





import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


import com.mainmethod.springboot.gingafleets.model.Initial;


@Repository
public interface InitialRepository extends JpaRepository<Initial, Integer>{
	
	@Query("SELECT e FROM Initial e where e.type = :type And e.company_code=:company_code")
	public Initial findtypeAndcompanycode(@Param("type")String type,@Param("company_code") String company_code);
	
	public Initial findById(int id);
}
