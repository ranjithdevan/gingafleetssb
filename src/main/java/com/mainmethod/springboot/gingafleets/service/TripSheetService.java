package com.mainmethod.springboot.gingafleets.service;

import java.util.List;
import java.util.Optional;

import com.mainmethod.springboot.gingafleets.model.Load_manifest;


public interface TripSheetService {
 public List<Load_manifest> findByCompany();
	 
 Load_manifest saveTripSheetMaster(Load_manifest load_manifest);
	 
 Optional<Load_manifest> getSelectionMasterById(int id);
	 
 Load_manifest updateSelectionMaster(Load_manifest load_manifest);
 
 void delete(int id);

}
