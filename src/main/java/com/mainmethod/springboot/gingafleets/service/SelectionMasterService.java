package com.mainmethod.springboot.gingafleets.service;


import java.util.List;
import java.util.Optional;

import com.mainmethod.springboot.gingafleets.common.all.SubGroupAlreadyExistsException;
import com.mainmethod.springboot.gingafleets.model.Cust_selection;


public interface SelectionMasterService {
	
	
	 public List<Cust_selection> getall();
	 
	 Cust_selection saveSelectionMaster(Cust_selection cust_selection) throws SubGroupAlreadyExistsException;
	 
	 Optional<Cust_selection> getSelectionMasterById(int id);
	 
	 Cust_selection updateSelectionMaster(Cust_selection cust_selection);
	
	 void delete(int id);
}
