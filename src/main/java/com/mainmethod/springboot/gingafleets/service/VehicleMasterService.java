package com.mainmethod.springboot.gingafleets.service;

import java.util.List;
import java.util.Optional;

import com.mainmethod.springboot.gingafleets.common.all.SubGroupAlreadyExistsException;
import com.mainmethod.springboot.gingafleets.model.Vehicle;

public interface VehicleMasterService {
	
	 public List<Vehicle> getall();
	 
	 Vehicle saveVehicleMaster(Vehicle vehicle) throws SubGroupAlreadyExistsException;
	 
 Optional<Vehicle> getSelectionMasterById(int id);
	 
 Vehicle updateSelectionMaster(Vehicle vehicle);
 
 void delete(int id);

}
