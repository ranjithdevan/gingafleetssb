package com.mainmethod.springboot.gingafleets.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mainmethod.springboot.gingafleets.model.LoginBean;

import com.mainmethod.springboot.gingafleets.repository.LoginRepository;

//import com.mainmethod.springboot.Employee;
//login service
@Service
public class LoginService {
	
	@Autowired
	LoginRepository loginRepository;
	
	public List<LoginBean> checUser(String username,  String password){
		return loginRepository.findByUserNameAndPassword(username, password);
	}
	
	
	
	//loginDAO.save(Login login);
	
	
	
	//@Autowired
	//private PasswordEncoder bcryptEncoder;
	
//	public Login checUser(String username,  String password){
	//	if (loginDAO== null) {
		//	throw new UsernameNotFoundException("User not found with username: " + username);
		//}
	//	return loginDAO.findByUserNameAndPassword(username, password);
	//}
	

}

