package com.mainmethod.springboot.gingafleets.service;

import java.io.FileNotFoundException;

import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JRException;

public interface TripSheetReportService {
	
	public String exportReport(HttpServletResponse response,String format) throws FileNotFoundException, JRException;
}
