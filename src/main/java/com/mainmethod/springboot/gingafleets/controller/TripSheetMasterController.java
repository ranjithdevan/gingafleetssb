package com.mainmethod.springboot.gingafleets.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.mainmethod.springboot.gingafleets.model.Load_manifest;

import com.mainmethod.springboot.gingafleets.servicelmpl.TripSheetServiceImpl;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/TripSheet")
public class TripSheetMasterController {
	
	@Autowired
	TripSheetServiceImpl tripSheetServiceImpl;
	
	
	
	@PostMapping(value = "/getallRecords")
	public ResponseEntity<LinkedHashMap>  getselectionfield() {
		
      List<Load_manifest> list =tripSheetServiceImpl.findByCompany();
      ArrayList<Object> TripSheetTable = new ArrayList<Object>();
      for(int i=0;i<list.size();i++) {
    	  LinkedHashMap selectionObject =new LinkedHashMap();
	    	selectionObject.put("TripSheetCode", list.get(i).getManifest_code());
	    	selectionObject.put("TripSheetNo", list.get(i).getManifest_alias());
	    	selectionObject.put("Date", list.get(i).getManifest_date());
	    	selectionObject.put("VehicleType", list.get(i).getVehilce_type());
	    	selectionObject.put("VehicleNo", list.get(i).getVehicle_no());
	    	selectionObject.put("ConsigneeName", list.get(i).getConsignee_name());
	    	selectionObject.put("Material", list.get(i).getMaterial_name());
	    	selectionObject.put("ID", list.get(i).getId());
	    	TripSheetTable.add(selectionObject);
      }
      LinkedHashMap tableData =new LinkedHashMap();
	    ArrayList<String> tableHeading = new ArrayList<String>();
	    tableHeading.add("TripSheetCode");
	    tableHeading.add("TripSheetNo");
	    tableHeading.add("Date");
	    tableHeading.add("VehicleType");
	    tableHeading.add("VehicleNo");
	    tableHeading.add("ConsigneeName");
	    tableHeading.add("Material");
	    tableHeading.add(" ");
	    tableHeading.add(" ");
	    tableData.put("tableHeading", tableHeading);
	    tableData.put("tableData", TripSheetTable);
    return ResponseEntity.ok(tableData);
     
  
	}
	
	@PostMapping("/CreateTripSheetSelection")
	public Load_manifest create(@RequestBody Load_manifest load_manifest) {
		Load_manifest saveVehicleMasterDetails= tripSheetServiceImpl.saveTripSheetMaster(load_manifest);
		return saveVehicleMasterDetails;
	}
	
	@PutMapping("/updateTripSheetmaster")
	public Load_manifest update(@RequestBody Load_manifest load_manifest) {
		Load_manifest updateVehicleMasterDetails =	tripSheetServiceImpl.updateSelectionMaster(load_manifest);
			return updateVehicleMasterDetails;	
	}
	
	@DeleteMapping("/delete/{id}")
	public void delete(@PathVariable("id") int id) {
		tripSheetServiceImpl.delete(id);
     
	}

}
