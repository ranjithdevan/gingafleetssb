package com.mainmethod.springboot.gingafleets.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mainmethod.springboot.gingafleets.common.frontdata.ReportServiceImpl;
import com.mainmethod.springboot.gingafleets.common.json.Json;
import com.mainmethod.springboot.gingafleets.dto.Cust_selectionField;
import com.mainmethod.springboot.gingafleets.dto.Form_template_report;
import com.mainmethod.springboot.gingafleets.jdbc.DataSourceConfig;
import com.mainmethod.springboot.gingafleets.servicelmpl.angularFormlyServiceImpl;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/ReportsType")
public class ReportFieldController {
	ReportServiceImpl repo= new ReportServiceImpl();
	
	@Autowired
	private DataSourceConfig dataSourceConfig;
	private angularFormlyServiceImpl angular =new angularFormlyServiceImpl();
	
	

	  @SuppressWarnings("unchecked")
	   @PostMapping(value = "/Fields/{id}")
		public ResponseEntity<?> getReport(@PathVariable("id") int id,@RequestBody ArrayList<String> Company) {
		  DataSource dataSource=dataSourceConfig.connectDataSource();
		  
		 
		  repo.setDataSource(dataSource);
		  System.out.println("id----->"+id);
		  List<Form_template_report> lists = repo.selectAllReport(id);
		  List<Form_template_report> fieldList = lists.stream().filter(value ->{
		    	
		    	// System.out.println("listsss---->"+lists);
	    	   
	    	   return  value.getField_name().equalsIgnoreCase("vehicle_no") ||
	    			   value.getField_name().equalsIgnoreCase("supplier_name") || 
	    			   value.getField_name().equalsIgnoreCase("ac_head_name") || 
	    			   value.getField_name().equalsIgnoreCase("cust_name")|| 
	    			   value.getField_name().equalsIgnoreCase("vehicle_type")||
	    			   value.getField_name().equalsIgnoreCase("datefrom")||
	    			   value.getField_name().equalsIgnoreCase("dateto")||   
	    			   value.getField_name().equalsIgnoreCase("dateto")||
	    			   value.getField_name().equalsIgnoreCase("dateto")||
	    			  value.getField_name().equalsIgnoreCase("dateto")|| 
	    			  value.getField_name().equalsIgnoreCase("center_name");
	    			
	       }).collect(Collectors.toList());
	       @SuppressWarnings("rawtypes")
			LinkedHashMap urls =new LinkedHashMap();
	       urls.put("fields", angular.buildFormlyStructure(fieldList,Company,dataSource));
	       LinkedHashMap<String, String> url =new LinkedHashMap<String, String>();
	       url.put("Print", "Reports/TripsheetReport");
			
			urls.put("url", url);
	      

	  
			return ResponseEntity.ok(urls);
		
	  
	  }	    
}
