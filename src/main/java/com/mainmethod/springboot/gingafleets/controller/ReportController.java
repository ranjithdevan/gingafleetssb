package com.mainmethod.springboot.gingafleets.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.query.Param;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mainmethod.springboot.gingafleets.dto.TripsheetReportDto;
import com.mainmethod.springboot.gingafleets.model.Cust_selection;
import com.mainmethod.springboot.gingafleets.model.Load_manifest;
import com.mainmethod.springboot.gingafleets.repository.TripSheetRepository;
import com.mainmethod.springboot.gingafleets.servicelmpl.TripSheetServiceImpl;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/Reports")
public class ReportController {
	
	@Autowired
	TripSheetRepository  tripSheetRepository;
	
	 @GetMapping("/TripsheetReport")
	 private void createPdfReport(HttpServletResponse response) throws JRException, IOException {
	 //private void createPdfReport(HttpServletResponse response,@RequestBody Load_manifest load_manifest) throws JRException {
		 System.out.println("check the query");
	 List<Load_manifest> findby=tripSheetRepository.findByCompany();
	//	 List<Load_manifest> findby=tripSheetRepository.findingByVehicleNOAndtype(load_manifest.getVehicle_no());
	       if(findby !=null) {
	    	   
	    	System.out.println("inside if");
	        InputStream stream = this.getClass().getResourceAsStream("/TripReportsSheetRegister.jrxml");      
	        JasperReport report = JasperCompileManager.compileReport(stream);      
	        JRBeanCollectionDataSource source = new JRBeanCollectionDataSource(findby);
	         Map<String, Object> parameters = new HashMap<>();
	         System.out.println("success");
	       JasperPrint print = JasperFillManager.fillReport(report, parameters, source);
	        String filePath = "E:\\Jasper Report Download\\";
	        // Export the report to a PDF file.
	       JasperExportManager.exportReportToPdfFile(print, filePath + "TripSheetReport.pdf");
	        JasperExportManager.exportReportToPdfStream(print, response.getOutputStream());
	        response.setContentType("application/pdf");
	        response.addHeader("Content-Disposition", "inline; filename=jasper.pdf;");
	      //  System.out.println("viewed in chrome page");
	       }
	    }

}
