package com.mainmethod.springboot.gingafleets.controller;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mainmethod.springboot.gingafleets.common.language.LanguageTranslateImpl;
import com.mainmethod.springboot.gingafleets.dto.Cust_selectionField;
import com.mainmethod.springboot.gingafleets.jdbc.DataSourceConfig;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/Language")
public class LanguageController {
	
	@Autowired
	private DataSourceConfig dataSourceConfig;
	
	LanguageTranslateImpl dao = new LanguageTranslateImpl();
	
	
	@PostMapping(value = "Translate/{languageCode}")
	public ResponseEntity<?> getselectionfield(@PathVariable("languageCode") String languageCode) {
System.out.println("language code-----"+languageCode);
		DataSource dataSource=dataSourceConfig.connectDataSource();
       dao.setDataSource(dataSource);
       ArrayList<Object> list = dao.selectLanguage(languageCode);
       return ResponseEntity.ok(list); 
}


	
}

	
