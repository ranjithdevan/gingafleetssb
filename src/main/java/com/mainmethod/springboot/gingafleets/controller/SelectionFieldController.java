package com.mainmethod.springboot.gingafleets.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;


import javax.sql.DataSource;


import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;



import com.mainmethod.springboot.gingafleets.common.frontdata.CustFieldsDao;

import com.mainmethod.springboot.gingafleets.common.json.Json;
import com.mainmethod.springboot.gingafleets.dto.Branchdetails;
import com.mainmethod.springboot.gingafleets.dto.Cust_selectionField;
import com.mainmethod.springboot.gingafleets.dto.Selection;
import com.mainmethod.springboot.gingafleets.jdbc.DataSourceConfig;
import com.mainmethod.springboot.gingafleets.model.Cust_selection;
import com.mainmethod.springboot.gingafleets.servicelmpl.angularFormlyServiceImpl;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/AllMasters")
public class SelectionFieldController {
	
	
	CustFieldsDao dao = new CustFieldsDao();
	
	
	 JSONObject jsonObject= new JSONObject();
	
	
	@Autowired
	private DataSourceConfig dataSourceConfig;
	private angularFormlyServiceImpl angular =new angularFormlyServiceImpl();
	
	@Autowired
	Json jSON;

	  @SuppressWarnings("unchecked")
	@PostMapping(value = "/Fields/{id}")
<<<<<<< HEAD
		public ResponseEntity<LinkedHashMap> getselectionfield(@PathVariable("id") int id) {
>>>>>>> refs/remotes/origin/test
=======
		public ResponseEntity<?> getselectionfield(@PathVariable("id") int id) {

>>>>>>> branch 'master' of https://gitlab.com/ranjithdevan/gingafleetssb.git
			DataSource dataSource=dataSourceConfig.connectDataSource();
	       dao.setDataSource(dataSource);
	       List<Cust_selectionField> list = dao.selectAll(id);
	       ArrayList<String> combobox= null;   
	       List<Cust_selectionField> fieldList = list.stream().filter(value ->{
	    	 
	    	   return value.getField_name().equals("group_name")||value.getField_name().equals("sub_group")||value.getField_name().equals("svalue")|| value.getField_type().equals("panel");
			
	       }).collect(Collectors.toList());
	    

	       @SuppressWarnings("rawtypes")
		LinkedHashMap selectionMaster =new LinkedHashMap();
	       selectionMaster.put("fields", angular.buildFormlyStructure(fieldList,combobox,dataSource));
		LinkedHashMap<String, String> url =new LinkedHashMap<String, String>();
		url.put("table", "SelectionMaster/getallRecords");
		url.put("save", "SelectionMaster/CreateSelection");
		url.put("update", "SelectionMaster/updateSelectionmaster");
		url.put("delete", "SelectionMaster/delete/{id}");
		selectionMaster.put("url", url);
			return ResponseEntity.ok(selectionMaster);
<<<<<<< HEAD
>>>>>>> refs/remotes/origin/test
=======

>>>>>>> branch 'master' of https://gitlab.com/ranjithdevan/gingafleetssb.git
		}
	
	@PostMapping(value = "/Query")
	public ResponseEntity<LinkedHashMap> getQuerys(@RequestBody List<String> name) {
		
		DataSource dataSource=dataSourceConfig.connectDataSource();
		dao.setDataSource(dataSource);
      List<Selection> rule = dao.selectAllRules(name);
      Iterator<Selection> iterator = rule.iterator();
      List<Object> list=new ArrayList<Object>();
      LinkedHashMap<String,String> dropdownData;
      List<String> heading=new ArrayList<>();
      heading.add("Group");
      LinkedHashMap groupNameAutocompleteData = new LinkedHashMap();
      List<Object> lists=new ArrayList<Object>();
    
      for(Selection cc: rule) {
    	  dropdownData = new LinkedHashMap<String,String>();
    	  dropdownData.put("Group", cc.getSub_group());
    	 
    	  dropdownData.put("optionDisplayValue", cc.getSub_group());
    	  lists.add(dropdownData);
      }
      
      groupNameAutocompleteData.put("heading", heading);
      groupNameAutocompleteData.put("data", lists);
      return ResponseEntity.ok(groupNameAutocompleteData);
	}

	@PostMapping(value = "/SubgroupDropdown")
	public ResponseEntity<?> getQuery(@RequestBody List<String> svalue) {
		//System.out.println("checking the value after getting in array"+svalue);
		DataSource dataSource=dataSourceConfig.connectDataSource();
		dao.setDataSource(dataSource);
		List<Selection> rule = dao.selectSubGroupDropDown(svalue);
     Iterator<Selection> iterator = rule.iterator();
     List<Object> list=new ArrayList<Object>();
     LinkedHashMap<String,String> dropdownData;
     List<String> heading=new ArrayList<>();
     heading.add("Sub Group");
     LinkedHashMap groupNameAutocompleteData = new LinkedHashMap();
     for(Selection cc: rule) {
   	  dropdownData = new LinkedHashMap<String,String>();
   	  dropdownData.put("Sub Group", cc.getSvalue());
   	 
   	  dropdownData.put("optionDisplayValue", cc.getSvalue());
   	list.add(dropdownData);
     }
     groupNameAutocompleteData.put("heading", heading);
     groupNameAutocompleteData.put("data", list);
     return ResponseEntity.ok(groupNameAutocompleteData);
   
	}
}
