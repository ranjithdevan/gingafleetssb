package com.mainmethod.springboot.gingafleets.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.sql.DataSource;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.mainmethod.springboot.gingafleets.common.frontdata.CustFieldsDao;
import com.mainmethod.springboot.gingafleets.common.json.Json;
import com.mainmethod.springboot.gingafleets.common.vehicleinfo.DropDownVehicleInfoServiceImpl;
import com.mainmethod.springboot.gingafleets.dto.Cust_selectionField;
import com.mainmethod.springboot.gingafleets.dto.Selection;
import com.mainmethod.springboot.gingafleets.jdbc.DataSourceConfig;
import com.mainmethod.springboot.gingafleets.servicelmpl.angularFormlyServiceImpl;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/TripSheet")
public class TripSheetFieldController {
	
CustFieldsDao dao = new CustFieldsDao();
	
	
	
	@Autowired
	private DataSourceConfig dataSourceConfig;
	private angularFormlyServiceImpl angular =new angularFormlyServiceImpl();
	
	
	DropDownVehicleInfoServiceImpl combodao=new DropDownVehicleInfoServiceImpl();
	
	@Autowired
	Json jSON;

	  @SuppressWarnings("unchecked")
	@PostMapping(value = "/Fields/{id}")
		public ResponseEntity<?> getselectionfield(@PathVariable("id") int id,@RequestBody ArrayList<String> Company) {

			DataSource dataSource=dataSourceConfig.connectDataSource();
	       dao.setDataSource(dataSource);
	     
	       List<Cust_selectionField> list = dao.selectAllVechile(id);
	       List<Cust_selectionField> fieldList = list.stream().filter(value ->{
	    	  // System.out.println("value"+value);
	    	   return value.getField_name().equalsIgnoreCase("manifest_alias") || 
	    			   value.getField_name().equalsIgnoreCase("manifest_date") || 
	    			   value.getField_name().equalsIgnoreCase("manifest_time")|| 
	    			   value.getField_name().equalsIgnoreCase("vehicle_type")||
	    			   value.getField_name().equalsIgnoreCase("vehicle_no")||
	    			   value.getField_name().equalsIgnoreCase("kms")||   
	    			   value.getField_name().equalsIgnoreCase("supplier_name")||
	    			   value.getField_name().equalsIgnoreCase("stionary_charge")||
	    			  value.getField_name().equalsIgnoreCase("consignee_name")|| 
	    			  value.getField_name().equalsIgnoreCase("delivery_add1")||
	    			  value.getField_name().equalsIgnoreCase("route")||
	    			  value.getField_name().equalsIgnoreCase("consignee_code")||
	    			  value.getField_name().equalsIgnoreCase("veh_type")||
	    			  value.getField_name().equalsIgnoreCase("vehicle_id")||
	    			 value.getField_name().equalsIgnoreCase("supplier_code")||
	    			 value.getField_name().equalsIgnoreCase("route_no")||
	    			 value.getField_name().equalsIgnoreCase("collapse-show")||
	    			 value.getField_name().equalsIgnoreCase("driver_name_frt")||
	    			 value.getField_name().equalsIgnoreCase("licences_no")||
	    			 value.getField_name().equalsIgnoreCase("contact_no")||
	    			 value.getField_name().equalsIgnoreCase("manifest_type")||
	    			 value.getField_name().equalsIgnoreCase("driver_name_sec")||
	    			 value.getField_name().equalsIgnoreCase("customer_name")||
	    			 value.getField_name().equalsIgnoreCase("consignor_name")||
	    			 value.getField_name().equalsIgnoreCase("driver_code_frt")||
	    			 value.getField_name().equalsIgnoreCase("driver_code_sec")||
	    			 value.getField_name().equalsIgnoreCase("customer_code")||
	    			 value.getField_name().equalsIgnoreCase("consignor_code")||
	    			 value.getField_name().equalsIgnoreCase("collapse-hide")||
	    			 value.getField_name().equalsIgnoreCase("loc_from")||
	    			 value.getField_name().equalsIgnoreCase("loc_to")||
	    			 value.getField_name().equalsIgnoreCase("calc_type")||
	    			 value.getField_name().equalsIgnoreCase("material_name")||
	    		//value.getField_name()().equals("Makers_name")||
	    			 value.getField_name().equalsIgnoreCase("quantity")||
	    			 value.getField_name().equalsIgnoreCase("cust_invoice_no")||
	    			 value.getField_name().equalsIgnoreCase("cust_invoice_date")||
	    			 value.getField_name().equalsIgnoreCase("rate_ton")||
	    			 value.getField_name().equalsIgnoreCase("freight_amt")||
	    			 value.getField_name().equalsIgnoreCase("adv_freight")||
	    			 value.getField_name().equalsIgnoreCase("mrk_freight")||
	    			 value.getField_name().equalsIgnoreCase("adv_date")||
	    			 value.getField_name().equalsIgnoreCase("adv_cash_bank")||
	    			 value.getField_name().equalsIgnoreCase("trans_no")||
	    			 value.getField_name().equalsIgnoreCase("adv_amt")||
	    			 value.getField_name().equalsIgnoreCase("TripSheet")||
	    			 value.getField_name().equalsIgnoreCase("collapse-show")||
	    			 value.getField_name().equalsIgnoreCase("collapse-hide")||
	    			 value.getField_name().equalsIgnoreCase("collapse-hide")||
	    			 value.getField_name().equalsIgnoreCase("collapse-show")||
	    			 value.getField_type().equalsIgnoreCase("panel");
	    	   
	    			
	       }).collect(Collectors.toList());
	       @SuppressWarnings("rawtypes")
			LinkedHashMap Tripsheetmaster =new LinkedHashMap();
	       Tripsheetmaster.put("fields", angular.buildFormlyStructure(fieldList,Company,dataSource));
	     
	       LinkedHashMap<String, String> url =new LinkedHashMap<String, String>();
	       url.put("table", "TripSheet/getallRecords");
			url.put("save", "TripSheet/CreateTripSheetSelection");
			url.put("update", "TripSheet/updateTripSheetmaster");
			url.put("delete", "TripSheet/delete/{id}");
			Tripsheetmaster.put("url", url);
	     
	  
			return ResponseEntity.ok(Tripsheetmaster);

		}
	
}
