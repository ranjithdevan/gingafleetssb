package com.mainmethod.springboot.gingafleets.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mainmethod.springboot.gingafleets.common.tripsheet.DropDownTripsheetServiceImpl;
import com.mainmethod.springboot.gingafleets.dto.Acc_master;
import com.mainmethod.springboot.gingafleets.dto.Branchdetails;
import com.mainmethod.springboot.gingafleets.dto.Consignor_consignee;
import com.mainmethod.springboot.gingafleets.dto.Customer;
import com.mainmethod.springboot.gingafleets.dto.DriverChangeDto;
import com.mainmethod.springboot.gingafleets.dto.HireDetailsDto;
import com.mainmethod.springboot.gingafleets.dto.Maintenance;
import com.mainmethod.springboot.gingafleets.dto.Route;
import com.mainmethod.springboot.gingafleets.dto.Selection;
import com.mainmethod.springboot.gingafleets.dto.Vendor;
import com.mainmethod.springboot.gingafleets.dto.VendorSupplierDto;
import com.mainmethod.springboot.gingafleets.jdbc.DataSourceConfig;
import com.mainmethod.springboot.gingafleets.model.Cust_selection;
import com.mainmethod.springboot.gingafleets.model.Load_manifest;
import com.mainmethod.springboot.gingafleets.reports.AutoCompleteServiceImpl;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/TripsheetDropDown")
public class TripSheetDropDownController {
	
	DropDownTripsheetServiceImpl dao=new DropDownTripsheetServiceImpl();
	AutoCompleteServiceImpl daao= new AutoCompleteServiceImpl();
	
	@Autowired
	private DataSourceConfig dataSourceConfig;
	
	@PostMapping(value = "/VehicleType")
	public ResponseEntity<?> getQuery() {
		System.out.println("query execute check");
		DataSource dataSource=dataSourceConfig.connectDataSource();
		dao.setDataSource(dataSource);
		System.out.println("query execute");
      List<Selection> rule = dao.vehicletype();
      Iterator<Selection> iterator = rule.iterator();
      List<Object> lists=new ArrayList<Object>();
      List<String> heading=new ArrayList<>();
      LinkedHashMap<String,String> dropdownData;
      heading.add("VehicleType");
     
     System.out.println("vehicle type"); 
      LinkedHashMap groupNameAutocompleteData = new LinkedHashMap();
      for(Selection cc: rule) {
    	  dropdownData = new LinkedHashMap<String,String>();
    	  dropdownData.put("VehicleType", cc.getSvalue());
    	  dropdownData.put("optionDisplayValue", cc.getSvalue());
    	  lists.add(dropdownData);
      }
      
      groupNameAutocompleteData.put("heading", heading);
      groupNameAutocompleteData.put("data", lists);
      return ResponseEntity.ok(groupNameAutocompleteData);
      
	}

	@PostMapping(value = "/ConsginmentType")
public ResponseEntity<List<Selection>> getConsigtype() {
		
		DataSource dataSource=dataSourceConfig.connectDataSource();
		dao.setDataSource(dataSource);
      List<Selection> rule = dao.consigType();
     /* Iterator<Selection> iterator = rule.iterator();
      List<String> list=new ArrayList<>();
      while (iterator.hasNext()) {
        // list.add(iterator.next().getSvalue());
         list.add(iterator.next().getSub_group());
      }*/
      return ResponseEntity.ok(rule);
      
	}

	@PostMapping(value = "/Calctype")
	public ResponseEntity<List<String>> getCalcType() {
			
			DataSource dataSource=dataSourceConfig.connectDataSource();
			dao.setDataSource(dataSource);
	      List<Selection> rule = dao.calcType();
	      Iterator<Selection> iterator = rule.iterator();
	      List<String> list=new ArrayList<>();
	      while (iterator.hasNext()) {
	         //list.add(iterator.next().getSvalue());
	         list.add(iterator.next().getSvalue());
	      }
	      return ResponseEntity.ok(list);
	      
		}
	
	@PostMapping(value = "/TransportName")
	public ResponseEntity<LinkedHashMap> getQuery(@RequestBody List<String> companyAndsupplier_name) {
		
		DataSource dataSource=dataSourceConfig.connectDataSource();
		dao.setDataSource(dataSource);
      List<Vendor> rule = dao.transporterName(companyAndsupplier_name);
      List<Object> lists=new ArrayList<Object>();
      List<String> heading=new ArrayList<>();
      LinkedHashMap<String,String> dropdownData;
      heading.add("Code");
      heading.add("Name");
      
      LinkedHashMap groupNameAutocompleteData = new LinkedHashMap();
      for(Vendor cc: rule) {
    	  dropdownData = new LinkedHashMap<String,String>();
    	  dropdownData.put("Code", cc.getSupplier_code());
    	  dropdownData.put("Name", cc.getSupplier_name());
    	  dropdownData.put("optionDisplayValue", cc.getSupplier_name());
    	  lists.add(dropdownData);
      }
    
      groupNameAutocompleteData.put("heading", heading);
      groupNameAutocompleteData.put("data", lists);
      return ResponseEntity.ok(groupNameAutocompleteData);
	}
	
	@PostMapping(value = "/consigneeName")
	public ResponseEntity<LinkedHashMap> getQuerys(@RequestBody List<String> companyAndcnr_name) {
		//System.out.println("checking the value"+cnr_name+"...."+company);
		DataSource dataSource=dataSourceConfig.connectDataSource();
		dao.setDataSource(dataSource);
      List<Consignor_consignee> rule = dao.consigneeName(companyAndcnr_name);
    //  Iterator<Consignor_consignee> iterator = rule.iterator();
      List<Object> lists=new ArrayList<Object>();
      List<String> heading=new ArrayList<>();
      LinkedHashMap<String,String> dropdownData;
      heading.add("Code");
      heading.add("Name");
      
      LinkedHashMap groupNameAutocompleteData = new LinkedHashMap();
      for(Consignor_consignee cc: rule) {
    	  dropdownData = new LinkedHashMap<String,String>();
    	  dropdownData.put("Code", cc.getCnr_code());
    	  dropdownData.put("Name", cc.getCnr_name());
    	  dropdownData.put("optionDisplayValue",cc.getCnr_name());
    	  lists.add(dropdownData);
      }
      
      groupNameAutocompleteData.put("heading", heading);
      groupNameAutocompleteData.put("data", lists);
      return ResponseEntity.ok(groupNameAutocompleteData);
	}
	
	@PostMapping(value = "/Route")
	public ResponseEntity<LinkedHashMap> getQuer(@RequestBody List<String> companyAndroute_no) {
	//	System.out.println("checking the value"+route_no+"...."+company);
		DataSource dataSource=dataSourceConfig.connectDataSource();
		dao.setDataSource(dataSource);
      List<Route> route = dao.route(companyAndroute_no);
      List<Object> lists=new ArrayList<Object>();
      List<String> heading=new ArrayList<>();
      LinkedHashMap<String,String> dropdownData;
      heading.add("Route");
      
      LinkedHashMap groupNameAutocompleteData = new LinkedHashMap();
      for(Route cc: route) {
    	  dropdownData = new LinkedHashMap<String,String>();
    	  dropdownData.put("Route", cc.getRoute_no());
    	  dropdownData.put("optionDisplayValue",cc.getRoute_no());
    	  lists.add(dropdownData);
      }
      
      groupNameAutocompleteData.put("heading", heading);
      groupNameAutocompleteData.put("data", lists);
      return ResponseEntity.ok(groupNameAutocompleteData);
	}
	
	@PostMapping(value = "/Customer")
	public ResponseEntity<LinkedHashMap> getQuers(@RequestBody List<String> companyAndcust_name) {
		//System.out.println("checking the value"+cust_name+"...."+company);
		DataSource dataSource=dataSourceConfig.connectDataSource();
		dao.setDataSource(dataSource);
      List<Customer> customer = dao.cust(companyAndcust_name);
      List<Object> lists=new ArrayList<Object>();
      List<String> heading=new ArrayList<>();
      LinkedHashMap<String,String> dropdownData;
      heading.add("Code");
      heading.add("Name");
      
      LinkedHashMap groupNameAutocompleteData = new LinkedHashMap();
      for(Customer cc: customer) {
    	  dropdownData = new LinkedHashMap<String,String>();
    	  dropdownData.put("Code", cc.getCust_code());
    	  dropdownData.put("Name", cc.getCust_name());
    	  dropdownData.put("optionDisplayValue", cc.getCust_name());
    	  lists.add(dropdownData);
      }
      
      groupNameAutocompleteData.put("heading", heading);
      groupNameAutocompleteData.put("data", lists);
      return ResponseEntity.ok(groupNameAutocompleteData);
	}
	
	@PostMapping(value = "/Material")
	public ResponseEntity<List<String>> getmaterial(@RequestBody List<String> company) {
		System.out.println("checking the value"+company);
		DataSource dataSource=dataSourceConfig.connectDataSource();
		dao.setDataSource(dataSource);
      List<Cust_selection> route = dao.material(company);
      Iterator<Cust_selection> iterator = route.iterator();
      List<String> list=new ArrayList<>();
      while (iterator.hasNext()) {
       //  list.add(iterator.next().getRoute_sysno());
         list.add(iterator.next().getSvalue());
      }
      return ResponseEntity.ok(list);
	}
	
	
	
	@PostMapping(value = "/VehicleOwnHire")
	public ResponseEntity<LinkedHashMap> getVehicleownHire(@RequestBody List<String> vehicletypeAndcompanyAndvehicle_no) {
		//System.out.println("checking the value"+company);
		DataSource dataSource=dataSourceConfig.connectDataSource();
		dao.setDataSource(dataSource);
      List<VendorSupplierDto> route = dao.vehicleOwnHire(vehicletypeAndcompanyAndvehicle_no);
      List<Object> lists=new ArrayList<Object>();
      List<String> heading=new ArrayList<>();
      LinkedHashMap<String,String> dropdownData;
      heading.add("Id");
      heading.add("Type");
      heading.add("Number");
      
      LinkedHashMap groupNameAutocompleteData = new LinkedHashMap();
      for(VendorSupplierDto cc: route) {
    	  dropdownData = new LinkedHashMap<String,String>();
    	  dropdownData.put("Id", cc.getVehicle_id());
    	  dropdownData.put("Type", cc.getVehicle_type());
    	  dropdownData.put("Number", cc.getVehicle_no());
    	  dropdownData.put("optionDisplayValue", cc.getVehicle_no());
    	  lists.add(dropdownData);
      }
      
      groupNameAutocompleteData.put("heading", heading);
      groupNameAutocompleteData.put("data", lists);
      return ResponseEntity.ok(groupNameAutocompleteData);
     
	}
	
	@PostMapping(value = "/DriverNameFirst")
	public ResponseEntity<LinkedHashMap> getdrivernamefirst(@RequestBody List<String> company) {
		System.out.println("checking the value"+company);
		DataSource dataSource=dataSourceConfig.connectDataSource();
		dao.setDataSource(dataSource);
      List<HireDetailsDto> route = dao.driverNameFirst(company);
      List<Object> lists=new ArrayList<Object>();
      List<String> heading=new ArrayList<>();
      LinkedHashMap<String,String> dropdownData;
      heading.add("Id");
      heading.add("Driver Name");
      heading.add("License Number");
      heading.add("phone No");
      
      LinkedHashMap groupNameAutocompleteData = new LinkedHashMap();
      for(HireDetailsDto cc: route) {
    	  dropdownData = new LinkedHashMap<String,String>();
    	  dropdownData.put("Id", cc.getEmployee_id());
    	  dropdownData.put("Driver Name", cc.getLicence_name());
    	  dropdownData.put("License Number", cc.getLicence_no());
    	  dropdownData.put("Phone No", cc.getPhone_no());
    	  dropdownData.put("optionDisplayValue", cc.getLicence_name());
    	  lists.add(dropdownData);
      }
      
      groupNameAutocompleteData.put("heading", heading);
      groupNameAutocompleteData.put("data", lists);
      return ResponseEntity.ok(groupNameAutocompleteData);
     
	}
	
	@PostMapping(value = "/DriverNameSecond/{company},{employee_sec_name}")
	public ResponseEntity<List<?>> getdrivernameSecond(@PathVariable("company") String company,@PathVariable("employee_sec_name") String employee_sec_name) {
		System.out.println("checking the value"+company);
		DataSource dataSource=dataSourceConfig.connectDataSource();
		dao.setDataSource(dataSource);
      List<DriverChangeDto> route = dao.driverNameSecond(company,employee_sec_name);
      return ResponseEntity.ok(route);
	}
	
	@PostMapping(value = "/CashBank")
	public ResponseEntity<LinkedHashMap> cashBank(@RequestBody List<String> companyAndCashBank) {
		//System.out.println("checking the value"+cust_name+"...."+company);
		DataSource dataSource=dataSourceConfig.connectDataSource();
		dao.setDataSource(dataSource);
      List<Acc_master> cash = dao.cashBank(companyAndCashBank);
      List<Object> lists=new ArrayList<Object>();
      List<String> heading=new ArrayList<>();
      LinkedHashMap<String,String> dropdownData;
      heading.add("Ac Head Name");
      heading.add("Ac code No");
     
      
      LinkedHashMap groupNameAutocompleteData = new LinkedHashMap();
      for(Acc_master cc: cash) {
    	  dropdownData = new LinkedHashMap<String,String>();
    	
    	  dropdownData.put("Ac Head Name", cc.getAc_head_name());
    	  dropdownData.put("Ac code No", cc.getCode_no());
    	  dropdownData.put("optionDisplayValue", cc.getAc_head_name());
    	  lists.add(dropdownData);
      }
      
      groupNameAutocompleteData.put("heading", heading);
      groupNameAutocompleteData.put("data", lists);
      return ResponseEntity.ok(groupNameAutocompleteData);
	}  

	@PostMapping(value = "/TripReportforall")
	public ResponseEntity<LinkedHashMap> trip(@RequestBody List<String> vehicle) {
		//System.out.println("checking the value"+cust_name+"...."+company);
		DataSource dataSource=dataSourceConfig.connectDataSource();
		daao.setDataSource(dataSource);
      List<Load_manifest> triip = daao.vehicleno(vehicle);
      List<Object> lists=new ArrayList<Object>();
      List<String> heading=new ArrayList<>();
      LinkedHashMap<String,String> dropdownData;
      heading.add("Vehicle No");
      LinkedHashMap groupNameAutocompleteData = new LinkedHashMap();
      for(Load_manifest cc: triip) {
    	  dropdownData = new LinkedHashMap<String,String>();
    	  dropdownData.put("Vehicle No", cc.getVehicle_no());
    	  dropdownData.put("optionDisplayValue", cc.getVehicle_no());
    	  lists.add(dropdownData);
      }
      
      groupNameAutocompleteData.put("heading", heading);
      groupNameAutocompleteData.put("data", lists);
      return ResponseEntity.ok(groupNameAutocompleteData);
	}  
	
	@PostMapping(value = "/ReportCenterName")
	public ResponseEntity<?> getcentername(@RequestBody List<String> companyAndCenterno) {
		
		DataSource dataSource=dataSourceConfig.connectDataSource();
		dao.setDataSource(dataSource);
      List<Branchdetails> rule = dao.centerName(companyAndCenterno);
      List<Object> lists=new ArrayList<Object>();
      List<String> heading=new ArrayList<>();
      LinkedHashMap<String,String> dropdownData;
      heading.add("Center Name");
     
      
      LinkedHashMap groupNameAutocompleteData = new LinkedHashMap();
      for(Branchdetails cc: rule) {
    	  dropdownData = new LinkedHashMap<String,String>();
    	  dropdownData.put("Center Name", cc.getCenter_name());
    	  dropdownData.put("optionDisplayValue", cc.getCenter_name());
    	  lists.add(dropdownData);
      }
      
      groupNameAutocompleteData.put("heading", heading);
      groupNameAutocompleteData.put("data", lists);
      return ResponseEntity.ok(groupNameAutocompleteData);
      
	}
	
	@PostMapping(value = "/ReportCustomerGrpBank")
	public ResponseEntity<LinkedHashMap> CustomerGrpBank(@RequestBody List<String> CustomerGrp) {
		//System.out.println("checking the value"+cust_name+"...."+company);
		DataSource dataSource=dataSourceConfig.connectDataSource();
		daao.setDataSource(dataSource);
      List<Acc_master> cash = daao.CustomerGrps(CustomerGrp);
      List<Object> lists=new ArrayList<Object>();
      List<String> heading=new ArrayList<>();
      LinkedHashMap<String,String> dropdownData;
      heading.add("Ac Head Name");
      LinkedHashMap groupNameAutocompleteData = new LinkedHashMap();
      for(Acc_master cc: cash) {
    	  dropdownData = new LinkedHashMap<String,String>();
    	  dropdownData.put("Ac Head Name", cc.getAc_head_name());
    	  dropdownData.put("optionDisplayValue", cc.getAc_head_name());
    	  lists.add(dropdownData);
      }
      
      groupNameAutocompleteData.put("heading", heading);
      groupNameAutocompleteData.put("data", lists);
      return ResponseEntity.ok(groupNameAutocompleteData);
	}  

}
