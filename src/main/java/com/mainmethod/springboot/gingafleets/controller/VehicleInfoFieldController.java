package com.mainmethod.springboot.gingafleets.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mainmethod.springboot.gingafleets.common.frontdata.CustFieldsDao;
import com.mainmethod.springboot.gingafleets.common.json.Json;
import com.mainmethod.springboot.gingafleets.common.vehicleinfo.DropDownVehicleInfoServiceImpl;
import com.mainmethod.springboot.gingafleets.dto.Cust_selectionField;
import com.mainmethod.springboot.gingafleets.dto.Selection;
import com.mainmethod.springboot.gingafleets.jdbc.DataSourceConfig;
import com.mainmethod.springboot.gingafleets.model.Cust_selection;
import com.mainmethod.springboot.gingafleets.servicelmpl.angularFormlyServiceImpl;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/VehcileMasterInfo")
public class VehicleInfoFieldController {

	CustFieldsDao dao = new CustFieldsDao();
	
DropDownVehicleInfoServiceImpl combodao=new DropDownVehicleInfoServiceImpl();
	
	@Autowired
	private DataSourceConfig dataSourceConfig;
	private angularFormlyServiceImpl angular =new angularFormlyServiceImpl();
	
	
	
	
	@Autowired
	Json jSON;

	  @SuppressWarnings("unchecked")
	@PostMapping(value = "/Fields/{id}")
		public ResponseEntity<?> getselectionfield(@PathVariable("id") int id,@RequestBody ArrayList<String> Company) {

			DataSource dataSource=dataSourceConfig.connectDataSource();
	       dao.setDataSource(dataSource);
	       List<Cust_selectionField> list = dao.selectAllVechile(id);
	     
	    
	       List<Cust_selectionField> fieldList = list.stream().filter(value ->{
	    	
	    	 
	    	   
	    	   return  value.getField_name().equalsIgnoreCase("Vehicle_Info") ||
	    			   value.getField_name().equalsIgnoreCase("vehicle_id") || 
	    			   value.getField_name().equalsIgnoreCase("vehicle_no") || 
	    			   value.getField_name().equalsIgnoreCase("Make")|| 
	    			   value.getField_name().equalsIgnoreCase("Model")||
	    			   value.getField_name().equalsIgnoreCase("Alias_name")||
	    			   value.getField_name().equalsIgnoreCase("Speed_status")||   
	    			   value.getField_name().equalsIgnoreCase("Kms_run")||
	    			   value.getField_name().equalsIgnoreCase("Start_km")||
	    			  value.getField_name().equalsIgnoreCase("Acc_name")|| 
	    			  value.getField_name().equalsIgnoreCase("Center_name")||
	    			  value.getField_name().equalsIgnoreCase("Address1")||
	    			  value.getField_name().equalsIgnoreCase("Address2")||
	    			  value.getField_name().equalsIgnoreCase("Class_veh")||
	    			  value.getField_name().equalsIgnoreCase("Vehicle_type")||
	    			 value.getField_name().equalsIgnoreCase("Owner_name")||
	    			 value.getField_name().equalsIgnoreCase("Date_reg")||
	    			 value.getField_name().equalsIgnoreCase("Date_purch")||
	    			 value.getField_name().equalsIgnoreCase("Date_sale")||
	    			 value.getField_name().equalsIgnoreCase("Body_color")||
	    			 value.getField_name().equalsIgnoreCase("Size_rearaxle")||
	    			 value.getField_name().equalsIgnoreCase("Size_frontaxle")||
	    			 value.getField_name().equalsIgnoreCase("Fuel_type")||
	    			 value.getField_name().equalsIgnoreCase("Tank_capacity")||
	    			 value.getField_name().equalsIgnoreCase("Seat")||
	    			 value.getField_name().equalsIgnoreCase("Gross_vehiclewt")||
	    			 value.getField_name().equalsIgnoreCase("Unload_wt")||
	    			 value.getField_name().equalsIgnoreCase("Wt_manuf")||
	    			 value.getField_name().equalsIgnoreCase("Lease_veh")||
	    			 value.getField_name().equalsIgnoreCase("Engin_no")||
	    			 value.getField_name().equalsIgnoreCase("Chassis_no")||
	    			 value.getField_name().equalsIgnoreCase("Horse_power")||
	    			 value.getField_name().equalsIgnoreCase("Cubic_cap")||
	    			 value.getField_name().equalsIgnoreCase("Makers_class")||
	    		     value.getField_name().equals("Makers_name")||
	    			 value.getField_name().equalsIgnoreCase("Month_menuf")||
	    			 value.getField_name().equalsIgnoreCase("Migrate_state")||
	    			 value.getField_name().equalsIgnoreCase("Type_body")||
	    			 value.getField_name().equalsIgnoreCase("No_cyclinder")||
	    			 value.getField_name().equalsIgnoreCase("Remark")||
	    			 value.getField_name().equalsIgnoreCase("Vtsunituname")||
	    			 value.getField_name().equalsIgnoreCase("Veh_ref_code")||
	    			 value.getField_name().equalsIgnoreCase("Warranty_NO")||
	    			 value.getField_name().equalsIgnoreCase("Expi_Date")||
	    			 value.getField_name().equalsIgnoreCase("Expi_Kms")||
	    			 value.getField_name().equalsIgnoreCase("EMI Details")||
	    			 value.getField_name().equalsIgnoreCase("Emi_from_date")||
	    			 value.getField_name().equalsIgnoreCase("Emi_to_date")||
	    			 value.getField_name().equalsIgnoreCase("Emi_amount")||
	    			 value.getField_name().equalsIgnoreCase("Vehicle Maintainence ")||
	    			 value.getField_name().equalsIgnoreCase("sub_act_group")||
	    			 value.getField_name().equalsIgnoreCase("sub_act_name")||
	    			 value.getField_name().equalsIgnoreCase("sub_act_code")||
	    			 value.getField_name().equalsIgnoreCase("period_kms")||
	    			 value.getField_name().equalsIgnoreCase("period_days")||
	    			 value.getField_name().equalsIgnoreCase("remind_kms")||
	    			 value.getField_name().equalsIgnoreCase("remind_days")||
	    			 value.getField_name().equalsIgnoreCase("date")||
	    			 value.getField_name().equalsIgnoreCase("cur_kms")||
	    			 value.getField_name().equalsIgnoreCase("exp_date")||
	    			 value.getField_name().equalsIgnoreCase("add_amt")||
	    			 value.getField_name().equalsIgnoreCase("remarks")||
	    			 value.getField_type().equalsIgnoreCase("panel")||
	    	   value.getField_type().equals("panel");
	       }).collect(Collectors.toList());
	       for(int i=0;i<fieldList.size();i++) {
	    	   System.out.println(fieldList.get(i).getField_type().toString());
	       } 
	       @SuppressWarnings("rawtypes")
			LinkedHashMap urls =new LinkedHashMap();
	       urls.put("fields", angular.buildFormlyStructure(fieldList,Company,dataSource));
	       LinkedHashMap<String, String> url =new LinkedHashMap<String, String>();
	       url.put("table", "VehicleMaster/getallRecords");
			url.put("save", "VehicleMaster/CreateVehicleSelection");
			url.put("update", "VehicleMaster/updateVehiclemaster");
			url.put("delete", "VehicleMaster/delete/{id}");
			urls.put("url", url);
	      

	  
			return ResponseEntity.ok(urls);

		}
	

}
