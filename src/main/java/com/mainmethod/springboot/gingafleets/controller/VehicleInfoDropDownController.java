package com.mainmethod.springboot.gingafleets.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.mainmethod.springboot.gingafleets.common.vehicleinfo.DropDownVehicleInfoServiceImpl;
import com.mainmethod.springboot.gingafleets.dto.Branchdetails;
import com.mainmethod.springboot.gingafleets.dto.Maintenance;
import com.mainmethod.springboot.gingafleets.dto.Selection;
import com.mainmethod.springboot.gingafleets.jdbc.DataSourceConfig;
import com.mainmethod.springboot.gingafleets.model.Cust_selection;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/VehicleInfoDropDown")
public class VehicleInfoDropDownController {
	
	
	DropDownVehicleInfoServiceImpl dao=new DropDownVehicleInfoServiceImpl();
	
	@Autowired
	private DataSourceConfig dataSourceConfig;
	
	@PostMapping(value = "/Speedometer")
	public ResponseEntity<List<String>> getQuery() {
		
		DataSource dataSource=dataSourceConfig.connectDataSource();
		dao.setDataSource(dataSource);
      List<Selection> rule = dao.SpeedoMeter();
      Iterator<Selection> iterator = rule.iterator();
      List<String> list=new ArrayList<>();
      while (iterator.hasNext()) {
         list.add(iterator.next().getSvalue());
      }
      return ResponseEntity.ok(list);
      
	}
	
	@PostMapping(value = "/VehicleType")
	public ResponseEntity<List<String>> getvehicletype() {
		
		DataSource dataSource=dataSourceConfig.connectDataSource();
		dao.setDataSource(dataSource);
      List<Selection> rule = dao.leaseType();
      Iterator<Selection> iterator = rule.iterator();
      List<String> list=new ArrayList<>();
      while (iterator.hasNext()) {
         list.add(iterator.next().getSvalue());
      }
      return ResponseEntity.ok(list);
      
	}
	
	@PostMapping(value = "/Make")
	public ResponseEntity<List<String>> getmaterial(@RequestBody List<String> company) {
		//System.out.println("checking the value"+company);
		DataSource dataSource=dataSourceConfig.connectDataSource();
		dao.setDataSource(dataSource);
      ArrayList<String> route = dao.make(company);
      Iterator<String> iterator = route.iterator();
      List<String> list=new ArrayList<>();
      while (iterator.hasNext()) {
       //  list.add(iterator.next().getRoute_sysno());
         list.add(iterator.next());
      }
      return ResponseEntity.ok(list);
	}
	
	@PostMapping(value = "/Model")
	public ResponseEntity<List<String>> getmodel(@RequestBody List<String> company) {
		System.out.println("checking the value"+company);
		DataSource dataSource=dataSourceConfig.connectDataSource();
		dao.setDataSource(dataSource);
      List<Cust_selection> route = dao.model(company);
      Iterator<Cust_selection> iterator = route.iterator();
      List<String> list=new ArrayList<>();
      while (iterator.hasNext()) {
       //  list.add(iterator.next().getRoute_sysno());
         list.add(iterator.next().getSvalue());
      }
      return ResponseEntity.ok(list);
	}
	
	@PostMapping(value = "/CenterNo")
	public ResponseEntity<LinkedHashMap> getcustomerno(@RequestBody List<String> companyAndcentername) {
		//System.out.println("checking the value"+company);
		DataSource dataSource=dataSourceConfig.connectDataSource();
		dao.setDataSource(dataSource);
      List<Branchdetails> route = dao.centerno(companyAndcentername);
      List<Object> lists=new ArrayList<Object>();
      List<String> heading=new ArrayList<>();
      LinkedHashMap<String,String> dropdownData;
      heading.add("Center Name");
      heading.add("Id");
      
      LinkedHashMap groupNameAutocompleteData = new LinkedHashMap();
      for(Branchdetails cc: route) {
    	  dropdownData = new LinkedHashMap<String,String>();
    	  dropdownData.put("Center Name", cc.getCenter_name());
    	  dropdownData.put("Id", cc.getCenter_id());
    	  dropdownData.put("optionDisplayValue", cc.getCenter_name());
    	  lists.add(dropdownData);
      }
      
      groupNameAutocompleteData.put("heading", heading);
      groupNameAutocompleteData.put("data", lists);
      return ResponseEntity.ok(groupNameAutocompleteData);
      
	}
	
	@PostMapping(value = "/VehicleTypes")
	public ResponseEntity<List<String>> getvehicle(@RequestBody List<String> company) {
		System.out.println("checking the value"+company);
		DataSource dataSource=dataSourceConfig.connectDataSource();
		dao.setDataSource(dataSource);
      List<Selection> route = dao.vehicletype(company);
      Iterator<Selection> iterator = route.iterator();
      List<String> list=new ArrayList<>();
      while (iterator.hasNext()) {
       //  list.add(iterator.next().getRoute_sysno());
         list.add(iterator.next().getSvalue());
        
      }
      return ResponseEntity.ok(list);
	}

	@PostMapping(value = "/MaintenanceGroup")
	public ResponseEntity<List<String>> getMaintainGrp() {
		
		DataSource dataSource=dataSourceConfig.connectDataSource();
		dao.setDataSource(dataSource);
      List<Selection> rule = dao.maintanGrp();
      Iterator<Selection> iterator = rule.iterator();
      List<String> list=new ArrayList<>();
      while (iterator.hasNext()) {
         list.add(iterator.next().getSvalue());
      }
      return ResponseEntity.ok(list);
      
	}
	
	@PostMapping(value = "/FuelType")
	public ResponseEntity<List<String>> getFuelType(@RequestBody List<String> company) {
		
		DataSource dataSource=dataSourceConfig.connectDataSource();
		dao.setDataSource(dataSource);
      List<Cust_selection> rule = dao.fuelType(company);
      Iterator<Cust_selection> iterator = rule.iterator();
      List<String> list=new ArrayList<>();
      while (iterator.hasNext()) {
         list.add(iterator.next().getSvalue());
      }
      return ResponseEntity.ok(list);
      
	}
	
	@PostMapping(value = "/ActivityName")
	public ResponseEntity<LinkedHashMap> getactivityName(@RequestBody List<String> companyAndactivity_nameAndmaint_group) {
		//System.out.println("checking the value"+company);
		DataSource dataSource=dataSourceConfig.connectDataSource();
		dao.setDataSource(dataSource);
      List<Maintenance> route = dao.activityName(companyAndactivity_nameAndmaint_group);
      List<Object> lists=new ArrayList<Object>();
      List<String> heading=new ArrayList<>();
      LinkedHashMap<String,String> dropdownData;
      heading.add("Maintenance Group");
      heading.add("Activity Name");
      heading.add("Activity Code");
      heading.add("Period Kms");
      heading.add("Period Days");
      heading.add("Remind Kms");
      heading.add("Remind Days");
      
      LinkedHashMap groupNameAutocompleteData = new LinkedHashMap();
      for(Maintenance cc: route) {
    	  dropdownData = new LinkedHashMap<String,String>();
    	  dropdownData.put("Maintenance Group", cc.getMaint_group());
    	  dropdownData.put("Activity Name", cc.getActivity_name());
    	  dropdownData.put("Activity Code", cc.getMaint_code());
    	  dropdownData.put("Period Kms", cc.getPeriod_kms());
    	  dropdownData.put("Period Days", cc.getPeriod_days());
    	  dropdownData.put("Remind Kms", cc.getRemind_kms());
    	  dropdownData.put("Remind Days", cc.getRemind_days());
    	  dropdownData.put("optionDisplayValue", cc.getActivity_name());
    	  lists.add(dropdownData);
      }
      
      groupNameAutocompleteData.put("heading", heading);
      groupNameAutocompleteData.put("data", lists);
      return ResponseEntity.ok(groupNameAutocompleteData);
      
	}
}
