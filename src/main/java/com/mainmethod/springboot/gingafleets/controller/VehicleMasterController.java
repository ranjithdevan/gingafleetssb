package com.mainmethod.springboot.gingafleets.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mainmethod.springboot.gingafleets.common.all.SubGroupAlreadyExistsException;
import com.mainmethod.springboot.gingafleets.model.Vehicle;
import com.mainmethod.springboot.gingafleets.servicelmpl.VehicleMasterServiceImpl;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/VehicleMaster")
public class VehicleMasterController {
	
	@Autowired
	VehicleMasterServiceImpl vehicleMasterServiceImpl;
	
	@PostMapping(value = "/getallRecords")
	public ResponseEntity<?>   getselectionfield() {
		
	
      List<Vehicle> list =vehicleMasterServiceImpl.getall();
      ArrayList<Object> VehicleInfoTable = new ArrayList<Object>();
      for(int i=0;i<list.size();i++) {
    	  LinkedHashMap selectionObject =new LinkedHashMap();
	    //	selectionObject.put("VEHICLE CODE", list.get(i).getVehicle_id());
	    	selectionObject.put("VEHICLE NO", list.get(i).getVehicle_no());
	    	selectionObject.put("ALIAS NAME", list.get(i).getAlias_name());
	    	selectionObject.put("CENTER NAME", list.get(i).getCenter_name());
	    	selectionObject.put("VEHICLE TYPE", list.get(i).getVehicle_type());
	    	selectionObject.put("DATE OF PURCHASE", list.get(i).getDate_purch());
	    	selectionObject.put("FUEL TYPE", list.get(i).getFuel_type());
	    	selectionObject.put("ID", list.get(i).getId());
	    	VehicleInfoTable.add(selectionObject);
      }
      LinkedHashMap tableData =new LinkedHashMap();
	    ArrayList<String> tableHeading = new ArrayList<String>();
	    tableHeading.add("VEHICLE CODE");
	    tableHeading.add("VEHICLE NO");
	    tableHeading.add("ALIAS NAME");
	    tableHeading.add("CENTER NAME");
	    tableHeading.add("VEHICLE TYPE");
	    tableHeading.add("DATE OF PURCHASE");
	    tableHeading.add("FUEL TYPE");
	    tableHeading.add("ID");
	    tableHeading.add(" ");
	    tableHeading.add(" ");
	    tableData.put("tableHeading", tableHeading);
	    tableData.put("tableData", VehicleInfoTable);
      return ResponseEntity.ok(tableData);
	}
	
	@PostMapping("/CreateVehicleSelection")
	public Vehicle create(@RequestBody Vehicle vehicle) throws SubGroupAlreadyExistsException {
	Vehicle saveVehicleMasterDetails= vehicleMasterServiceImpl.saveVehicleMaster(vehicle);
		return saveVehicleMasterDetails;
	}
	
	@PutMapping("/updateVehiclemaster")
	public Vehicle update(@RequestBody Vehicle vehicle) {
		Vehicle updateVehicleMasterDetails = vehicleMasterServiceImpl.updateSelectionMaster(vehicle);
			return updateVehicleMasterDetails;	
	}
	
	@DeleteMapping("/delete/{id}")
	public void delete(@PathVariable("id") int id) {
		vehicleMasterServiceImpl.delete(id);
     
	}
}
