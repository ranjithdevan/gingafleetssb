
package com.mainmethod.springboot.gingafleets.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import java.util.TreeMap;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.mainmethod.springboot.gingafleets.common.all.SubGroupAlreadyExistsException;
import com.mainmethod.springboot.gingafleets.dto.Selection;
import com.mainmethod.springboot.gingafleets.model.Cust_selection;
import com.mainmethod.springboot.gingafleets.servicelmpl.SelectionMasterServiceImpl;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/SelectionMaster")
public class SelectionMasterController {
	
	@Autowired
	SelectionMasterServiceImpl selectionMasterServiceImpl;
	
	
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@PostMapping(value = "/getallRecords")
	public ResponseEntity<LinkedHashMap>  getselectionfield() {
		
	    List<Cust_selection> list =selectionMasterServiceImpl.getall();
	    ArrayList<Object> selectionMasterTable = new ArrayList<Object>();
	    for(int i=0;i<list.size();i++) {
	    	LinkedHashMap selectionObject =new LinkedHashMap();
	    	selectionObject.put("Selection code", list.get(i).getSelection_code());
	    	selectionObject.put("Group Name", list.get(i).getGroup_name());
	    	selectionObject.put("Sub Group Name", list.get(i).getSub_group());
	    	selectionObject.put("Svalue", list.get(i).getSvalue());
	    	selectionObject.put("ID", list.get(i).getId());
	    	selectionMasterTable.add(selectionObject);
	    }
		LinkedHashMap tableData =new LinkedHashMap();
	    ArrayList<String> tableHeading = new ArrayList<String>();
	    tableHeading.add("SELECTION CODE");
	    tableHeading.add("GROUP NAME");
	    tableHeading.add("SUB GROUP NAME");
	    tableHeading.add("VALUE");
	    tableHeading.add("ID");
	    tableHeading.add("");
	    tableHeading.add("");
	    tableData.put("tableHeading", tableHeading);
	    tableData.put("tableData", selectionMasterTable);
		return ResponseEntity.ok(tableData);
	}
	
	@PostMapping("/CreateSelection")
	public Cust_selection create(@RequestBody Cust_selection cust_select) throws SubGroupAlreadyExistsException {
	System.out.println("company code--------------- checking from front end"+cust_select.getCompany_code());
		
		Cust_selection saveSelectionMasterDetails= selectionMasterServiceImpl.saveSelectionMaster(cust_select);
		return saveSelectionMasterDetails;
	}
	
	@PutMapping("/updateSelectionmaster")
	public Cust_selection update(@RequestBody Cust_selection cust_select) {
		Cust_selection updateSelectionMasterDetails =	selectionMasterServiceImpl.updateSelectionMaster(cust_select);
			return updateSelectionMasterDetails;
		
	}
	
	@DeleteMapping("/delete")
	public void delete(@PathVariable("id") int id) {
		selectionMasterServiceImpl.delete(id);
     
	}

}
