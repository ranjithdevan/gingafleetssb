package com.mainmethod.springboot.gingafleets.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
//import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mainmethod.springboot.gingafleets.common.frontdata.CustFieldsDao;
import com.mainmethod.springboot.gingafleets.common.json.Json;
import com.mainmethod.springboot.gingafleets.dto.Cust_selectionField;
import com.mainmethod.springboot.gingafleets.dynamic.LandingDynamicPageImpl;
import com.mainmethod.springboot.gingafleets.dynamic.angularMenu;
import com.mainmethod.springboot.gingafleets.jdbc.DataSourceConfig;
import com.mainmethod.springboot.gingafleets.model.ItemDescription;
import com.mainmethod.springboot.gingafleets.model.LoginBean;
import com.mainmethod.springboot.gingafleets.service.LoginService;


//controller for login
@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/Login")
public class LoginController {
	
	LandingDynamicPageImpl dao = new LandingDynamicPageImpl();

	@Autowired
	private LoginService loginService;
	
	@Autowired
	private DataSourceConfig dataSourceConfig;
	
	private angularMenu angularMenuService = new angularMenu();
	
	@Autowired
	Json jSON;

	@PostMapping(value = "/UserNameAndPassword")
	public ResponseEntity<List<?>> login(@RequestBody() LoginBean login) {
		DataSource dataSource=dataSourceConfig.connectDataSource();
	       dao.setDataSource(dataSource);
		if((login.getUsername() !="" && login.getPassword()!=null )
				&& ( login.getPassword()!="" && login.getPassword() !=null)){
			List<LoginBean> user = loginService.checUser(login.getUsername(), login.getPassword());
			if(user != null) {
			       ArrayList<Object> listZero = dao.selectDynamic("0","cement5@gmail.com","C001","A000198E001","yes");
			       
			  
				ArrayList<Object> menuItems = angularMenuService.menuBuild(listZero, "cement5@gmail.com", "C001",dataSource);
				return ResponseEntity.ok(menuItems);

				
			}else {
				return null;
			}
		}
		else {
			return 	null;
		}
		
		
		
		}
	}
	



