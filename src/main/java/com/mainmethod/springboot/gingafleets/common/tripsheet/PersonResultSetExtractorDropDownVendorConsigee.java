package com.mainmethod.springboot.gingafleets.common.tripsheet;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.mainmethod.springboot.gingafleets.dto.Consignor_consignee;


public class PersonResultSetExtractorDropDownVendorConsigee implements ResultSetExtractor {
	//List<Consignor_consignee> consignees = new ArrayList();
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		System.out.println("---------------->rs.getSring(1)");
		//Vendor vendor= new Vendor();
		Consignor_consignee consignee= new Consignor_consignee();  
		consignee.setCnr_code(rs.getString(1));
		System.out.println("---------------->rs.getSring(1)"+rs.getString(1));
		consignee.setCnr_name(rs.getString(2));
		System.out.println("---------------->rs.getSring(2)"+rs.getString(2));
		//consignees.add(consignee);
		return consignee;
	}
}
