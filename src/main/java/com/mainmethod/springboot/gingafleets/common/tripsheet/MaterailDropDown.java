package com.mainmethod.springboot.gingafleets.common.tripsheet;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;

import com.mainmethod.springboot.gingafleets.common.frontdata.PersonResultSetExtractor;

public class MaterailDropDown implements RowMapper {
	@Override
    public Object mapRow(ResultSet rs, int line) throws SQLException {
        PersonResultSetExtractorMaterial extractor = new PersonResultSetExtractorMaterial();
        return extractor.extractData(rs);
    }
}
