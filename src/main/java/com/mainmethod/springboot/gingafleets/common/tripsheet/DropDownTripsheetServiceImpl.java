package com.mainmethod.springboot.gingafleets.common.tripsheet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.mainmethod.springboot.gingafleets.common.frontdata.SubgroupDropDown;
import com.mainmethod.springboot.gingafleets.common.frontdata.SubgroupDropDownSelection;
import com.mainmethod.springboot.gingafleets.dto.Acc_master;
import com.mainmethod.springboot.gingafleets.dto.Branchdetails;
import com.mainmethod.springboot.gingafleets.dto.Consignor_consignee;
import com.mainmethod.springboot.gingafleets.dto.Customer;
import com.mainmethod.springboot.gingafleets.dto.DriverChangeDto;
import com.mainmethod.springboot.gingafleets.dto.HireDetailsDto;
import com.mainmethod.springboot.gingafleets.dto.Maintenance;
import com.mainmethod.springboot.gingafleets.dto.Route;
import com.mainmethod.springboot.gingafleets.dto.Selection;
import com.mainmethod.springboot.gingafleets.dto.Vendor;
import com.mainmethod.springboot.gingafleets.dto.VendorSupplierDto;
import com.mainmethod.springboot.gingafleets.model.Cust_selection;

public class DropDownTripsheetServiceImpl implements DropDownTripsheetService {

	private DataSource dataSource;
	
	
	public void setDataSource(DataSource ds) {	
        dataSource = ds;	
    }
	@Override
	public List<Selection> vehicletype() {
		
		JdbcTemplate select = new JdbcTemplate(dataSource);
		  String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no='0301V00255F'", String.class);
		  System.out.println("refrenceQuery------->"+refrenceQuery);
		 return  (ArrayList)  select.query(refrenceQuery,
				 new SubgroupDropDownSelection());
	
	

}
	@Override
	public List<Selection> consigType() {
		
		JdbcTemplate select = new JdbcTemplate(dataSource);
		  String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no='0401V00260F'", String.class);
		  System.out.println("refrenceQuery------->"+refrenceQuery);
		 return  (ArrayList)  select.query(refrenceQuery,new ConsigeetypeDropDown());
				// new SubgroupDropDown());
	}
	@Override
	public List<Selection> calcType() {
		JdbcTemplate select = new JdbcTemplate(dataSource);
		  String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no='0301V00265'", String.class);
		  System.out.println("refrenceQuery------->"+refrenceQuery);
		 return  (ArrayList)  select.query(refrenceQuery,
				 new SubgroupDropDown());
	}
	@Override
	public List<Vendor> transporterName(List<String> companyAndsupplier_name) {
		String companycode="";
		String supplier_name="";
		JdbcTemplate select = new JdbcTemplate(dataSource);
		 String[] obj = null;
		 if(companyAndsupplier_name.size()==2) {
		companycode=companyAndsupplier_name.get(1);
		supplier_name=companyAndsupplier_name.get(0);
		
		if(companycode.equals("")) {
			companycode=" ";
		    }
		supplier_name=supplier_name.replaceAll("\\s+", " ");
		 if(supplier_name.equals(" ")) {
			 supplier_name="";
		 } 
		 String newReferenceQuery="";
		 String finalQuery="";
		  String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no='0201V00269F'", String.class);
		  System.out.println("refrenceQuery------->"+refrenceQuery);
		  if(supplier_name.isEmpty() && companycode !=null) {
			  newReferenceQuery=refrenceQuery.replace("'?%'", "'%%'");
		    	 System.out.println("newReferenceQuery------>"+newReferenceQuery);
		     finalQuery=newReferenceQuery.replace("'&^'", "?");
		     System.out.println("finalQuery------>"+finalQuery);
		     obj =  new String[] {companycode};
		  }
		  else {
			  newReferenceQuery=refrenceQuery.replace("'?%'", "?");
			  finalQuery=newReferenceQuery.replace("'&^'", "?");
		     System.out.println("finalQuery------>"+finalQuery);
		     obj =  new String[] {companycode,supplier_name+"%"};
		  }
		  if((supplier_name.isEmpty() || companycode !=null))  {
				 return  (ArrayList)  select.query(finalQuery,
						 obj,
						 new TripSheetDropDownVendorResultSet());
				  }
				  else if	(supplier_name !=null && companycode !=null) {
					  return  (ArrayList)  select.query(finalQuery,
								 obj,
								 new TripSheetDropDownVendorResultSet());
					  
				  }
		 }

		  return Collections.EMPTY_LIST;
	}
	
	@Override
	public List<Consignor_consignee> consigneeName(List<String> companyAndcnr_name) {
		
		String companycode="";
		String cnr_name="";
		JdbcTemplate select = new JdbcTemplate(dataSource);
		 String[] obj = null;
		 if(companyAndcnr_name.size()==2) {
				
		companycode=companyAndcnr_name.get(0);
		cnr_name=companyAndcnr_name.get(1);
		if(companycode.equals("")) {
			companycode=" ";
		    }
		cnr_name=cnr_name.replaceAll("\\s+", " ");
		 if(cnr_name.equals(" ")) {
			 cnr_name="";
		 } 
		 String newReferenceQuery="";
		 String finalQuery="";
		  String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no='0201V00270F'", String.class);
		  System.out.println("refrenceQuery------->"+refrenceQuery);
		  if(cnr_name.isEmpty() && companycode !=null) {
		     newReferenceQuery=refrenceQuery.replace("'?%'", "'%%'");
		     System.out.println("newReferenceQuery------>"+newReferenceQuery);
		     finalQuery=newReferenceQuery.replace("'&^'", "?");
		     System.out.println("finalQuery------>"+finalQuery); 
		     obj =  new String[] {companycode};
		  }
		  else {
		      newReferenceQuery=refrenceQuery.replace("'?%'", "?");
		      finalQuery=newReferenceQuery.replace("'&^'", "?");
		     System.out.println("finalQuery------>"+finalQuery);
		     obj =  new String[] {companycode,cnr_name+"%"};
		  }
		  if((cnr_name.isEmpty() || companycode !=null))  {
				 return  (ArrayList)  select.query(finalQuery,
						 obj,
						 new TripSheetDropDownConsignee());
				  }
				  else if	(cnr_name !=null && companycode !=null) {
					  return  (ArrayList)  select.query(finalQuery,
								 obj,
								 new TripSheetDropDownConsignee());
					  
				  }
		 }

			return Collections.EMPTY_LIST;
	}
	
	@Override
	public List<Route> route(List<String> companyAndroute_no) {
		String company="";
		String route_no="";
		JdbcTemplate select = new JdbcTemplate(dataSource);
		 String[] obj = null;
		 if(companyAndroute_no.size()==2) {
				
			 company=companyAndroute_no.get(1);
			 route_no=companyAndroute_no.get(0);
		if(company.equals("")) {
			company=" ";
		    }
		route_no=route_no.replaceAll("\\s+", " ");
		 if(route_no.equals(" ")) {
			 route_no="";
		 } 
		 String newReferenceQuery="";
		 String finalQuery="";
		  String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no='0201V00271F'", String.class);
		  System.out.println("refrenceQuery------->"+refrenceQuery);
		  if(route_no.isEmpty() && company !=null) {
		      newReferenceQuery=refrenceQuery.replace("'?%'", "'%%'");
		     System.out.println("newReferenceQuery------>"+newReferenceQuery);
		     finalQuery=newReferenceQuery.replace("'&^'", "?");
		     System.out.println("finalQuery------>"+finalQuery); 
		     obj =  new String[] {company};
		  }
		  else
		  {
			  newReferenceQuery=refrenceQuery.replace("'?%'", "?");
			     System.out.println("newReferenceQuery------>"+newReferenceQuery);
			     finalQuery=newReferenceQuery.replace("'&^'", "?");
			     System.out.println("finalQuery------>"+finalQuery);
			     obj =  new String[] {company,route_no+"%"};
		  }
		  if((route_no.isEmpty() || route_no !=null))  {
				 return  (ArrayList)  select.query(finalQuery,
						 obj,
						 new TripSheetDropDownRoute());
				  }
				  else if	(route_no !=null && route_no !=null) {
					  return  (ArrayList)  select.query(finalQuery,
								 obj,
								 new TripSheetDropDownRoute());
					  
				  }
		 }

			return Collections.EMPTY_LIST;
		 
	}
	@Override
	public List<Customer> cust(List<String> companyAndcust_name) {
		String company="";
		String cust_name="";
		JdbcTemplate select = new JdbcTemplate(dataSource);
		 String[] obj = null;
		 if(companyAndcust_name.size()==2) {
				
			 company=companyAndcust_name.get(1);
			 cust_name=companyAndcust_name.get(0);
		if(company.equals("")) {
			company=" ";
		    }
		cust_name=cust_name.replaceAll("\\s+", " ");
		 if(cust_name.equals(" ")) {
			 cust_name="";
		 } 
		 String newReferenceQuery="";
		 String finalQuery="";
		  String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no='0201V00272F'", String.class);
		  System.out.println("refrenceQuery------->"+refrenceQuery);
		  if(cust_name.isEmpty() && company !=null) {
		      newReferenceQuery=refrenceQuery.replace("'?%'", "'%%'");
		     System.out.println("newReferenceQuery------>"+newReferenceQuery);
		     finalQuery=newReferenceQuery.replace("'&^'", "?");
		     System.out.println("finalQuery------>"+finalQuery);
		     obj =  new String[] {company};
		  }
		  else {
			  newReferenceQuery=refrenceQuery.replace("'?%'", "?");
			     System.out.println("newReferenceQuery------>"+newReferenceQuery);
			     finalQuery=newReferenceQuery.replace("'&^'", "?");
			     System.out.println("finalQuery------>"+finalQuery);
			     obj =  new String[] {company,cust_name+"%"};
		  }
		  if((cust_name.isEmpty() || company !=null))  {
				 return  (ArrayList)  select.query(finalQuery,
						 obj,
						 new TripSheetDropDownCustomer());
				  }
				  else if	(cust_name !=null && company !=null) {
					  return  (ArrayList)  select.query(finalQuery,
								 obj,
								 new TripSheetDropDownCustomer());
					  
				  }  
		  }
		 return Collections.EMPTY_LIST;
	}
	@Override
	public List<Cust_selection> material(List<String> company) {
		String companycode=company.get(0);
		JdbcTemplate select = new JdbcTemplate(dataSource);
		  String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no='0301V00253F'", String.class);
		
		     String finalQuery=refrenceQuery.replace("'&^'", "?");
		     System.out.println("finalQuery------>"+finalQuery); 
		 return  (ArrayList)  select.query(finalQuery,
				 new Object[] {companycode},
				 new MaterailDropDown());
		
	}

	@Override
	public List<VendorSupplierDto> vehicleOwnHire(List<String> vehicletypeAndcompanyAndvehicle_no) {
		String vehicletype="";
				String company="";
				String vehicle_no="";
		JdbcTemplate select = new JdbcTemplate(dataSource);
		 String finalQuery="";
		String own="OWN";
		String hire="HIRE";
		String value="";
		 String refrenceQuery="";
		 String newReferenceQuery="";
		 String toExceute="";
		String[] vehiclearray = null;
		//String[] ownvehicle=null;
		if(vehicletypeAndcompanyAndvehicle_no.size()==3) {
		vehicletype=vehicletypeAndcompanyAndvehicle_no.get(1);
		 value="0201V02013"+vehicletype;
		  refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no= ?",new Object[] {value}, String.class);
				company=vehicletypeAndcompanyAndvehicle_no.get(2);
				vehicle_no=vehicletypeAndcompanyAndvehicle_no.get(0);
				vehicle_no=vehicle_no.replaceAll("\\s+", " ");
				 if(vehicle_no.equals(" ")) {
					 vehicle_no="";
				 } 
				 if(("0201V02013"+own).equalsIgnoreCase(value)) {
		 System.out.println("for own vehicle");
		  if(company !=null && vehicle_no !=null) {
		     newReferenceQuery=refrenceQuery.replace("'?%'", "?");
		     toExceute=newReferenceQuery.replace("'&^'", "?");
		     finalQuery =toExceute.replace("'%?%'", "?");
		     vehiclearray =  new String[]{company,"%"+vehicle_no+"%",vehicle_no+"%"};	
		     return  ( List<VendorSupplierDto>)  select.query(finalQuery,
					 vehiclearray,
					 new VendorHireOwnDropdown());
		  }
		  if(company !=null && vehicle_no.isEmpty()) {
			  System.out.println("for own vehicle if empty array pass");
			  newReferenceQuery=refrenceQuery.replace("'?%'", "'%%'");
			     toExceute=newReferenceQuery.replace("'&^'", "?");
			     finalQuery =toExceute.replace("'%?%'", "'%%'");
			     vehiclearray =  new String[]{company};	
				 return  ( List<VendorSupplierDto>)  select.query(finalQuery,
						 vehiclearray,
						 new VendorHireOwnDropdown());
		  }
		}
		
		
				 if(("0201V02013"+hire).equalsIgnoreCase(value)) {  
					
		if(company !=null && vehicle_no !=null) {
			 System.out.println("for hire vehicle not empty");
			 System.out.println("refrenceQuery------->"+refrenceQuery);
		      newReferenceQuery=refrenceQuery.replace("'%?%'", "?");
		     System.out.println("newReferenceQuery------>"+newReferenceQuery);
		      finalQuery=newReferenceQuery.replace("'&^'", "?");
		      vehiclearray =  new String[]{company,"%"+vehicle_no+"%"};	
		      return  ( List<VendorSupplierDto>)  select.query(finalQuery,
						 vehiclearray,
						 new VendorHireOwnDropdown());
		}
		if(company !=null && vehicle_no.isEmpty()) {
			 System.out.println("for hire vehicle empty array pass");
			 System.out.println("refrenceQuery------->"+refrenceQuery);
		      newReferenceQuery=refrenceQuery.replace("'%?%'", "'%%'");
		     System.out.println("newReferenceQuery------>"+newReferenceQuery);
		      finalQuery=newReferenceQuery.replace("'&^'", "?");
		      vehiclearray =  new String[]{company};	
		      return  ( List<VendorSupplierDto>)  select.query(finalQuery,
						 vehiclearray,
						 new VendorHireOwnDropdown());
		}	 
		 }
		}
		return Collections.EMPTY_LIST;
	}
	@Override
	public List<HireDetailsDto> driverNameFirst(List<String> company) {
		String companys="";
		String drivername="";
		JdbcTemplate select = new JdbcTemplate(dataSource);
		 String[] obj = null;
		 if(company.size()==2) {
				
			 companys=company.get(1);
			 drivername=company.get(0);
		if(companys.equals("")) {
			companys=" ";
		    }
		drivername=drivername.replaceAll("\\s+", " ");
		 if(drivername.equals(" ")) {
			 drivername="";
		 } 
		 String newReferenceQuery="";
		 String finalQuery="";
		//String companycode=company.get(0);
		
		  String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no='0201V00276'", String.class);
		  System.out.println("refrenceQuery------->"+refrenceQuery);
		  if(drivername.isEmpty() && company !=null) {
		      newReferenceQuery=refrenceQuery.replace("'?%'", "'%%'");
		     System.out.println("newReferenceQuery------>"+newReferenceQuery);
		     finalQuery=newReferenceQuery.replace("'&^'", "?");
		     System.out.println("finalQuery------>"+finalQuery);
		     obj =  new String[] {companys};
		  }
		  else {
			  newReferenceQuery=refrenceQuery.replace("'?%'", "?");
			     System.out.println("newReferenceQuery------>"+newReferenceQuery);
			     finalQuery=newReferenceQuery.replace("'&^'", "?");
			     System.out.println("finalQuery------>"+finalQuery);
			     obj =  new String[] {companys,drivername+"%"};
		  }
		  if((drivername.isEmpty() || companys !=null))  {
				 return  (ArrayList)  select.query(finalQuery,
						 obj,
						 new DriverNameFirstOwnDropdown());
				  }
				  else if(drivername !=null || companys !=null) {
					  return  (ArrayList)  select.query(finalQuery,
								 obj,
								 new DriverNameFirstOwnDropdown());
					  
				  }  
		
		 }
		 return Collections.EMPTY_LIST;
	}
	@Override
	public List<DriverChangeDto> driverNameSecond(String company, String employee_sec_name) {
		JdbcTemplate select = new JdbcTemplate(dataSource);
		  String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no='0201V00270F'", String.class);
		  System.out.println("refrenceQuery------->"+refrenceQuery);
		     String newReferenceQuery=refrenceQuery.replace("'%?%'", "?");
		     System.out.println("newReferenceQuery------>"+newReferenceQuery);
		     String finalQuery=newReferenceQuery.replace("'&^'", "?");
		     System.out.println("finalQuery------>"+finalQuery); 
		 return (List<DriverChangeDto>)  select.query(finalQuery,
				 new Object[] {company,"%"+employee_sec_name+"%"},
				 //new TripSheetDropDownVendorResultSet());
				 new DriverNameSecondOwnDropdown());
	}
	
	@Override
	public List<Acc_master> cashBank(List<String> companyAndCashBank) {
		String company="";
		String CashBank="";
		JdbcTemplate select = new JdbcTemplate(dataSource);
		 String[] obj = null;
		 if(companyAndCashBank.size()==2) {
				
			 company=companyAndCashBank.get(1);
			 CashBank=companyAndCashBank.get(0);
		if(company.equals("")) {
			company=" ";
		    }
		CashBank=CashBank.replaceAll("\\s+", " ");
		 if(CashBank.equals(" ")) {
			 CashBank="";
		 } 
		 String newReferenceQuery="";
		 String finalQuery="";
		  String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no='0201V00274F'", String.class);
		  System.out.println("refrenceQuery------->"+refrenceQuery);
		  if(CashBank.isEmpty() && company !=null) {
		      newReferenceQuery=refrenceQuery.replace("'?%'", "'%%'");
		     System.out.println("newReferenceQuery------>"+newReferenceQuery);
		     finalQuery=newReferenceQuery.replace("'&^'", "?");
		     System.out.println("finalQuery------>"+finalQuery);
		     obj =  new String[] {company};
		  }
		  else {
			  newReferenceQuery=refrenceQuery.replace("'?%'", "?");
			     System.out.println("newReferenceQuery------>"+newReferenceQuery);
			     finalQuery=newReferenceQuery.replace("'&^'", "?");
			     System.out.println("finalQuery------>"+finalQuery);
			     obj =  new String[] {company,CashBank+"%"};
		  }
		  if((CashBank.isEmpty() || company !=null))  {
				 return  (ArrayList)  select.query(finalQuery,
						 obj,
						 new CashBankDropDownCustomer());
				  }
				  else if	(company !=null && company !=null) {
					  return  (ArrayList)  select.query(finalQuery,
								 obj,
								 new CashBankDropDownCustomer());
					  
				  }  
		  }
		 return Collections.EMPTY_LIST;
	}
	@Override
	public List<Branchdetails> centerName(List<String> companyAndCenterno) {
		String company="";
		String Centername="";
		JdbcTemplate select = new JdbcTemplate(dataSource);
		 String[] obj = null;
		 if(companyAndCenterno.size()==2) {
				
			 company=companyAndCenterno.get(1);
			 Centername=companyAndCenterno.get(0);
		if(company.equals("")) {
			company=" ";
		    }
		Centername=Centername.replaceAll("\\s+", " ");
		 if(Centername.equals(" ")) {
			 Centername="";
		 } 
		 String newReferenceQuery="";
		 String finalQuery="";
		  String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no='0201V01573'", String.class);
		  System.out.println("refrenceQuery------->"+refrenceQuery);
		  if(Centername.isEmpty() && company !=null) {
		      newReferenceQuery=refrenceQuery.replace("'?%'", "'%%'");
		     System.out.println("newReferenceQuery------>"+newReferenceQuery);
		     finalQuery=newReferenceQuery.replace("'&^'", "?");
		     System.out.println("finalQuery------>"+finalQuery);
		     obj =  new String[] {company};
		  }
		  else {
			  newReferenceQuery=refrenceQuery.replace("'?%'", "?");
			     System.out.println("newReferenceQuery------>"+newReferenceQuery);
			     finalQuery=newReferenceQuery.replace("'&^'", "?");
			     System.out.println("finalQuery------>"+finalQuery);
			     obj =  new String[] {company,Centername+"%"};
		  }
		  if((Centername.isEmpty() || company !=null))  {
				 return  (ArrayList)  select.query(finalQuery,
						 obj,
						 new DropDownCentername());
				  }
				  else if	(company !=null && company !=null) {
					  return  (ArrayList)  select.query(finalQuery,
								 obj,
								 new DropDownCentername());
					  
				  }  
		  }
		 return Collections.EMPTY_LIST;
}
}
