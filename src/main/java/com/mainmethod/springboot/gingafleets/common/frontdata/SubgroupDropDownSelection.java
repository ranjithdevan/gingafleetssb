package com.mainmethod.springboot.gingafleets.common.frontdata;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;

public class SubgroupDropDownSelection implements RowMapper {
	
	
	@Override
    public Object mapRow(ResultSet rs, int line) throws SQLException {
		
        PersonResultSetExtractorDropDownSubGrpSelection extractor = new PersonResultSetExtractorDropDownSubGrpSelection();
        return extractor.extractData(rs);
    }

}
