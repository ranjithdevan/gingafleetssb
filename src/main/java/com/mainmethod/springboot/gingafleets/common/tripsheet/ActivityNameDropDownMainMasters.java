package com.mainmethod.springboot.gingafleets.common.tripsheet;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

public class ActivityNameDropDownMainMasters implements RowMapper {
	
	@Override
    public Object mapRow(ResultSet rs, int line) throws SQLException {
		
        PersonResultSetExtractorDropDownCustomerActivityNameMaster extractor = new PersonResultSetExtractorDropDownCustomerActivityNameMaster();
        return extractor.extractData(rs);
    }
}
