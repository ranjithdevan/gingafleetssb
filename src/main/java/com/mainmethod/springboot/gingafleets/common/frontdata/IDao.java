package com.mainmethod.springboot.gingafleets.common.frontdata;

import java.util.List;

import com.mainmethod.springboot.gingafleets.dto.Cust_selectionField;

import com.mainmethod.springboot.gingafleets.dto.Selection;


public interface IDao {
	//List<Cust_selectionField> selectAll();
List<Cust_selectionField> selectAll(int id);
	
	
	List<Selection> selectAllRules(List<String> name);
	
	List<Cust_selectionField> selectAllVechile(int id);
	
	
	List<Selection> selectSubGroupDropDown(List<String> svalue);
	
}
