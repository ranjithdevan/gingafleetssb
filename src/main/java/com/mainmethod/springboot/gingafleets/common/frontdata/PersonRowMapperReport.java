package com.mainmethod.springboot.gingafleets.common.frontdata;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class PersonRowMapperReport implements RowMapper{
	
	@Override
    public Object mapRow(ResultSet rs, int line) throws SQLException {
        PersonResultSetExtractorReport extractor = new PersonResultSetExtractorReport();
        return extractor.extractData(rs);
    }

}
