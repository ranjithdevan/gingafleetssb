package com.mainmethod.springboot.gingafleets.common.language;

import java.util.ArrayList;

public interface LanguageTranslate {
	
	ArrayList<Object> selectLanguage(String languagecode);

}
