package com.mainmethod.springboot.gingafleets.common.frontdata;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.mainmethod.springboot.gingafleets.dto.Cust_selectionField;
import com.mainmethod.springboot.gingafleets.dto.Form_template_report;

public class PersonResultSetExtractorReport implements ResultSetExtractor{
	//List<Form_template_report> form_template_report =new ArrayList();
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException  {
		Form_template_report selectionfield= new Form_template_report();
		//menu_id,field_name,label_name,field_type,mandatory_field,validation,filter	
		selectionfield.setMenu_id(rs.getInt(1));
		selectionfield.setField_name(rs.getString(2));
		System.out.println("field_name----->"+rs.getString(2));
		selectionfield.setLabel_name(rs.getString(3));
		selectionfield.setField_type(rs.getString(4));
		selectionfield.setMandatory_field(rs.getString(5));
		selectionfield.setValidation(rs.getString(6));
		if(rs.getString(7) !=null && !rs.getString(7).isEmpty()) {
		selectionfield.setFilter(rs.getString(7));
		System.out.println(rs.getString(7));
		}
		if(rs.getString(8) !=null && !rs.getString(8).isEmpty()) {
			selectionfield.setPattern(rs.getString(8));
			System.out.println(rs.getString(8));
			}
		if(rs.getString(9) !=null && !rs.getString(9).isEmpty()) {
			selectionfield.setValidationName(rs.getString(9));
			System.out.println(rs.getString(9));
			}
		if(rs.getString(10) !=null && !rs.getString(10).isEmpty()) {
			selectionfield.setValidatorErrorPath(rs.getString(10));
			System.out.println(rs.getString(10));
			}
		if(rs.getString(11) !=null && !rs.getString(11).isEmpty()) {
			selectionfield.setEventType(rs.getString(11));
			System.out.println(rs.getString(11));
			}
		if(rs.getString(12) !=null && !rs.getString(12).isEmpty()) {
			selectionfield.setEventValidation(rs.getString(12));
			System.out.println(rs.getString(12));
			}
		
		if(rs.getString(13) !=null && !rs.getString(13).isEmpty()) {
			selectionfield.setAutoType(rs.getString(13));
			System.out.println(rs.getString(13));
			}
		
		if(rs.getString(14) !=null && !rs.getString(14).isEmpty()) {
			selectionfield.setPatternErrorMessage(rs.getString(14));
			System.out.println(rs.getString(14));
			}
	//	form_template_report.add(selectionfield);
		return selectionfield;
	
	}

}
