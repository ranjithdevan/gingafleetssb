package com.mainmethod.springboot.gingafleets.common.vehicleinfo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.mainmethod.springboot.gingafleets.dto.Branchdetails;
import com.mainmethod.springboot.gingafleets.dto.Selection;

public class PersonResultSetExtractorDropDownCustomerno implements ResultSetExtractor {
	
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		Branchdetails selection= new Branchdetails();
		selection.setCenter_id(rs.getString(1));
		
		selection.setCenter_name(rs.getString(2));
		System.out.println("centername----->"+rs.getString(2));
		
		
		return selection;
	}
}
