package com.mainmethod.springboot.gingafleets.common.vehicleinfo;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;

import com.mainmethod.springboot.gingafleets.common.frontdata.PersonResultSetExtractorDropDown;

public class VehicleInfoDropDownCustomerNO implements RowMapper {
	
	
	@Override
    public Object mapRow(ResultSet rs, int line) throws SQLException {
		
        PersonResultSetExtractorDropDownCustomerno extractor = new PersonResultSetExtractorDropDownCustomerno();
        return extractor.extractData(rs);
    }


}
