package com.mainmethod.springboot.gingafleets.common.all;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.mainmethod.springboot.gingafleets.dto.TableHeadDto;

public class TableHeader {
	
	String selectioncode="Selection Code";
	String subGroupName="Sub Group Name";
	String GroupName="Group Name";
	String value="Value";
	String view="view";
	String update="Update";
	String delete="Delete";
	String vehiclecode="Vehicle Code";
	String vehicleno="Vehicle No";
	String alias="Alias Name";
	String centerName="Center Name";
	String vehicletype="vehicle Type";
	String dateOfPurchase="Date Of Purchase";
	String fuelType="FuelType";
	String makersClass="Makers Class";
	String makersName="Makers Name";
	String tripsheetcode="Tripsheetcode";
	String tripsheetno="Tripsheetno";
	String date="Date";
	String consigneename="Consigneename";
	String material="Material";
	String quantity="Quantity";
	
	
	
	public List<String> list;
	
	public void headingselectionMaster(){
		list = new ArrayList<String>();
		list.add(selectioncode);
		list.add(subGroupName);
		list.add(GroupName);
		list.add(value);
		list.add(view);
		list.add(update);
		list.add(delete);
		list.add(vehiclecode);
		list.add(vehicleno);
		list.add(dateOfPurchase);
		list.add(makersClass);
		list.add(makersName);
		list.add(fuelType);
		list.add(tripsheetcode);
		list.add(tripsheetno);
		list.add(consigneename);
		list.add(material);
		list.add(quantity);
	}
	public List<String> getList()
	{
		return list;
	}
		


}
