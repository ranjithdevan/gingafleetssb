package com.mainmethod.springboot.gingafleets.common.tripsheet;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.mainmethod.springboot.gingafleets.dto.Consignor_consignee;
import com.mainmethod.springboot.gingafleets.dto.Route;

public class PersonResultSetExtractorDropDownRoute implements ResultSetExtractor {
		
		@Override
		public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
			
			
			Route route= new Route();  
			route.setRoute_sysno(rs.getString(1));
			
			route.setRoute_no(rs.getString(2));
			System.out.println("---------------->rs.getSring(2)"+rs.getString(2));
			return route;
		}

}
