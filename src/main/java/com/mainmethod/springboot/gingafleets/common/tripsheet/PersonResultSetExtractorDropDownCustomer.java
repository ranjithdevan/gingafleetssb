package com.mainmethod.springboot.gingafleets.common.tripsheet;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.mainmethod.springboot.gingafleets.dto.Customer;
import com.mainmethod.springboot.gingafleets.dto.Route;

public class PersonResultSetExtractorDropDownCustomer implements ResultSetExtractor {
		//List<Customer> lisst=new ArrayList<>();
		@Override
		public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
			
			
			Customer cust= new Customer();  
			cust.setCust_name(rs.getString(1));
			
			cust.setCust_code(rs.getString(2));
			System.out.println("---------------->rs.getSring(2)"+rs.getString(2));
		//	lisst.add(cust);
			return cust;
		}

}
