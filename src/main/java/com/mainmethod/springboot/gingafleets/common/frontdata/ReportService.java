package com.mainmethod.springboot.gingafleets.common.frontdata;

import java.util.List;

import com.mainmethod.springboot.gingafleets.dto.Cust_selectionField;
import com.mainmethod.springboot.gingafleets.dto.Form_template_report;

public interface ReportService {
	
	List<Form_template_report> selectAllReport(int id);

}
