package com.mainmethod.springboot.gingafleets.common.vehicleinfo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestBody;

import com.mainmethod.springboot.gingafleets.common.frontdata.SubgroupDropDown;
import com.mainmethod.springboot.gingafleets.common.tripsheet.ActivityNameDropDownMainMasters;
import com.mainmethod.springboot.gingafleets.common.tripsheet.DropDownCentername;
import com.mainmethod.springboot.gingafleets.common.tripsheet.MaterailDropDown;
import com.mainmethod.springboot.gingafleets.common.tripsheet.TripSheetDropDownCustomer;
import com.mainmethod.springboot.gingafleets.dto.Branchdetails;
import com.mainmethod.springboot.gingafleets.dto.Maintenance;
import com.mainmethod.springboot.gingafleets.dto.Selection;
import com.mainmethod.springboot.gingafleets.model.Cust_selection;

public class DropDownVehicleInfoServiceImpl implements DropDownVehicleInfoService{

private DataSource dataSource;
	
	
	public void setDataSource(DataSource ds) {	
        dataSource = ds;	
    }
	@Override
	public List<Selection> SpeedoMeter() {
		JdbcTemplate select = new JdbcTemplate(dataSource);
		  String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no='0301V0001011'", String.class);
		  System.out.println("refrenceQuery------->"+refrenceQuery);
		 return  (ArrayList)  select.query(refrenceQuery,
				 new SubgroupDropDown());
	}
	@Override
	public List<Selection> leaseType() {
		JdbcTemplate select = new JdbcTemplate(dataSource);
		  String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no='0301V00043'", String.class);
		  System.out.println("refrenceQuery------->"+refrenceQuery);
		  //A000198
		 return  (ArrayList)  select.query(refrenceQuery,
				 new SubgroupDropDown());
	}
	@Override
	public ArrayList<String> make(List<String> company) {
		String reference=company.get(0);
		//if(referene.contains(" " "));
		System.out.println("reference---->"+reference);
		String companycode=company.get(1);
		System.out.println("companycode---->"+companycode);
		String finalQuery="";
		JdbcTemplate select = new JdbcTemplate(dataSource);
		
		  String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no=?",new Object[] {reference}, String.class);
		
		  System.out.println("Combo fun calling ");
		if(companycode !=null && reference!=null && refrenceQuery.contains("'&^'")) {
		 // if(refrenceQuery.contains("'&^'")) {
			companycode=company.get(1);
			finalQuery =refrenceQuery.replace("'&^'", "?");
			return  (ArrayList<String>)  select.query(finalQuery,
					 new Object[] {companycode},
					 new SubgroupDropDown());
		//}
		}
		else if(companycode.isEmpty()) {
		finalQuery=	refrenceQuery;
		 return  (ArrayList)  select.query(finalQuery,
				 new SubgroupDropDown());
		}
		 
		else {
			finalQuery=	refrenceQuery;
			 return  (ArrayList)  select.query(finalQuery,
					 new SubgroupDropDown());
		}
		 
	}
	
	@Override
	public List<Cust_selection> model(List<String> company) {
		String companycode=company.get(0);
		JdbcTemplate select = new JdbcTemplate(dataSource);
		  String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no='0301V00024'", String.class);
		
		     String finalQuery=refrenceQuery.replace("'&^'", "?");
		     System.out.println("finalQuery------>"+finalQuery); 
		 return  (ArrayList)  select.query(finalQuery,
				 new Object[] {companycode},
				 new MaterailDropDown());
	}
	@Override
	public List<Branchdetails> centerno(List<String> companyAndcentername) {
		String company="";
		String Centername="";
		JdbcTemplate select = new JdbcTemplate(dataSource);
		 String[] obj = null;
		 if(companyAndcentername.size()==2) {
				
			 company=companyAndcentername.get(1);
			 Centername=companyAndcentername.get(0);
		if(company.equals("")) {
			company=" ";
		    }
		Centername=Centername.replaceAll("\\s+", " ");
		 if(Centername.equals(" ")) {
			 Centername="";
		 } 
		 String newReferenceQuery="";
		 String finalQuery="";
		  String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no='0201V00038'", String.class);
		  System.out.println("refrenceQuery------->"+refrenceQuery);
		  if(Centername.isEmpty() && company !=null) {
		      newReferenceQuery=refrenceQuery.replace("'?%'", "'%%'");
		     System.out.println("newReferenceQuery------>"+newReferenceQuery);
		     finalQuery=newReferenceQuery.replace("'&^'", "?");
		     System.out.println("finalQuery------>"+finalQuery);
		     obj =  new String[] {company};
		  }
		  if(!Centername.isEmpty() && company !=null) {
			  newReferenceQuery=refrenceQuery.replace("'?%'", "?");
			     System.out.println("newReferenceQuery------>"+newReferenceQuery);
			     finalQuery=newReferenceQuery.replace("'&^'", "?");
			     System.out.println("finalQuery------>"+finalQuery);
			     obj =  new String[] {company,Centername+"%"};
		  }
		   if((Centername.isEmpty() || company !=null))  {
				 return  (ArrayList)  select.query(finalQuery,
						 obj, new VehicleInfoDropDownCustomerNO());
						// new DropDownCentername());
				  }
				   if	(Centername !=null && company !=null) {
					  return  (ArrayList)  select.query(finalQuery,
								 obj, new VehicleInfoDropDownCustomerNO());
							//	 new DropDownCentername());
					  
				  }  
				   
		  }
		 return Collections.EMPTY_LIST;
}
		 /*return  (ArrayList)  select.query(finalQuery,
				 new Object[] {companycode,centername+"%"},
				 new VehicleInfoDropDownCustomerNO()); */
	
	
	@Override
	public List<Selection> vehicletype(List<String> company) {
		String companycode=company.get(0);
		JdbcTemplate select = new JdbcTemplate(dataSource);
		  String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no='0301V00041'", String.class);
		  System.out.println("refrenceQuery------->"+refrenceQuery);
		  String finalQuery=refrenceQuery.replace("'&^'", "?");
		     System.out.println("finalQuery------>"+finalQuery); 
		 return  (ArrayList)  select.query(finalQuery, new Object[] {companycode},
				 new SubgroupDropDown());
		
	}
	@Override
	public List<Selection> maintanGrp() {
		JdbcTemplate select = new JdbcTemplate(dataSource);
		  String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no='0301V00042'", String.class);
		  System.out.println("refrenceQuery------->"+refrenceQuery);
		 return  (ArrayList)  select.query(refrenceQuery,
				 new SubgroupDropDown());
	}
	@Override
	public List<Cust_selection> fuelType(List<String> company) {
		String companycode=company.get(0);
		JdbcTemplate select = new JdbcTemplate(dataSource);
		  
		  String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no='0301V00044'", String.class);
		  System.out.println("refrenceQuery------->"+refrenceQuery);
		  String finalQuery=refrenceQuery.replace("'&^'", "?");
		     System.out.println("finalQuery------>"+finalQuery);
		 return  (ArrayList)  select.query(finalQuery,new Object[] {companycode},
				 new MaterailDropDown());
	}
	
	
	@Override
	public List<Maintenance> activityName(List<String> companyAndactivity_nameAndmaint_group) {
		String companycode="";
		String activity_names="";
		String maint_groups="";
		String newReferenceQuery="";
		JdbcTemplate select = new JdbcTemplate(dataSource);
		 String[] obj = null;
		 String toExceute="";
		 String finalQuery="";
		 if(companyAndactivity_nameAndmaint_group.size()==3) {
		 companycode=companyAndactivity_nameAndmaint_group.get(2);
		activity_names=companyAndactivity_nameAndmaint_group.get(0);
		maint_groups=companyAndactivity_nameAndmaint_group.get(1);
		//JdbcTemplate select = new JdbcTemplate(dataSource);
		if(maint_groups.equals("")) {
			maint_groups=" ";
		    }
		if(companycode.equals("")) {
			companycode="";
		}
		  
		   activity_names=activity_names.replaceAll("\\s+", " ");
			 if(activity_names.equals(" ")) {
				 activity_names="";
			 } 
			 String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no='0201V00039'", String.class);
			 if(activity_names.isEmpty() && companycode !=null && maint_groups!=null ) {
				 newReferenceQuery=refrenceQuery.replace("'?%'", "'%%'");
				 finalQuery=newReferenceQuery.replace("'&^'", "?");
				 toExceute=finalQuery.replace("'?'", "?");
				 obj =  new String[] {companycode,maint_groups};
			 }
			 else {
				 newReferenceQuery=refrenceQuery.replace("'?%'", "?");
			     System.out.println("newReferenceQuery------>"+newReferenceQuery);
			     finalQuery=newReferenceQuery.replace("'&^'", "?");
			     toExceute =finalQuery.replace("'?'", "?");
			     System.out.println("finalQuery------>"+toExceute); 
			     obj =  new String[] {companycode,activity_names+"%",maint_groups};
			
			 
			 }
			 if((activity_names.isEmpty() || companycode !=null  && maint_groups!=null))  {
				 return  ( List<Maintenance>)  select.query(toExceute,
						 obj,
						 new ActivityNameDropDownMainMasters());
				  }
			 else if	(companycode !=null && maint_groups !=null && activity_names!=null) {
				  return  ( List<Maintenance>)  select.query(toExceute,
							 obj,
							 new ActivityNameDropDownMainMasters());
				  
			  }
		    
		 }
		 return Collections.EMPTY_LIST;
	}
	

}
