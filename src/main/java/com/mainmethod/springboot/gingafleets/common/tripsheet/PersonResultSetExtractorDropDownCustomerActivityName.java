package com.mainmethod.springboot.gingafleets.common.tripsheet;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.mainmethod.springboot.gingafleets.dto.Customer;
import com.mainmethod.springboot.gingafleets.dto.Maint_act_masters_lines;

public class PersonResultSetExtractorDropDownCustomerActivityName implements ResultSetExtractor {
	
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		
		Maint_act_masters_lines maint_act_masters_lines= new Maint_act_masters_lines();  
		maint_act_masters_lines.setPeriod_kms(rs.getString(1));
		
		maint_act_masters_lines.setPeriod_days(rs.getString(2));
		System.out.println("---------------->rs.getSring(2)"+rs.getString(2));
		maint_act_masters_lines.setRemind_kms(rs.getString(3));
		maint_act_masters_lines.setRemind_days(rs.getString(4));
		return maint_act_masters_lines;
	}

}
