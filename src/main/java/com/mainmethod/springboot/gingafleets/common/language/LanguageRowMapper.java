package com.mainmethod.springboot.gingafleets.common.language;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;

import com.mainmethod.springboot.gingafleets.common.frontdata.PersonResultSetExtractor;

public class LanguageRowMapper implements RowMapper {
	@Override
    public Object mapRow(ResultSet rs, int line) throws SQLException {
        PersonResultSetExtractorlanguage extractor = new PersonResultSetExtractorlanguage();
        return extractor.extractData(rs);
    }
	

}
