package com.mainmethod.springboot.gingafleets.common.tripsheet;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.mainmethod.springboot.gingafleets.dto.Selection;

public class PersonResultSetExtractorDropDownConsigtype implements ResultSetExtractor {
	
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		Selection selection= new Selection();
		
		
		selection.setSub_group(rs.getString(2));
		
		return selection;
	}
}