package com.mainmethod.springboot.gingafleets.common.frontdata;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.mainmethod.springboot.gingafleets.dto.Cust_selectionField;



public class PersonResultSetExtractor implements ResultSetExtractor<Object> {
	
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		
	
	Cust_selectionField selectionfield= new Cust_selectionField();
	selectionfield.setMenu_id(rs.getInt(1));
	selectionfield.setField_name(rs.getString(2));
	selectionfield.setLabel_name(rs.getString(3));
	selectionfield.setField_type(rs.getString(4));
	selectionfield.setMandatory_field(rs.getString(5));
	selectionfield.setRules_on_insert(rs.getString(6));
	selectionfield.setValidation(rs.getString(7));
	//System.out.println("checking fr filter value its coming or not--->"+rs.getString(8));
	if(rs.getString(8) !=null && !rs.getString(8).isEmpty()) {
	selectionfield.setFilter(rs.getString(8));
	System.out.println("checking fr filter value its coming or not--->"+rs.getString(8));
	}
	if(rs.getString(9) !=null && !rs.getString(9).isEmpty()) {
	selectionfield.setGet_from_table(rs.getString(9));
	System.out.println("rs.getstring----get from table for combo:"+rs.getString(9));
	}
	if(rs.getString(10) !=null && !rs.getString(10).isEmpty()) {
		selectionfield.setPattern(rs.getString(10));
		System.out.println("rs.getstring----get from table for pattern:"+rs.getString(10));
		}
	if(rs.getString(11) !=null && !rs.getString(11).isEmpty()) {
		selectionfield.setValidationName(rs.getString(11));
		System.out.println("rs.getstring----get from table for ValidationName:"+rs.getString(11));
		}
	if(rs.getString(12) !=null && !rs.getString(12).isEmpty()) {
		selectionfield.setValidatorErrorPath(rs.getString(12));
		System.out.println("rs.getstring----get from table for ValidatorErrorPath:"+rs.getString(12));
		}
	if(rs.getString(13) !=null && !rs.getString(13).isEmpty()) {
		selectionfield.setEventValidation(rs.getString(13));
		System.out.println("rs.getstring----get from table for setEventValidation:"+rs.getString(13));
		}
	if(rs.getString(14) !=null && !rs.getString(14).isEmpty()) {
		selectionfield.setEventType(rs.getString(14));
		System.out.println("rs.getstring----get from table for setEventType:"+rs.getString(14));
		}
	if(rs.getString(15) !=null && !rs.getString(15).isEmpty()) {
		selectionfield.setAutoType(rs.getString(15));
		System.out.println("rs.getstring----get from table for setAutoType:"+rs.getString(15));
		}
	
	if(rs.getString(16) !=null && !rs.getString(16).isEmpty()) {
		selectionfield.setPatternErrorMessage(rs.getString(16));
		System.out.println("rs.getstring----get from table for setPatternErrorMessage:"+rs.getString(16));
		}
	return selectionfield;
	
	
	}
}
