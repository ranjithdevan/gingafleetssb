package com.mainmethod.springboot.gingafleets.common.frontdata;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.mainmethod.springboot.gingafleets.dto.Cust_selectionField;
import com.mainmethod.springboot.gingafleets.dto.Reference;
import com.mainmethod.springboot.gingafleets.dto.Selection;
import com.mainmethod.springboot.gingafleets.model.Vehicle;

public class CustFieldsDao implements IDao {
	//queues = (List<Selection>)  Arrays.asList(resultArray);
	//List<List<String>> stuff = new ArrayList<List<String>>();
	private DataSource dataSource;
	
	
	public void setDataSource(DataSource ds) {	
        dataSource = ds;	
    }

	public List<Cust_selectionField> selectAll(int id) {
		
        JdbcTemplate select = new JdbcTemplate(dataSource);
       return select.query("select menu_id,field_name,label_name,field_type,mandatory_field,rules_on_insert,validation,filter,get_from_table,pattern,validationName,validatorErrorPath,eventType,eventValidation,autoType,patternErrorMessage from live_saas_design.form_template where menu_id=?",
        	new Object[] {id},
        		new PersonRowMapper());
      
    }
	
	public List<Selection> selectAllRules(List<String> name) {
		String str =name.get(0);
		 str=str.replaceAll("\\s+", " ");
		 if(str.equals(" ")) {
			 str="";
		 } 
		 String[] obj = null;
		 String newReferenceQuery="";
	        JdbcTemplate select = new JdbcTemplate(dataSource);
	       String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no='0201V00031'", String.class);
	       System.out.println("refrenceQuery------->"+refrenceQuery);
	     if(str.isEmpty()) {
	    	 newReferenceQuery=refrenceQuery.replace("'?%'", "'%%'");
	    	 System.out.println("newReferenceQuery------>"+newReferenceQuery);
	    	 obj =  new String[] {};
	     }
	     else {
	    	 newReferenceQuery=refrenceQuery.replace("'?%'", "?");
	    	 System.out.println("newReferenceQuery------>"+newReferenceQuery);
	    	 obj =  new String[]{str+"%"};
	     }
	 return  (ArrayList)  select.query(newReferenceQuery,
			 obj,
			 new DropDownResultSet());
	    }

	@Override
	public List<Cust_selectionField> selectAllVechile(int id) {
		 JdbcTemplate select = new JdbcTemplate(dataSource);
	       return select.query("select menu_id,field_name,label_name,field_type,mandatory_field,rules_on_insert,validation,filter,get_from_table,pattern,validationName,validatorErrorPath,eventType,eventValidation,autoType,patternErrorMessage from live_saas_design.form_template where menu_id=?",
	        	new Object[] {id},
	        		new PersonRowMapper());
	}

	@Override
	public List<Selection> selectSubGroupDropDown(List<String> svalue) {
		String svalue1 ="";
		 String svalue2="";
		 String finalQuery="";
		 JdbcTemplate select = new JdbcTemplate(dataSource);
		 String[] obj = null;
		 System.out.println("svalue---size"+svalue.size());
		if(svalue.size()==2) {
			svalue1=   svalue.get(0);
  
   System.out.println("svalue1------------------->"+svalue1);
   
    svalue2 =svalue.get(1);
   if(svalue2.equals("")) {
 	   svalue2=" ";
    }
   System.out.println("svalue2------------------->"+svalue2);
   svalue1=svalue1.replaceAll("\\s+", " ");
	 if(svalue1.equals(" ")) {
		 svalue1="";
	 } 
	
	 String newReferenceQuery="";
	 
		
		  String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no='0201V00032'", String.class);
		 // newReferenceQuery=refrenceQuery.replace("'?%'", "'%%'");
		  
		 if(svalue1.isEmpty() && svalue2 !=null) {
		    	 newReferenceQuery=refrenceQuery.replace("'?%'", "'%%'");
		    	 System.out.println("newReferenceQuery------>"+newReferenceQuery);
		    	 finalQuery=newReferenceQuery.replace("'?'", "?");
		    	 obj =  new String[] {svalue2};
		     }
		  else {
		      newReferenceQuery=refrenceQuery.replace("'?%'", "?");
		      finalQuery=newReferenceQuery.replace("'?'", "?");
		     System.out.println("finalQuery------>"+finalQuery);
		     obj =  new String[] {svalue1+"%",svalue2};
		  }
		 if((svalue1.isEmpty() || svalue2 !=null))  {
			 return  (ArrayList)  select.query(finalQuery,
					 obj,
					 new SubgroupDropDownSelection());
			  }
			  else if	(svalue1 !=null && svalue2 !=null) {
				  return  (ArrayList)  select.query(finalQuery,
							 obj,
							 new SubgroupDropDownSelection());
				  
			  }
		}

		return Collections.EMPTY_LIST;

		  
		    
	}
	
	

}
