package com.mainmethod.springboot.gingafleets.common.tripsheet;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.mainmethod.springboot.gingafleets.dto.Acc_master;
import com.mainmethod.springboot.gingafleets.dto.Branchdetails;

public class PersonResultSetExtractorDropDowncentername implements ResultSetExtractor {
	//List<Acc_master> lisst=new ArrayList<>();
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		
		Branchdetails cust= new Branchdetails();  
		
		cust.setCenter_name(rs.getString(1));
		System.out.println("---------------->rs.getSring(1)"+rs.getString(1));

		return cust;
	}
}