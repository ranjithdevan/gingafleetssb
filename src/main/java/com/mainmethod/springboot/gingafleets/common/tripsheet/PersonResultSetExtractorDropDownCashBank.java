package com.mainmethod.springboot.gingafleets.common.tripsheet;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.mainmethod.springboot.gingafleets.dto.Acc_master;
import com.mainmethod.springboot.gingafleets.dto.Customer;

public class PersonResultSetExtractorDropDownCashBank implements ResultSetExtractor {
	//List<Acc_master> lisst=new ArrayList<>();
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		
		Acc_master cust= new Acc_master();  
		cust.setAc_head_name(rs.getString(1));
		System.out.println("---------------->rs.getSring(1)"+rs.getString(1));
		cust.setCode_no(rs.getString(2));
		System.out.println("---------------->rs.getSring(2)"+rs.getString(2));
		//cust.setAcc_no(rs.getString(3));
		//lisst.add(cust);
		return cust;
	}

}
