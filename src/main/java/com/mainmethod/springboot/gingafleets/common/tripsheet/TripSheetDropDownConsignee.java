package com.mainmethod.springboot.gingafleets.common.tripsheet;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;

public class TripSheetDropDownConsignee implements RowMapper {
	
	@Override
    public Object mapRow(ResultSet rs, int line) throws SQLException {
		
        PersonResultSetExtractorDropDownVendorConsigee extractor = new PersonResultSetExtractorDropDownVendorConsigee();
        return extractor.extractData(rs);
    }

}
