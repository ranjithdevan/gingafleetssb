package com.mainmethod.springboot.gingafleets.common.tripsheet;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;

public class DriverNameFirstOwnDropdown implements RowMapper {
	
	@Override
    public Object mapRow(ResultSet rs, int line) throws SQLException {
		
        PersonResultSetExtractorDropDownDriverNameFirst extractor = new PersonResultSetExtractorDropDownDriverNameFirst();
        return extractor.extractData(rs);
    }

}
