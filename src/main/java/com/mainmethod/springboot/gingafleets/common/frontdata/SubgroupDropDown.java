package com.mainmethod.springboot.gingafleets.common.frontdata;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class SubgroupDropDown implements RowMapper {
	
	
	@Override
    public Object mapRow(ResultSet rs, int line) throws SQLException {
		
        PersonResultSetExtractorDropDownSubGrp extractor = new PersonResultSetExtractorDropDownSubGrp();
        return extractor.extractData(rs);
    }
	}
