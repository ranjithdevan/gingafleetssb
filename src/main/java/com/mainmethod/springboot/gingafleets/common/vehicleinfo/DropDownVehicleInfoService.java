package com.mainmethod.springboot.gingafleets.common.vehicleinfo;

import java.util.List;



import com.mainmethod.springboot.gingafleets.dto.Branchdetails;
import com.mainmethod.springboot.gingafleets.dto.Maintenance;
import com.mainmethod.springboot.gingafleets.dto.Selection;
import com.mainmethod.springboot.gingafleets.model.Cust_selection;
import com.mainmethod.springboot.gingafleets.model.Load_manifest;

public interface DropDownVehicleInfoService {
	
	
	List<?> make(List<String> company);
	
	List<Cust_selection> model(List<String> company);
	
	List<Selection> SpeedoMeter();
	
	List<Selection> leaseType();
	
	List<Branchdetails> centerno(List<String> companyAndcentername);
	
	List<Selection> vehicletype(List<String> company);
	
	List<Selection> maintanGrp();
	
	List<Cust_selection> fuelType(List<String> company);
	
	List<Maintenance> activityName(List<String> companyAndactivity_nameAndmaint_group);
	
	

}
