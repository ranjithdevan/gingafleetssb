package com.mainmethod.springboot.gingafleets.common.json;

import org.json.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class Json {
	
	public JSONObject getResponseObject(String statusMessage, int statusCode, Object object){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status", statusMessage);
        jsonObject.put("statusCode", statusCode);
        jsonObject.put("info", object);
        return jsonObject;
    }

}
