package com.mainmethod.springboot.gingafleets.common.tripsheet;

import java.util.List;

import com.mainmethod.springboot.gingafleets.dto.Acc_master;
import com.mainmethod.springboot.gingafleets.dto.Branchdetails;
import com.mainmethod.springboot.gingafleets.dto.Consignor_consignee;
import com.mainmethod.springboot.gingafleets.dto.Customer;
import com.mainmethod.springboot.gingafleets.dto.DriverChangeDto;
import com.mainmethod.springboot.gingafleets.dto.HireDetailsDto;
import com.mainmethod.springboot.gingafleets.dto.Maintenance;
import com.mainmethod.springboot.gingafleets.dto.Route;
import com.mainmethod.springboot.gingafleets.dto.Selection;
import com.mainmethod.springboot.gingafleets.dto.Vendor;
import com.mainmethod.springboot.gingafleets.dto.VendorSupplierDto;
import com.mainmethod.springboot.gingafleets.model.Cust_selection;

public interface DropDownTripsheetService {
	
	List<Selection> vehicletype();
	
	List<Selection> consigType();
	
	List<Selection> calcType();
	
	List<Vendor> transporterName(List<String> companyAndsupplier_name);
	
	List<Consignor_consignee> consigneeName(List<String> companyAndcnr_name);
	
	List<Route> route(List<String> companyAndroute_no);
	
	List<Customer> cust(List<String> companyAndcust_name);
	
	List<Cust_selection> material(List<String> company);
	
	List<Acc_master> cashBank(List<String> companyAndCashBank);
	
	List<VendorSupplierDto>  vehicleOwnHire(List<String> vehicletypeAndcompanyAndvehicle_no);

	List<HireDetailsDto>  driverNameFirst(List<String> company);
	
	List<DriverChangeDto>  driverNameSecond(String company,String employee_sec_name);
	
	List<Branchdetails> centerName(List<String> companyAndCenterno);

}
