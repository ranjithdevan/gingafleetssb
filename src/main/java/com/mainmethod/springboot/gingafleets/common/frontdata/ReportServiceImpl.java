package com.mainmethod.springboot.gingafleets.common.frontdata;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.mainmethod.springboot.gingafleets.dto.Cust_selectionField;
import com.mainmethod.springboot.gingafleets.dto.Form_template_report;

public class ReportServiceImpl implements ReportService {
	
private DataSource dataSource;

	
	
	public void setDataSource(DataSource ds) {	
        dataSource = ds;	
    }
	
public List<Form_template_report> selectAllReport(int id) {
	
        JdbcTemplate select = new JdbcTemplate(dataSource);
       return select.query("select menu_id,field_name,label_name,field_type,mandatory_field,validation,filter,pattern,validationname,validatorerrorpath,eventType,eventvalidation,autoType,patternErrorMessage from live_saas_design.form_template_report where menu_id=? ORDER BY position",
        	new Object[] {id},
        		new PersonRowMapperReport());
      
    }

}
