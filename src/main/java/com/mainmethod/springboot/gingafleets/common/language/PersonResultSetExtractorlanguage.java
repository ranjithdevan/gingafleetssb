package com.mainmethod.springboot.gingafleets.common.language;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;


import com.mainmethod.springboot.gingafleets.model.Form_lang_label;

public class PersonResultSetExtractorlanguage implements ResultSetExtractor<Object> {
	
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		
	
		Form_lang_label form_lang_label= new Form_lang_label();
		form_lang_label.setLabel_name(rs.getString(1));
		form_lang_label.setLanguage_code(rs.getString(2));
		form_lang_label.setRel_lang_label_name(rs.getString(3));
		return form_lang_label;
	
	}
}