package com.mainmethod.springboot.gingafleets.common.frontdata;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.mainmethod.springboot.gingafleets.dto.Selection;

public class PersonResultSetExtractorDropDownSubGrpSelection implements ResultSetExtractor {
	
	//List<Selection> retsele =new ArrayList();
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		Selection selection= new Selection();
		
		selection.setSvalue(rs.getString(1));
		System.out.println(rs.getString(1));
	//	retsele.add(selection);
		return selection;
	}

}
