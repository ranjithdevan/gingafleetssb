package com.mainmethod.springboot.gingafleets.common.tripsheet;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.mainmethod.springboot.gingafleets.dto.HireDetailsDto;
import com.mainmethod.springboot.gingafleets.dto.Selection;
import com.mainmethod.springboot.gingafleets.dto.Vendor;

public class PersonResultSetExtractorDropDownVendor implements ResultSetExtractor {
	//List<Vendor> maint_acts=new ArrayList<>();
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		Vendor vendor= new Vendor();
		
		vendor.setSupplier_name(rs.getString(1));
		System.out.println("---------------->rs.getSring(1)"+rs.getString(1));
		
		vendor.setSupplier_code(rs.getString(2));
		System.out.println("---------------->rs.getSring(2)"+rs.getString(2));
		//maint_acts.add(vendor);
		return vendor;
	}
}
