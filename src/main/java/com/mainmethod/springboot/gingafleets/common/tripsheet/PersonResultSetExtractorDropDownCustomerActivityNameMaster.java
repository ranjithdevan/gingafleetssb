package com.mainmethod.springboot.gingafleets.common.tripsheet;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.mainmethod.springboot.gingafleets.dto.Maint_act_masters;
import com.mainmethod.springboot.gingafleets.dto.Maint_act_masters_lines;
import com.mainmethod.springboot.gingafleets.dto.Maintenance;

public class PersonResultSetExtractorDropDownCustomerActivityNameMaster implements ResultSetExtractor {
	//List<Maintenance> maint_acts=new ArrayList<>();
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		Maintenance maint_acts_masters=new Maintenance();
		
		maint_acts_masters.setMaint_group(rs.getString(1));
		System.out.println("rs.get1------>"+rs.getString(1));
		
		maint_acts_masters.setActivity_name(rs.getString(2));
		System.out.println("rs.get2----->"+rs.getString(2));
		System.out.println("check get by name----->"+maint_acts_masters.getActivity_name());
		
		maint_acts_masters.setMaint_code(rs.getString(3));
		System.out.println("rs.get3------>"+rs.getString(3));
		
		maint_acts_masters.setPeriod_kms(rs.getString(4));
		System.out.println("rs.get4------>"+rs.getString(4));
		
		maint_acts_masters.setPeriod_days(rs.getString(5));
		System.out.println("rs.get5------>"+rs.getString(5));
		
		maint_acts_masters.setRemind_kms(rs.getString(6));
		System.out.println("rs.get6------>"+rs.getString(6));
		
		maint_acts_masters.setRemind_days(rs.getString(7));
		
		System.out.println("rs.get7------>"+rs.getString(7));
		//maint_acts_masters.add(e):
		//maint_acts.add(maint_acts_masters);
		return maint_acts_masters;
	}
}
