package com.mainmethod.springboot.gingafleets.common.tripsheet;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.mainmethod.springboot.gingafleets.dto.HireDetailsDto;
import com.mainmethod.springboot.gingafleets.dto.VendorSupplierDto;

public class PersonResultSetExtractorDropDownDriverNameFirst implements ResultSetExtractor {
	//List<HireDetailsDto> maint_acts=new ArrayList<>();
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		HireDetailsDto maint_acts_masters=new HireDetailsDto();
		
		maint_acts_masters.setEmployee_id(rs.getString(1));
		maint_acts_masters.setLicence_name(rs.getString(2));
		maint_acts_masters.setLicence_no(rs.getString(3));
		maint_acts_masters.setPhone_no(rs.getString(4));
		//maint_acts.add(maint_acts_masters);
		return maint_acts_masters;
	}
}
