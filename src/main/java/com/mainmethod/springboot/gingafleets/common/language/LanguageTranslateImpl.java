package com.mainmethod.springboot.gingafleets.common.language;

import java.util.ArrayList;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.mainmethod.springboot.gingafleets.common.frontdata.PersonRowMapper;

public class LanguageTranslateImpl implements LanguageTranslate{
	
private DataSource dataSource;
	
	
	public void setDataSource(DataSource ds) {	
        dataSource = ds;	
    }

	@Override
	public ArrayList<Object> selectLanguage(String languagecode) {
		 JdbcTemplate select = new JdbcTemplate(dataSource);
	       return (ArrayList<Object>) select.query("SELECT * FROM form_lang_label WHERE language_code='01'",
	        	//new Object[] {id},
	        		new LanguageRowMapper());
	}

}
