package com.mainmethod.springboot.gingafleets.common.frontdata;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;


import com.mainmethod.springboot.gingafleets.dto.Selection;

public class PersonResultSetExtractorDropDown implements ResultSetExtractor {
	
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		Selection selection= new Selection();
		
		selection.setSub_group(rs.getString(1));
		System.out.println(rs.getString(1));
		return selection;
	}

}
