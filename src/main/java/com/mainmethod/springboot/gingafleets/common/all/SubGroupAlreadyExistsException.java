package com.mainmethod.springboot.gingafleets.common.all;

public class SubGroupAlreadyExistsException extends Exception {

	public SubGroupAlreadyExistsException(String string) {
		super(string);
	}

}
