package com.mainmethod.springboot.gingafleets.common.tripsheet;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.mainmethod.springboot.gingafleets.dto.DriverChangeDto;
import com.mainmethod.springboot.gingafleets.dto.VendorSupplierDto;

public class PersonResultSetExtractorDropDownDriverNameSecond implements ResultSetExtractor {
	List<DriverChangeDto> maint_acts=new ArrayList<>();
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		DriverChangeDto maint_acts_masters=new DriverChangeDto();
		
		maint_acts_masters.setEmployee_sec_name(rs.getString(1));
		maint_acts_masters.setDriver_sec_code(rs.getString(2));
		
		maint_acts.add(maint_acts_masters);
		return maint_acts;
	}
}
