package com.mainmethod.springboot.gingafleets.dto;

public class DriverChangeDto {
	
	private String employee_sec_name;
	
	private String driver_sec_code;

	public String getEmployee_sec_name() {
		return employee_sec_name;
	}

	public void setEmployee_sec_name(String employee_sec_name) {
		this.employee_sec_name = employee_sec_name;
	}

	public String getDriver_sec_code() {
		return driver_sec_code;
	}

	public void setDriver_sec_code(String driver_sec_code) {
		this.driver_sec_code = driver_sec_code;
	}
	
	

}
