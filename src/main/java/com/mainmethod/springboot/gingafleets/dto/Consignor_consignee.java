package com.mainmethod.springboot.gingafleets.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "consignor_consignee")
public class Consignor_consignee {
	
	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="cnr_code")
	private String cnr_code;
	
	@Column(name="cnr_name")
	private String cnr_name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCnr_code() {
		return cnr_code;
	}

	public void setCnr_code(String cnr_code) {
		this.cnr_code = cnr_code;
	}

	public String getCnr_name() {
		return cnr_name;
	}

	public void setCnr_name(String cnr_name) {
		this.cnr_name = cnr_name;
	}

	
	
	

}
