package com.mainmethod.springboot.gingafleets.dto;

public class TripsheetReportDto {
	

	
	private String companycode;
	
	private String vehicle_type;
	
	private String vehicle_no;
	
	private String manifest_date1;
	
	private String manifest_date2;

	public String getCompanycode() {
		return companycode;
	}

	public void setCompanycode(String companycode) {
		this.companycode = companycode;
	}

	

	public String getVehicle_no() {
		return vehicle_no;
	}

	public void setVehicle_no(String vehicle_no) {
		this.vehicle_no = vehicle_no;
	}

	public String getManifest_date1() {
		return manifest_date1;
	}

	public void setManifest_date1(String manifest_date1) {
		this.manifest_date1 = manifest_date1;
	}

	public String getManifest_date2() {
		return manifest_date2;
	}

	public void setManifest_date2(String manifest_date2) {
		this.manifest_date2 = manifest_date2;
	}

	
	public String getVehicle_type() {
		return vehicle_type;
	}

	public void setVehicle_type(String vehicle_type) {
		this.vehicle_type = vehicle_type;
	}

}
