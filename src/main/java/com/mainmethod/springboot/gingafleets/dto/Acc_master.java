package com.mainmethod.springboot.gingafleets.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


public class Acc_master {
	
	private int id;
	
	
	private String ac_head_name;
	
	
	private String code_no;
	
	private String acc_no;

	public String getAc_head_name() {
		return ac_head_name;
	}

	public void setAc_head_name(String ac_head_name) {
		this.ac_head_name = ac_head_name;
	}

	public String getCode_no() {
		return code_no;
	}

	public void setCode_no(String code_no) {
		this.code_no = code_no;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAcc_no() {
		return acc_no;
	}

	public void setAcc_no(String acc_no) {
		this.acc_no = acc_no;
	}
	
	

}
