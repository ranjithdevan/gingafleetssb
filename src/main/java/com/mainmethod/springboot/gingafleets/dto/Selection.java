package com.mainmethod.springboot.gingafleets.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "selection")
public class Selection {
	
	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="sub_group")
	private String sub_group;
	
	@Column(name="group_name")
	private String group_name;
	
	@Column(name="svalue")
	private String svalue;
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSub_group() {
		return sub_group;
	}

	public void setSub_group(String sub_group) {
		this.sub_group = sub_group;
	}

	public String getGroup_name() {
		return group_name;
	}

	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}

	public String getSvalue() {
		return svalue;
	}

	public void setSvalue(String svalue) {
		this.svalue = svalue;
	}
	
	

}
