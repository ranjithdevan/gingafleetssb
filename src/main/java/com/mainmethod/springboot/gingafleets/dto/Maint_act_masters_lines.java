package com.mainmethod.springboot.gingafleets.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Maint_act_masters_lines")
public class Maint_act_masters_lines {
	
	@Id
	@Column(name="id")
	private int id; 
	
	@Column(name="period_kms")
	private String period_kms;
	
	@Column(name="period_days")
	private String period_days;
	
	@Column(name="remind_kms")
	private String remind_kms;
	
	@Column(name="remind_days")
	private String remind_days;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPeriod_kms() {
		return period_kms;
	}

	public void setPeriod_kms(String period_kms) {
		this.period_kms = period_kms;
	}

	public String getPeriod_days() {
		return period_days;
	}

	public void setPeriod_days(String period_days) {
		this.period_days = period_days;
	}

	public String getRemind_kms() {
		return remind_kms;
	}

	public void setRemind_kms(String remind_kms) {
		this.remind_kms = remind_kms;
	}

	public String getRemind_days() {
		return remind_days;
	}

	public void setRemind_days(String remind_days) {
		this.remind_days = remind_days;
	}
	
	

}
