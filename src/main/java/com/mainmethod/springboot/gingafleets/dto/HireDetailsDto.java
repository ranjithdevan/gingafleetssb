package com.mainmethod.springboot.gingafleets.dto;

public class HireDetailsDto {
	
	private String employee_id;
	
	private String licence_name;
	
	private String licence_no;
	
	private String phone_no;

	public String getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}

	public String getLicence_name() {
		return licence_name;
	}

	public void setLicence_name(String licence_name) {
		this.licence_name = licence_name;
	}

	public String getLicence_no() {
		return licence_no;
	}

	public void setLicence_no(String licence_no) {
		this.licence_no = licence_no;
	}

	public String getPhone_no() {
		return phone_no;
	}

	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}
	
	

}
