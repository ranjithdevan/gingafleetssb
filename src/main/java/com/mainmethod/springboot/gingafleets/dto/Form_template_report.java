package com.mainmethod.springboot.gingafleets.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "form_template_report")
public class Form_template_report {
	
	@Id
	@Column(name="menu_id")
	private int menu_id;
	
	@Column(name="field_name")
	private String field_name;
	
	@Column(name="label_name")
	private String label_name;
	
	@Column(name="field_type")
	private String field_type;
	
	@Column(name="mandatory_field")
	private String	mandatory_field;
	
	@Column(name="validation")
	private String validation;
	
	@Column(name="filter")
	private String	filter;
	
	@Column(name="pattern")
	private String pattern;
	
	@Column(name="get_from_table")
	private String get_from_table;
	
	@Column(name="validationName")
	private String validationName;
	
	@Column(name="validatorErrorPath")
	private String validatorErrorPath;
	
	@Column(name="eventType")
	private String eventType;
	
	
	@Column(name="eventValidation")
	private String eventValidation;
	
	@Column(name="autoType")
	private String autoType;
	
	@Column(name="patternErrorMessage")
	private String patternErrorMessage;
	
	public String getAutoType() {
		return autoType;
	}
	public void setAutoType(String autoType) {
		this.autoType = autoType;
	}
	public String getPatternErrorMessage() {
		return patternErrorMessage;
	}
	public void setPatternErrorMessage(String patternErrorMessage) {
		this.patternErrorMessage = patternErrorMessage;
	}
	public int getMenu_id() {
		return menu_id;
	}
	public void setMenu_id(int menu_id) {
		this.menu_id = menu_id;
	}
	public String getField_name() {
		return field_name;
	}
	public void setField_name(String field_name) {
		this.field_name = field_name;
	}
	public String getLabel_name() {
		return label_name;
	}
	public void setLabel_name(String label_name) {
		this.label_name = label_name;
	}
	public String getField_type() {
		return field_type;
	}
	public void setField_type(String field_type) {
		this.field_type = field_type;
	}
	public String getMandatory_field() {
		return mandatory_field;
	}
	public void setMandatory_field(String mandatory_field) {
		this.mandatory_field = mandatory_field;
	}
	public String getValidation() {
		return validation;
	}
	public void setValidation(String validation) {
		this.validation = validation;
	}
	public String getFilter() {
		return filter;
	}
	public void setFilter(String filter) {
		this.filter = filter;
	}
	public String getPattern() {
		return pattern;
	}
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}
	public String getGet_from_table() {
		return get_from_table;
	}
	public void setGet_from_table(String get_from_table) {
		this.get_from_table = get_from_table;
	}
	public String getValidationName() {
		return validationName;
	}
	public void setValidationName(String validationName) {
		this.validationName = validationName;
	}
	
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getEventValidation() {
		return eventValidation;
	}
	public void setEventValidation(String eventValidation) {
		this.eventValidation = eventValidation;
	}
	public String getValidatorErrorPath() {
		return validatorErrorPath;
	}
	public void setValidatorErrorPath(String validatorErrorPath) {
		this.validatorErrorPath = validatorErrorPath;
	}
	
	

}
