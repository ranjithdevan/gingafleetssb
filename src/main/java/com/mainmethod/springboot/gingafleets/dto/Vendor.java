package com.mainmethod.springboot.gingafleets.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mainmethod.springboot.gingafleets.model.BaseEntity;

@Entity
@Table(name = "vendor")
public class Vendor extends BaseEntity {
	
	@Id
	@Column(name="id")
	private int id;
	
	
@Column(name="supplier_code")	
private String supplier_code;

@Column(name="supplier_name")
private String	supplier_name;

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getSupplier_code() {
	return supplier_code;
}

public void setSupplier_code(String supplier_code) {
	this.supplier_code = supplier_code;
}

public String getSupplier_name() {
	return supplier_name;
}

public void setSupplier_name(String supplier_name) {
	this.supplier_name = supplier_name;
}



}
