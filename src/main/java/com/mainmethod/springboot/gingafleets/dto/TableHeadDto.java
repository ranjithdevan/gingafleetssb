package com.mainmethod.springboot.gingafleets.dto;

public class TableHeadDto {
	
	private String SelectionCode;
	private String GroupName;
	private String SubGroupName;
	private String Value;
	private String View;
	private String Update;
	private String Delete;
	
	public String getSelectionCode() {
		return SelectionCode;
	}
	public void setSelectionCode(String selectionCode) {
		SelectionCode = selectionCode;
	}
	public String getGroupName() {
		return GroupName;
	}
	public void setGroupName(String groupName) {
		GroupName = groupName;
	}
	public String getSubGroupName() {
		return SubGroupName;
	}
	public void setSubGroupName(String subGroupName) {
		SubGroupName = subGroupName;
	}
	public String getValue() {
		return Value;
	}
	public void setValue(String value) {
		Value = value;
	}
	public String getView() {
		return View;
	}
	public void setView(String view) {
		View = view;
	}
	public String getUpdate() {
		return Update;
	}
	public void setUpdate(String update) {
		Update = update;
	}
	public String getDelete() {
		return Delete;
	}
	public void setDelete(String delete) {
		Delete = delete;
	}
	
	
	

}
