package com.mainmethod.springboot.gingafleets.dto;

public class Maintenance {
	
	
	private String maint_group;
	
	private String activity_name;
	
	private String maint_code;
	
	private String period_kms;
	
	private String period_days;
	
	private String remind_kms;
	
	private String remind_days;


	public String getMaint_group() {
		return maint_group;
	}

	public void setMaint_group(String maint_group) {
		this.maint_group = maint_group;
	}

	public String getActivity_name() {
		return activity_name;
	}

	public void setActivity_name(String activity_name) {
		this.activity_name = activity_name;
	}

	public String getMaint_code() {
		return maint_code;
	}

	public void setMaint_code(String maint_code) {
		this.maint_code = maint_code;
	}

	public String getPeriod_kms() {
		return period_kms;
	}

	public void setPeriod_kms(String period_kms) {
		this.period_kms = period_kms;
	}

	public String getPeriod_days() {
		return period_days;
	}

	public void setPeriod_days(String period_days) {
		this.period_days = period_days;
	}

	public String getRemind_kms() {
		return remind_kms;
	}

	public void setRemind_kms(String remind_kms) {
		this.remind_kms = remind_kms;
	}

	public String getRemind_days() {
		return remind_days;
	}

	public void setRemind_days(String remind_days) {
		this.remind_days = remind_days;
	}
	
	

}
