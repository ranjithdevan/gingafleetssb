package com.mainmethod.springboot.gingafleets.dto;

public class Selectiondto {
	
	private String svalue;
	
	private String sub_group;

	public String getSvalue() {
		return svalue;
	}

	public void setSvalue(String svalue) {
		this.svalue = svalue;
	}

	public String getSub_group() {
		return sub_group;
	}

	public void setSub_group(String sub_group) {
		this.sub_group = sub_group;
	}
	

}
