package com.mainmethod.springboot.gingafleets.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "maint_act_masters")
public class Maint_act_masters {
	
	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="maint_group")
	private String maint_group;
	
	@Column(name="activity_name")
	private String activity_name;
	
	@Column(name="maint_code")
	private String maint_code;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMaint_group() {
		return maint_group;
	}

	public void setMaint_group(String maint_group) {
		this.maint_group = maint_group;
	}

	public String getActivity_name() {
		return activity_name;
	}

	public void setActivity_name(String activity_name) {
		this.activity_name = activity_name;
	}

	public String getMaint_code() {
		return maint_code;
	}

	public void setMaint_code(String maint_code) {
		this.maint_code = maint_code;
	}
	


}
