package com.mainmethod.springboot.gingafleets.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.mainmethod.springboot.gingafleets.model.BaseEntity;

@Entity
@Table(name = "branchdetails")
public class Branchdetails extends BaseEntity{
	
	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="center_id")
	private String center_id;
	
	@Column(name="center_name")
	private String center_name;
	
private String address1;
	
	private String address2;
	
	private String area;
	
	
	private String city;
	
	
	private String state;
	
	
	private String country;
	
	
	private String pincode;
	
	
	private String branch_mngr;
	
	
	private String phone_no;
	
	
	private String email_id;
	
	
	private String website;
	
	
	private String cp_name;
	
	
	private String cp_phoneno;
	
	
	private String cp_emailid;
	
	
	private String remarks;
	
	
	private String activate;
	
	private String cp_personphoneno;
	
	
	
	
	private String self_ref_code;
	
	
	private String licen_product_code;
	
	
	private String licen_exp_date;
	
	
	private String company_id;
	
	private String bst_code;
	
	private String bst_gst_no;
	
	private String bst_pan_no;
	
	
	private String alias_no;
	
	
	private String active;
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCenter_id() {
		return center_id;
	}

	public void setCenter_id(String center_id) {
		this.center_id = center_id;
	}

	

	public String getCenter_name() {
		return center_name;
	}

	public void setCenter_name(String center_name) {
		this.center_name = center_name;
	}
	

}
