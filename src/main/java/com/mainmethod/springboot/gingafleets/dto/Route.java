package com.mainmethod.springboot.gingafleets.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "route")
public class Route {
	
	@Id
	@Column(name="id")
	private int id;
	
	@Column(name="route_sysno")
	private String route_sysno;
	
	@Column(name="route_no")
	private String route_no;
	
	@Column(name="location_from")
	private String location_from;
	
	@Column(name="location_to")
	private String location_to;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRoute_sysno() {
		return route_sysno;
	}

	public void setRoute_sysno(String route_sysno) {
		this.route_sysno = route_sysno;
	}

	public String getRoute_no() {
		return route_no;
	}

	public void setRoute_no(String route_no) {
		this.route_no = route_no;
	}

	public String getLocation_from() {
		return location_from;
	}

	public void setLocation_from(String location_from) {
		this.location_from = location_from;
	}

	public String getLocation_to() {
		return location_to;
	}

	public void setLocation_to(String location_to) {
		this.location_to = location_to;
	}
	
	

}
