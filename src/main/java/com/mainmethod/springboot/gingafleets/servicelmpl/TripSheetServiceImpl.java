package com.mainmethod.springboot.gingafleets.servicelmpl;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mainmethod.springboot.gingafleets.common.all.Updateautonumber;
import com.mainmethod.springboot.gingafleets.model.Initial;
import com.mainmethod.springboot.gingafleets.model.Load_manifest;

import com.mainmethod.springboot.gingafleets.repository.InitialRepository;
import com.mainmethod.springboot.gingafleets.repository.TripSheetRepository;

import com.mainmethod.springboot.gingafleets.service.TripSheetService;

@Service
public class TripSheetServiceImpl implements TripSheetService{
	
	@Autowired
	TripSheetRepository tripSheetRepository;
	
	@Autowired 
	InitialRepository initialRepository;  
	
	@Autowired 
	Updateautonumber updateautonumber;
	
	String initialUpdate="";
	String initialSaveSelectionMaster="";
	String currentvalues="";
	String type="LA";
	
	@Override
	public List<Load_manifest> findByCompany() {
		return tripSheetRepository.findByCompany();
	}
	
	@Override
	public Load_manifest saveTripSheetMaster(Load_manifest load_manifest) {
		Load_manifest saveTripSheetMaster=null;
		Initial finding = initialRepository.findtypeAndcompanycode(type,load_manifest.getCompany_code());
		//if(!finding.getCompany_code().equals(cust_selection.getCompany_code()) && finding) {
		if(finding.getCurrent_value()!=null) {
		initialUpdate=updateautonumber.getStringInitalvalueUpdate(finding.getCurrent_value());
		}
		Initial  checkId= initialRepository.findById(finding.getId());
		checkId.setCurrent_value(initialUpdate);
		if (checkId != null) {
		 initialRepository.save(checkId);	
		 currentvalues=checkId.getCurrent_value();
		initialSaveSelectionMaster=updateautonumber.getStringInitalvalueSave(currentvalues,type);
		load_manifest.setManifest_code(initialSaveSelectionMaster);
		saveTripSheetMaster = tripSheetRepository.save(load_manifest);	
		}
		return saveTripSheetMaster;
	}
	@Override
	public Optional<Load_manifest> getSelectionMasterById(int id) {
		Optional<Load_manifest> tripshet = tripSheetRepository.findById(id);
		return tripshet;
	}
	@Override
	public Load_manifest updateSelectionMaster(Load_manifest load_manifest) {
		Optional<Load_manifest>  veh= tripSheetRepository.findById(load_manifest.getId());
		if (veh.isPresent()) {
			Load_manifest updatesave = tripSheetRepository.save(load_manifest);
			return updatesave;
		}
		else {
		return null;
		}
	}
	@Override
	public void delete(int id) {
		tripSheetRepository.deleteById(id);
		
	}

	

	

	
	

}
