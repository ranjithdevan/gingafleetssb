package com.mainmethod.springboot.gingafleets.servicelmpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.sql.DataSource;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mainmethod.springboot.gingafleets.common.frontdata.CustFieldsDao;
import com.mainmethod.springboot.gingafleets.common.tripsheet.DropDownTripsheetServiceImpl;
import com.mainmethod.springboot.gingafleets.common.vehicleinfo.DropDownVehicleInfoServiceImpl;
import com.mainmethod.springboot.gingafleets.dto.Selection;
import com.mainmethod.springboot.gingafleets.jdbc.DataSourceConfig;
import com.mainmethod.springboot.gingafleets.model.Cust_selection;
import com.mainmethod.springboot.gingafleets.service.angularFormlyService;

@Service
public class angularFormlyServiceImpl implements angularFormlyService {

	public static DropDownVehicleInfoServiceImpl dropDownVehicleInfoServiceImpl;

	CustFieldsDao dao = new CustFieldsDao();

	DropDownVehicleInfoServiceImpl combodao = new DropDownVehicleInfoServiceImpl();
	public static LinkedHashMap<String, String> heading;
	@SuppressWarnings("rawtypes")
	public LinkedHashMap rowObj;
	public  ArrayList<Object> fieldGroup;
	public  int index =0;
	@SuppressWarnings("rawtypes")
	public  LinkedHashMap validationObject;
	@SuppressWarnings("rawtypes")
	public  LinkedHashMap validateOptions;
	private  boolean firstHeading=false;
	public  LinkedHashMap validators;
	public ArrayList<Object> validationTop;
	@SuppressWarnings({ "null", "unchecked", "rawtypes" })
	

	@Override
	public List<Object> buildFormlyStructure(List<?> ddd, ArrayList<String> combobox, DataSource dataSource2) {
		ArrayList<Object> fields = new ArrayList<Object>();
		ObjectMapper i1 = new ObjectMapper();
		LinkedHashMap validatorsOuter = new LinkedHashMap();
	
		JsonNode checkPanelReport = i1.valueToTree(ddd.get(0));
		if (!checkPanelReport.get("field_type").toString().contains("panel")) {
			rowObj = new LinkedHashMap();
			fieldGroup = new ArrayList<Object>();
			rowObj.put("fieldGroupClassName", "row");
		}
		ddd.stream().forEach(p -> {
			
			JsonNode J23 = i1.valueToTree(p);
			if (J23.get("field_type").toString().contains("panel")) {
				
				System.out.println("reoee   "+rowObj);
				
				if(index>0) {
					
					validators = new LinkedHashMap();
					validationTop = new ArrayList<Object>();

					fields.add(rowObj);
					
					angularFormlyServiceImpl.heading = new LinkedHashMap<String, String>();
					angularFormlyServiceImpl.heading.put("className", "section-label");
					angularFormlyServiceImpl.heading.put("template",
							"<hr /><h5>" + J23.get("label_name").toString().replaceAll("\"", "") + "</h5>");

					rowObj = new LinkedHashMap();
					rowObj.put("fieldGroupClassName", "row");
					fieldGroup =new ArrayList<Object>();
					fields.add(heading);
				}
				else {
					
					angularFormlyServiceImpl.heading = new LinkedHashMap<String, String>();
					angularFormlyServiceImpl.heading.put("className", "section-label");
					angularFormlyServiceImpl.heading.put("template",
							"<h5>" + J23.get("label_name").toString().replaceAll("\"", "") + "</h5>");

					rowObj = new LinkedHashMap();
					rowObj.put("fieldGroupClassName", "row");
					fieldGroup = new ArrayList<Object>();
					fields.add(heading);
					firstHeading=true;

				}

			} else {
				@SuppressWarnings("rawtypes")
				LinkedHashMap field = new LinkedHashMap();
				@SuppressWarnings("rawtypes")
				LinkedHashMap template = new LinkedHashMap();
				LinkedHashMap validation = new LinkedHashMap();
				LinkedHashMap messages = new LinkedHashMap();
				if (J23.get("validation").toString().contains("autoTab")) {
					
					template.put("required", J23.get("mandatory_field").toString().contains("*") ? true : false);
					template.put("label", J23.get("label_name"));
					field.put("className", "col-3");
					field.put("key", J23.get("field_name"));
					field.put("type", "autocomplete");
					field.put("autoType", J23.get("autoType"));
					System.out.println(J23.get("filter").toString());
					template.put("filter", J23.get("filter").toString());
					field.put("templateOptions", template);
					System.out.println("checking in autotab");
					String validatename=J23.get("validationName").toString().replaceAll("\"", "");
					String errorPath=J23.get("validatorErrorPath").toString().replaceAll("\"", "");
					
					if(!validatename.equals("null")  && !validatename.isEmpty() && !errorPath.equals("null")  && !errorPath.isEmpty() ) {
						
						validationObject = new LinkedHashMap<>();
						validateOptions = new LinkedHashMap<>();
						validationObject.put("name",validatename );
						validateOptions.put("errorPath", errorPath);
						validationObject.put("options", validateOptions);
						validationTop.add(validationObject);
						validators.put("validation", validationTop);
						

					}
					fieldGroup.add(field);
				}
				else if(J23.get("field_type").toString().contains("date")) {
					
					template.put("required", !J23.get("mandatory_field").toString().contains("*") ? true : false);
					template.put("label", J23.get("label_name"));
					field.put("className", "col-3");
					field.put("key", J23.get("field_name"));
					field.put("type", J23.get("field_type").toString().contains("date") ? "datepicker" : "datepicker");
					field.put("templateOptions", template);
					String validatename=J23.get("validationName").toString().replaceAll("\"", "");
					String errorPath=J23.get("validatorErrorPath").toString().replaceAll("\"", "");
					//System.out.println("errot path checking coming or not------>"+errorPath);
					if(!validatename.equals("null")  && !validatename.isEmpty() && !errorPath.equals("null")  && !errorPath.isEmpty() ) {
						
						validationObject = new LinkedHashMap<>();
						validateOptions = new LinkedHashMap<>();
						validationObject.put("name",validatename.toString() );
						validateOptions.put("errorPath",errorPath );
						validationObject.put("options", validateOptions);
						validationTop.add(validationObject);
						validators.put("validation", validationTop);
					

					}
					fieldGroup.add(field);
				}
				else if(J23.get("field_type").toString().contains("time")) {
					template.put("required", !J23.get("mandatory_field").toString().contains("*") ? true : false);
					template.put("label", J23.get("label_name"));
					field.put("className", "col-3");
					field.put("key", J23.get("field_name"));
					field.put("type", J23.get("field_type").toString().contains("time") ? "datepicker" : "datepicker");
					field.put("templateOptions", template);
					fieldGroup.add(field);
				}

				else if (J23.get("field_type").toString().contains("combo")) {
					if(combobox !=null) {
					combodao.setDataSource(dataSource2);
					String comboreferencevalue = J23.get("get_from_table").toString();
					comboreferencevalue = comboreferencevalue.replace("\"", "");
					
					combobox.add(0, comboreferencevalue);
					combobox.add(1, "A000198");
					
					ArrayList<String> combobo = combodao.make(combobox);

					LinkedHashMap optionObjects = null;
					template.put("required", J23.get("mandatory_field").toString().contains("*") ? true : false);
					template.put("label", J23.get("label_name"));
					if(!J23.get("eventType").equals("null")) {
						template.put("eventType", J23.get("eventValidation"));
						template.put("eventLogic", J23.get("eventType"));
					}
					field.put("className", "col-3");
					field.put("key", J23.get("field_name"));
					field.put("type", "select");
					ArrayList<Object> options = new ArrayList<Object>();
					optionObjects = new LinkedHashMap();
					optionObjects.put("value", "select");
					optionObjects.put("label", "Select");
					options.add(optionObjects);
					for (int i = 0; i < combobo.size(); i++) {
						optionObjects = new LinkedHashMap();
						optionObjects.put("value", combobo.get(i));
						optionObjects.put("label", combobo.get(i));
						options.add(optionObjects);
					}
					template.put("options", options);
					field.put("templateOptions", template);
					
					String validatename=J23.get("validationName").toString().replaceAll("\"", "");
					
					String errorPath=J23.get("validatorErrorPath").toString().replaceAll("\"", "");
					
					if(!validatename.equals("null")  && !validatename.isEmpty() && !errorPath.equals("null")  && !errorPath.isEmpty() ) {
						validationObject = new LinkedHashMap<>();
						validateOptions = new LinkedHashMap<>();
						 validationObject.put("name",validatename );
						 validateOptions.put("errorPath", errorPath);
						 validationObject.put("options", validateOptions);
						 validationTop.add(validationObject);
						 validators.put("validation", validationTop);	
					}

					}
					fieldGroup.add(field);
				}



				else {
					template.put("required", !J23.get("mandatory_field").toString().contains("*") ? true : false);
					template.put("label", J23.get("label_name"));
					if(J23.get("pattern")!=null) {
						//System.out.println("pattern for valid"+J23.get("pattern"));
						template.put("pattern", J23.get("pattern").toString().contains("null")?"":J23.get("pattern"));
						template.put("validation", validation);
						template.put("patternMessage", J23.get("patternErrorMessage").toString().contains("null")?"":J23.get("patternErrorMessage"));
					}
					if(!J23.get("eventType").equals("null")) {
						template.put("eventType", J23.get("eventValidation"));
						template.put("eventLogic", J23.get("eventType"));
					}
					field.put("className", "col-3");
					field.put("key", J23.get("field_name"));
					field.put("type", J23.get("field_type").toString().contains("text") ? "input" : "input");
					field.put("templateOptions", template);
					String validatename=J23.get("validationName").toString().replaceAll("\"", "");
					String errorPath=J23.get("validatorErrorPath").toString().replaceAll("\"", "");
					//System.out.println("validatename------checking output ####"+validatename);
					if(!validatename.equalsIgnoreCase("Null")  && !validatename.isEmpty() && !errorPath.equals("null")  && !errorPath.isEmpty()) {
						
						validationObject = new LinkedHashMap<>();
						validateOptions = new LinkedHashMap<>();
						validationObject.put("name",validatename );
						validateOptions.put("errorPath", errorPath);
						validationObject.put("options", validateOptions);
						validationTop.add(validationObject);
						validators.put("validation", validationTop);
						

					}
					fieldGroup.add(field);

				}


				rowObj.put("fieldGroup", fieldGroup);
				rowObj.put("validators", validators);


				//	System.out.println("before return");
			}
			index++;
			if(index == ddd.size()) {
				fields.add(rowObj);
			}	
			/*else {
				fields.add(rowObj);
			}*/
		});

		index=0;
		return fields;
	}

}