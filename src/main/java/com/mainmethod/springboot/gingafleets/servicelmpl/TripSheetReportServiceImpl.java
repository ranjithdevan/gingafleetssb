package com.mainmethod.springboot.gingafleets.servicelmpl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.mainmethod.springboot.gingafleets.model.Load_manifest;
import com.mainmethod.springboot.gingafleets.repository.TripSheetRepository;
import com.mainmethod.springboot.gingafleets.service.TripSheetReportService;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class TripSheetReportServiceImpl implements TripSheetReportService {

	@Autowired
	TripSheetRepository tripSheetRepository;
	@Override
	public String exportReport(HttpServletResponse response,String format) throws FileNotFoundException,JRException {
		  String path = "E:\\Jasper Report Download";
		List<Load_manifest> tripsheetReport=tripSheetRepository.findAll();
		File file = ResourceUtils.getFile("classpath:TripReportsSheetRegister.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(tripsheetReport);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("Testing", "Report");
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
        JasperExportManager.exportReportToPdfFile(jasperPrint, path + "\\tripsheetReport.pdf");
        try {
			JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());
			response.setContentType("application/pdf");
	        response.addHeader("Content-Disposition", "inline; filename=jasper.pdf;");
		}
        catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return path;   
    }

}
