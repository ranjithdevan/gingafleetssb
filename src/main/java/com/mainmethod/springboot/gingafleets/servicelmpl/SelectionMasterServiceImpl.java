package com.mainmethod.springboot.gingafleets.servicelmpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mainmethod.springboot.gingafleets.common.all.SubGroupAlreadyExistsException;
import com.mainmethod.springboot.gingafleets.common.all.Updateautonumber;
import com.mainmethod.springboot.gingafleets.model.Cust_selection;
import com.mainmethod.springboot.gingafleets.model.Initial;
import com.mainmethod.springboot.gingafleets.repository.InitialRepository;
import com.mainmethod.springboot.gingafleets.repository.SelectionRetireveRepository;
import com.mainmethod.springboot.gingafleets.service.SelectionMasterService;

@Service
public class SelectionMasterServiceImpl implements SelectionMasterService {

	@Autowired
	SelectionRetireveRepository selectionRetireveRepository;

	@Autowired 
	InitialRepository initialRepository;  

	@Autowired 
	Updateautonumber updateautonumber;



	@Override
	public List<Cust_selection> getall() {
		return selectionRetireveRepository.findBycompany();
	}

	@Override
	public Cust_selection saveSelectionMaster(Cust_selection cust_selection) throws SubGroupAlreadyExistsException {	
		String initialUpdate="";
		String initialSaveSelectionMaster="";
		String currentvalues="";
		String type="SVAL";
		Cust_selection savecus_selection= new Cust_selection();
		Cust_selection fidingSelectionMaster=selectionRetireveRepository.findingByGroupNameCompanyCodeAndSubgropAndSvalue(cust_selection.getGroup_name(),cust_selection.getCompany_code(),cust_selection.getSub_group(),cust_selection.getSvalue());
		if(fidingSelectionMaster!=null) {
			throw new SubGroupAlreadyExistsException("Already Exists"+"groupname:"+cust_selection.getGroup_name()+"companycode:"+cust_selection.getCompany_code()+"value:"+cust_selection.getSvalue());
		}
		else {
			Initial finding = initialRepository.findtypeAndcompanycode(type,cust_selection.getCompany_code());
			if(finding.getCurrent_value()!=null) {
				initialUpdate=updateautonumber.getStringInitalvalueUpdate(finding.getCurrent_value());
			}
			Initial  checkId= initialRepository.findById(finding.getId());
			checkId.setCurrent_value(initialUpdate);
			if (checkId != null) {
				initialRepository.save(checkId);	
				currentvalues=checkId.getCurrent_value();
				initialSaveSelectionMaster=updateautonumber.getStringInitalvalueSave(currentvalues,type);
				cust_selection.setSelection_code(initialSaveSelectionMaster);
				savecus_selection = selectionRetireveRepository.save(cust_selection);	
			}
		}
		return savecus_selection;
	}

	@Override
	public Optional<Cust_selection> getSelectionMasterById(int id) {
		Optional<Cust_selection> Cust_selections = selectionRetireveRepository.findById(id);
		return Cust_selections;
	}

	@Override
	public Cust_selection updateSelectionMaster(Cust_selection cust_selection) {
		Optional<Cust_selection>  cust_select= selectionRetireveRepository.findById(cust_selection.getId());
		if (cust_select.isPresent()) {
			Cust_selection updatesave = selectionRetireveRepository.save(cust_selection);
			return updatesave;
		}
		else {
			return null;
		}
	}

	@Override
	public void delete(int id) {
		selectionRetireveRepository.deleteById(id);
	}





}
