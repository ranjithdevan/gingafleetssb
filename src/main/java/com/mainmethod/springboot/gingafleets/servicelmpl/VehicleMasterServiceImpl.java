package com.mainmethod.springboot.gingafleets.servicelmpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mainmethod.springboot.gingafleets.common.all.SubGroupAlreadyExistsException;
import com.mainmethod.springboot.gingafleets.common.all.Updateautonumber;
import com.mainmethod.springboot.gingafleets.model.Initial;
import com.mainmethod.springboot.gingafleets.model.Vehicle;
import com.mainmethod.springboot.gingafleets.model.Vehicle_maintain_lines;
import com.mainmethod.springboot.gingafleets.repository.InitialRepository;
import com.mainmethod.springboot.gingafleets.repository.VehicleRepository;
import com.mainmethod.springboot.gingafleets.repository.Vehicle_maintian_linesRepository;
import com.mainmethod.springboot.gingafleets.service.VehicleMasterService;


@Service
public class VehicleMasterServiceImpl implements VehicleMasterService {

	@Autowired
	VehicleRepository vehicleRepository;
	
	@Autowired
	Vehicle_maintian_linesRepository vehicle_maintian_linesRepository;
	
	@Autowired 
	InitialRepository initialRepository;  
	
	@Autowired 
	Updateautonumber updateautonumber;
	
	String initialUpdate="";
	String initialSaveSelectionMaster="";
	String currentvalues="";
	String type="VEH";
	@Override
	public List<Vehicle> getall() {
		return vehicleRepository.findByCompany();
	}

	
	@Override
	public Vehicle saveVehicleMaster(Vehicle vehicle) throws SubGroupAlreadyExistsException {	
		Vehicle saveVehcileMaster=new Vehicle();
	//	Vehicle_maintain_lines vmls = new Vehicle_maintain_lines();
		List<Vehicle_maintain_lines> vehicleMaintainLines=new ArrayList<>();
		
			Vehicle findCompanyVehicleNo=vehicleRepository.findingbycompanyCodeandvehicle_NO(vehicle.getCompany_code(), vehicle.getVehicle_no());
		if(findCompanyVehicleNo !=null) {
			throw new SubGroupAlreadyExistsException("Already Exists->"+"vehicle NO:"+vehicle.getVehicle_no());
		}
		else {
	Initial finding = initialRepository.findtypeAndcompanycode(type,vehicle.getCompany_code());
	
	if(finding.getCurrent_value()!=null) {
	initialUpdate=updateautonumber.getStringInitalvalueUpdate(finding.getCurrent_value());
	}
	Initial  checkId= initialRepository.findById(finding.getId());
	checkId.setCurrent_value(initialUpdate);
	if (checkId != null) {
	 initialRepository.save(checkId);	
	 currentvalues=checkId.getCurrent_value();
	 System.out.println("currentvalues----------------->@@@@ check for output"+currentvalues);
	initialSaveSelectionMaster=updateautonumber.getStringInitalvalueSave(currentvalues,type);
	vehicle.setVehicle_id(initialSaveSelectionMaster);
	String vehicleidformaintin=vehicle.getVehicle_id();
	String vehiclecompanycode=vehicle.getCompany_code();
	 System.out.println("vehicle id------------->@@@@ checking  for 2nd table"+vehicleidformaintin);
		//if(vehicle.getVehicleMaintainLines().size()>0) {
	 for(Vehicle_maintain_lines vmls: vehicle.getVehicleMaintainLines())
	 {
	System.out.println("count size of the vehicle company-->"+vehicle.getVehicleMaintainLines().size());
			 vmls.setVehicle_id(vehicleidformaintin);
			 System.out.println("inside the vmllist vehicle id------>"+vmls.getVehicle_id());
		 vmls.setCompany_code(vehiclecompanycode);
		 System.out.println("inside the vmllist compnay code------>"+vmls.getCompany_code());
		 vmls.getPeriod_kms();
		 System.out.println("inside the vmllist periodkms------>"+vmls.getPeriod_kms());
		 vehicle.setVehicleMaintainLines(vehicleMaintainLines);
		 vehicleMaintainLines.add(vmls);
		 saveVehcileMaster = vehicleRepository.save(vehicle); 
	 }
		System.out.println("checking the vehicle  saved");
	
	}
		}
	return saveVehcileMaster;
	}
	
	@Override
	public Optional<Vehicle> getSelectionMasterById(int id) {
		Optional<Vehicle> vehicl = vehicleRepository.findById(id);
		return vehicl;
	}

	@Override
	public Vehicle updateSelectionMaster(Vehicle vehicle) {
		Optional<Vehicle>  veh= vehicleRepository.findById(vehicle.getId());
		if (veh.isPresent()) {
			Vehicle updatesave = vehicleRepository.save(vehicle);
			return updatesave;
		}
		else {
		return null;
		}
	}
	
	@Override
	public void delete(int id) {
		vehicleRepository.deleteById(id);
	}

}
