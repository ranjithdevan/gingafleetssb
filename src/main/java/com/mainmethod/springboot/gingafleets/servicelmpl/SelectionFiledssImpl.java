package com.mainmethod.springboot.gingafleets.servicelmpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mainmethod.springboot.gingafleets.dto.Cust_selectionField;
import com.mainmethod.springboot.gingafleets.repository.SelectionFieldRepository;
import com.mainmethod.springboot.gingafleets.service.SelectionFiledss;


@Service
public class SelectionFiledssImpl implements SelectionFiledss {

@Autowired
	SelectionFieldRepository selectionFieldRepository;
	
	@Override
	//public List<Cust_selectionField> findingbyMenuId(int id)
     public List<Cust_selectionField> findingbyMenuId() {
	
	//	return selectionFieldRepository.findAllByMenuId(id);
		return selectionFieldRepository.findAllByMenuId();
	}
	

}
