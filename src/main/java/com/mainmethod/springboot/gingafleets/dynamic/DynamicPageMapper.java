package com.mainmethod.springboot.gingafleets.dynamic;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.mainmethod.springboot.gingafleets.common.frontdata.PersonResultSetExtractor;

public class DynamicPageMapper implements RowMapper {
	@Override
    public Object mapRow(ResultSet rs, int line) throws SQLException {
        PersonResultSetExtractorDynamicRowMapper extractor = new PersonResultSetExtractorDynamicRowMapper();
        return extractor.extractData(rs);
    }

}
