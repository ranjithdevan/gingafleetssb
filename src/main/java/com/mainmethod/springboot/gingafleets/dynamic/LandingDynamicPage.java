package com.mainmethod.springboot.gingafleets.dynamic;

import java.util.ArrayList;
import java.util.List;

import com.mainmethod.springboot.gingafleets.dto.Cust_selectionField;
import com.mainmethod.springboot.gingafleets.model.ItemDescription;


public interface LandingDynamicPage {
	
	ArrayList<Object> selectDynamic(String level,String userName,String branchName,String user_id,String activate);
	
	ArrayList<Object> selectDynamicONeAndTwo(String level,String userName,String branchName,String rootMenu,String user_id,String activate);
	
	ArrayList<Object> selectDynamicTwo(String level,String userName,String branchName,String rootMenu,String user_id,String activate);

}
