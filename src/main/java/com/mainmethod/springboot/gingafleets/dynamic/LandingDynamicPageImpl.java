package com.mainmethod.springboot.gingafleets.dynamic;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.mainmethod.springboot.gingafleets.common.frontdata.PersonRowMapper;
import com.mainmethod.springboot.gingafleets.model.ItemDescription;

public class LandingDynamicPageImpl implements LandingDynamicPage {
private DataSource dataSource;
	
	
	public void setDataSource(DataSource ds) {	
        dataSource = ds;	
    }


	@Override
	public ArrayList<Object> selectDynamic(String level,String userName, String branchName,String user_id,String activate) {
		 JdbcTemplate select = new JdbcTemplate(dataSource);
		 String levels=level;
		// System.out.println("levels--------->"+levels);
		 String userNames=userName;
		// System.out.println("usernames--------->"+userNames);
		 String branchNames=branchName;
		// System.out.println("branchNames--------->"+branchNames);
		 //LandingDynamicPageImpl.String rootMenus=rootMenu;
		// System.out.println("rootMenus--------->"+rootMenus);
		 String user_ids=user_id;
		// System.out.println("user_ids--------->"+user_ids);
		 String activates=activate;
		// System.out.println("activates--------->"+activates);
		 
		 return (ArrayList<Object>) select.query("SELECT itemdescription.menucode, itemdescription.help_id,IFNULL(e.rel_lang_label_name, itemdescription.itemdescription) as itemdescription,IFNULL(f.rel_lang_label_name,itemdescription.condition_check) as condition_check,itemdescription.check_cond,itemdescription.action_name,itemdescription.root_menu FROM live_saas_design.itemdescription inner join live_saas_trans.userdetails b inner join live_saas_trans.usertab c on b.user_id = c.userid and itemdescription.menucode=c.itemcode inner join live_saas_trans.user_access_rights d left join live_saas_design.form_lang_label e on b.language_code = e.language_code AND upper(live_saas_design.itemdescription.itemdescription) =  upper(e.label_name) left join saas_design.form_lang_label f on b.language_code = f.language_code AND  upper(itemdescription.condition_check) =  upper(f.label_name) WHERE live_saas_design.itemdescription.itemdescription NOT IN  ('Report', 'New', 'Search', 'Edit', 'ACE', 'ACATNOT', 'ACPNOT', 'ACAFDNOT', 'ACABDNOT', 'AAPPAPNOT', 'ACATDNOT', 'AUTOINCNOT', 'AMLIMITNOT', 'DashBoard', 'ALL') and live_saas_design.itemdescription.level =? and b.login_id =? and d.branch_name=? and d.user_id=? and d.activate=? ORDER BY live_saas_design.itemdescription.`position` ASC, live_saas_design.itemdescription.menucode ASC",
	      // return (List<ItemDescription>) select.queryForObject("SELECT itemdescription.menucode, itemdescription.help_id,IFNULL(e.rel_lang_label_name, itemdescription.itemdescription) as itemdescription,IFNULL(f.rel_lang_label_name,itemdescription.condition_check) as condition_check,itemdescription.check_cond,itemdescription.action_name,itemdescription.root_menu FROM saas_design.itemdescription inner join saas_trans.userdetails b inner join saas_trans.usertab c on b.user_id = c.userid and itemdescription.menucode=c.itemcode inner join saas_trans.user_access_rights d left join saas_design.form_lang_label e on b.language_code = e.language_code AND upper(saas_design.itemdescription.itemdescription) =  upper(e.label_name) left join saas_design.form_lang_label f on b.language_code = f.language_code AND  upper(itemdescription.condition_check) =  upper(f.label_name) WHERE saas_design.itemdescription.itemdescription NOT IN  ('Report', 'New', 'Search', 'Edit', 'ACE', 'ACATNOT', 'ACPNOT', 'ACAFDNOT', 'ACABDNOT', 'AAPPAPNOT', 'ACATDNOT', 'AUTOINCNOT', 'AMLIMITNOT', 'DashBoard', 'ALL') and saas_design.itemdescription.level =? and b.login_id =? and d.branch_name=?' and saas_design.itemdescription.root_menu =? and d.user_id=? and d.activate=? ORDER BY saas_design.itemdescription.`position` ASC, saas_design.itemdescription.menucode ASC",
	        //	new Object[] {levels,userNames,branchNames,rootMenus,user_ids,activates},
				 new Object[] {levels,userNames,branchNames,user_ids,activates},
	        		new DynamicPageMapper());
	}


	@Override
	public ArrayList<Object> selectDynamicONeAndTwo(String level, String userName, String branchName, String rootMenu,
			String user_id, String activate) {
		JdbcTemplate select = new JdbcTemplate(dataSource);
		 String levels=level;
		// System.out.println("levels--------->"+levels);
		// System.out.println("check");
		 String userNames=userName;
		// System.out.println("usernames--------->"+userNames);
		 String branchNames=branchName;
		// System.out.println("branchNames--------->"+branchNames);
		 String rootMenus=rootMenu;
		// System.out.println("rootMenus--------->"+rootMenus);
		 String user_ids=user_id;
		// System.out.println("user_ids--------->"+user_ids);
		 String activates=activate;
		// System.out.println("activates--------->"+activates);
		 return (ArrayList<Object>) select.query("SELECT itemdescription.menucode, itemdescription.help_id,IFNULL(e.rel_lang_label_name, itemdescription.itemdescription) as itemdescription,IFNULL(f.rel_lang_label_name,itemdescription.condition_check) as condition_check,itemdescription.check_cond,itemdescription.action_name,itemdescription.root_menu FROM live_saas_design.itemdescription inner join live_saas_trans.userdetails b inner join live_saas_trans.usertab c on b.user_id = c.userid and itemdescription.menucode=c.itemcode inner join live_saas_trans.user_access_rights d left join form_lang_label e on b.language_code = e.language_code and upper(itemdescription.itemdescription) =  upper(e.label_name) left join form_lang_label f on b.language_code = f.language_code and upper(itemdescription.condition_check) =  upper(f.label_name) WHERE itemdescription.itemdescription NOT IN  ('Report', 'New', 'Search', 'Edit', 'ACE', 'ACATNOT', 'ACPNOT', 'ACAFDNOT', 'ACABDNOT', 'AAPPAPNOT', 'ACATDNOT', 'AUTOINCNOT', 'AMLIMITNOT', 'DashBoard', 'ALL') and itemdescription.level =? and b.login_id =? and d.branch_name=? and itemdescription.root_menu =? and d.user_id=? and d.activate=?  ORDER BY itemdescription.`position` ASC, itemdescription.menucode ASC",
				 new Object[] {levels,userNames,branchNames,rootMenus,user_ids,activates},

	        		new DynamicPageMapperONe());
	}


	@Override
	public ArrayList<Object> selectDynamicTwo(String level, String userName, String branchName, String rootMenu,
			String user_id, String activate) {
		JdbcTemplate select = new JdbcTemplate(dataSource);
		 String levels=level;
		 System.out.println("levels--------->"+levels);
		 String userNames=userName;
		// System.out.println("usernames--------->"+userNames);
		 String branchNames=branchName;
		// System.out.println("branchNames--------->"+branchNames);
		 String rootMenus=rootMenu;
		 System.out.println("rootMenus--------->"+rootMenus);
		 String user_ids=user_id;
		// System.out.println("user_ids--------->"+user_ids);
		 String activates=activate;
		// System.out.println("activates--------->"+activates);
		 return (ArrayList<Object>) select.query("SELECT itemdescription.menucode, itemdescription.help_id,IFNULL(e.rel_lang_label_name, itemdescription.itemdescription) as itemdescription,IFNULL(f.rel_lang_label_name,itemdescription.condition_check) as condition_check,itemdescription.check_cond,itemdescription.action_name,itemdescription.root_menu,itemdescription.redirect_url FROM live_saas_design.itemdescription inner join live_saas_trans.userdetails b inner join live_saas_trans.usertab c on b.user_id = c.userid and itemdescription.menucode=c.itemcode inner join live_saas_trans.user_access_rights d left join form_lang_label e on b.language_code = e.language_code and upper(itemdescription.itemdescription) =  upper(e.label_name) left join form_lang_label f on b.language_code = f.language_code and upper(itemdescription.condition_check) =  upper(f.label_name) WHERE itemdescription.itemdescription NOT IN  ('Report', 'New', 'Search', 'Edit', 'ACE', 'ACATNOT', 'ACPNOT', 'ACAFDNOT', 'ACABDNOT', 'AAPPAPNOT', 'ACATDNOT', 'AUTOINCNOT', 'AMLIMITNOT', 'DashBoard', 'ALL') and itemdescription.level =? and b.login_id =? and d.branch_name=? and itemdescription.root_menu =? and d.user_id=? and d.activate=?  ORDER BY itemdescription.`position` ASC, itemdescription.menucode ASC",
				 new Object[] {levels,userNames,branchNames,rootMenus,user_ids,activates},

	        		new DynamicPageMapperTwo());
	}

}
