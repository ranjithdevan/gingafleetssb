package com.mainmethod.springboot.gingafleets.dynamic;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.mainmethod.springboot.gingafleets.model.ItemDescription;

public class PersonResultSetExtractorDynamicRowOne implements ResultSetExtractor {
	//ArrayList<String> lisst=new ArrayList<>();
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		
		ItemDescription cust= new ItemDescription();  
		cust.setMenucode(rs.getInt(1));
		cust.setHelp_id(rs.getString(2));
		cust.setItemdescription(rs.getString(3));
		cust.setCondition_check(rs.getString(4));
		cust.setCheck_cond(rs.getString(5));
		cust.setAction_name(rs.getString(6));
		cust.setRoot_menu(rs.getString(7));
		//System.out.println("---------------->rs.getSring(2)"+rs.getString(2));
		//System.out.println("---------------->rs.getSring(3)"+rs.getString(3));
		//System.out.println("---------------->rs.getSring(4)"+rs.getString(4));
		//System.out.println("---------------->rs.getSring(5)"+rs.getString(5));
		//System.out.println("---------------->rs.getSring(6)"+rs.getString(6));
	//	System.out.println("---------------->root menu form query---->"+rs.getString(7));
		
	//	lisst.add(cust);
		return cust;
	}
}