package com.mainmethod.springboot.gingafleets.dynamic;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;

public class DynamicPageMapperTwo implements RowMapper {
	@Override
    public Object mapRow(ResultSet rs, int line) throws SQLException {
        PersonResultSetExtractorDynamicRowTwo extractor = new PersonResultSetExtractorDynamicRowTwo();
        return extractor.extractData(rs);
    }

}
