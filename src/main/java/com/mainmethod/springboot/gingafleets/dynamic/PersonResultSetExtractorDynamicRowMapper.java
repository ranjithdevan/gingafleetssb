package com.mainmethod.springboot.gingafleets.dynamic;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.mainmethod.springboot.gingafleets.dto.Customer;
import com.mainmethod.springboot.gingafleets.model.ItemDescription;

public class PersonResultSetExtractorDynamicRowMapper implements ResultSetExtractor {
	//ArrayList<String> lisst=new ArrayList<>();
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		//System.out.println("checking the value coming");
		ItemDescription cust= new ItemDescription();  
		//System.out.println("checking the value coming 1st line");
		cust.setMenucode(rs.getInt(1));
		//System.out.println("checking the value coming 2nd line");
		cust.setHelp_id(rs.getString(2));
		cust.setItemdescription(rs.getString(3));
		cust.setCondition_check(rs.getString(4));
		cust.setCheck_cond(rs.getString(5));
		cust.setAction_name(rs.getString(6));
		cust.setRoot_menu(rs.getString(7));
		//System.out.println("---------------->rs.getSring(2)"+rs.getString(2));
		//System.out.println("---------------->rs.getSring(3)"+rs.getString(3));
		//System.out.println("---------------->rs.getSring(4)"+rs.getString(4));
		//System.out.println("---------------->rs.getSring(5)"+rs.getString(5));
		//System.out.println("---------------->rs.getSring(6)"+rs.getString(6));
		//System.out.println("---------------->rs.getSring(7)"+rs.getString(7));
		
	//	lisst.add(cust);
		return cust;
	}
	
}
