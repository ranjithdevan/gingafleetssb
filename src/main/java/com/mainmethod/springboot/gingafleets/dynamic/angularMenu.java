package com.mainmethod.springboot.gingafleets.dynamic;

import java.awt.MenuItem;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.sql.DataSource;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class angularMenu {


	public  LinkedHashMap menuItem;
	public  LinkedHashMap childMenuItem;
	public LandingDynamicPageImpl landingDynamicPageImpl = new LandingDynamicPageImpl();
	public  String userName;
	public  String branchName;
	public static String routString;
	public ArrayList<Object> menuBuild(ArrayList<Object> masterMenu,String userNameParam,String branchNameParam,DataSource dataSource2 ) {
		//System.out.println("putside Looop@@@@@@");
		 ArrayList<Object> menuArray = new ArrayList<Object>();
		userName = userNameParam;
		branchName = branchNameParam;
		ObjectMapper i1 = new ObjectMapper(); 
		masterMenu.stream().forEach(item->{
			//System.out.println("inside Loop"+item);
			JsonNode itemObject = i1.valueToTree(item);
			menuItem = new LinkedHashMap<>();
			menuItem.put("displayName", itemObject.get("itemdescription"));
			menuItem.put("iconName", "");
			routString = itemObject.get("itemdescription").toString();
			landingDynamicPageImpl.setDataSource(dataSource2);
				ArrayList<Object> childObject1 = landingDynamicPageImpl.selectDynamicONeAndTwo("1",userName ,branchName,itemObject.get("help_id").toString().replaceAll("\"", ""),"A000198E001","Yes");
				//System.out.println("root menu"+itemObject.get("root_menu"));
				ArrayList<Object> children = new  ArrayList<Object>();
				children = buildChild(childObject1,children);
				menuItem.put("children", children);
			menuArray.add(menuItem);
		});
		return menuArray;
		
	}
	public ArrayList<Object> buildChild(ArrayList<Object> childObject1,ArrayList<Object> childern){
		ArrayList<Object> childObject2 = new ArrayList<Object>();
		ObjectMapper i1 = new ObjectMapper(); 
		LinkedHashMap childItem;
		LinkedHashMap subChildItem;
		ArrayList<Object> subChildren ;
		for(int i=0;i<childObject1.size();i++) {
			JsonNode itemObject = i1.valueToTree(childObject1.get(i));
			//System.out.println("getLink"+itemObject.get("itemdescription"));
			childItem = new LinkedHashMap<>();
			childItem.put("displayName", itemObject.get("itemdescription"));
			childItem.put("iconName", "");
			System.out.println("child--->passing value MostrootMenu"+itemObject.get("root_menu"));
			//System.out.println("userbame"+userName);
			//System.out.println("branchname"+branchName);
				 childObject2 = landingDynamicPageImpl.selectDynamicTwo("2","cement5@gmail.com" ,"C001",itemObject.get("help_id").toString().replaceAll("\"", ""),"A000198E001","Yes");
				//children = new  ArrayList<Object>();
				//System.out.println("childMosObj"+childObject2);
				 subChildren = new ArrayList<Object>();
				for(int j=0;j<childObject2.size();j++) {
					JsonNode itemObject123 = i1.valueToTree(childObject2.get(j));
					//System.out.println("childMost"+itemObject123.get("itemdescription"));
					subChildItem = new LinkedHashMap<>();
					subChildItem.put("displayName", itemObject123.get("itemdescription"));
					subChildItem.put("iconName", "");
					subChildItem.put("route", routString.contains("Reports")?"/dynamicReport":"/dynamic");
					subChildItem.put("click", itemObject123.get("redirect_url"));
					subChildren.add(subChildItem);
				}
				if(subChildren.size()==0) {
				//	if(!itemObject.get("redirect_url").equals(null)) {
						System.out.println("item redirect---->"+itemObject.get("redirect_url"));
				//	childItem.put("click", itemObject.get("redirect_url"));
					//}
				}
				else {
					childItem.put("children", subChildren);
				}
			childern.add(childItem);
		}
		return childern;
	}
}
