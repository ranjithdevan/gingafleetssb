package com.mainmethod.springboot.gingafleets.dynamic;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;

public class DynamicPageMapperONe implements RowMapper {
	@Override
    public Object mapRow(ResultSet rs, int line) throws SQLException {
        PersonResultSetExtractorDynamicRowOne extractor = new PersonResultSetExtractorDynamicRowOne();
        return extractor.extractData(rs);
    }

}
