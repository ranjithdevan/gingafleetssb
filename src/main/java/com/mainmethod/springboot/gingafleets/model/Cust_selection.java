package com.mainmethod.springboot.gingafleets.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cust_selection")
public class Cust_selection extends BaseEntity {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	
	@Column(name="group_name")
	private String group_name;
	
	@Column(name="sub_group")
	private String sub_group;
	
	
	@Column(name="svalue")
	private String svalue;
	
	@Column(name="type")
	private String type;
	
	@Column(name="selection_code")
	private String selection_code;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getGroup_name() {
		return group_name;
	}

	public void setGroup_name(String group_name) {
		this.group_name = group_name;
	}

	public String getSvalue() {
		return svalue;
	}

	public void setSvalue(String svalue) {
		this.svalue = svalue;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSelection_code() {
		return selection_code;
	}

	public void setSelection_code(String selection_code) {
		this.selection_code = selection_code;
	}

	public String getSub_group() {
		return sub_group;
	}

	public void setSub_group(String sub_group) {
		this.sub_group = sub_group;
	}

	@Override
	public String toString() {
		return "Cust_selection [id=" + id + ", group_name=" + group_name + ", svalue=" + svalue + ", type=" + type
				+ ", selection_code=" + selection_code + "]";
	}
	
	
	

}
