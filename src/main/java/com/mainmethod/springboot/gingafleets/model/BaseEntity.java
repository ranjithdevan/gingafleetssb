package com.mainmethod.springboot.gingafleets.model;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;



@MappedSuperclass
public class BaseEntity {

	@Column(name="employee_code")
	private String employee_code;
	@Column(name="sys_date")
	private String sys_date;
	@Column(name="company_code")
	private String company_code;
	@Column(name="center_code")
	private String center_code;
	
	
	
	
	
	
	
	
	
	public String getEmployee_code() {
		return employee_code;
	}
	public void setEmployee_code(String employee_code) {
		this.employee_code = employee_code;
	}
	public String getSys_date() {
		return sys_date;
	}
	public void setSys_date(String sys_date) {
		this.sys_date = sys_date;
	}
	public String getCompany_code() {
		return company_code;
	}
	public void setCompany_code(String company_code) {
		this.company_code = company_code;
	}
	public String getCenter_code() {
		return center_code;
	}
	public void setCenter_code(String center_code) {
		this.center_code = center_code;
	}
	
	
}
