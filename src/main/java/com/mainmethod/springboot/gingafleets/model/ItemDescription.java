package com.mainmethod.springboot.gingafleets.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "itemdescription")
public class ItemDescription {
	
	@Id
	@Column(name="menucode")
	private int menucode;
	
	@Column(name="help_id")
	private String help_id;
	
	@Column(name="itemdescription")
	private String itemdescription;
	
	@Column(name="condition_check")
	private String condition_check;
	
	@Column(name="check_cond")
	private String check_cond;
	
	@Column(name="action_name")
	private String action_name;
	
	@Column(name="root_menu")
	private String root_menu;
	
	@Column(name="redirect_url")
	private String	redirect_url;

	public int getMenucode() {
		return menucode;
	}

	public void setMenucode(int menucode) {
		this.menucode = menucode;
	}

	public String getHelp_id() {
		return help_id;
	}

	public void setHelp_id(String help_id) {
		this.help_id = help_id;
	}

	public String getItemdescription() {
		return itemdescription;
	}

	public void setItemdescription(String itemdescription) {
		this.itemdescription = itemdescription;
	}

	public String getCondition_check() {
		return condition_check;
	}

	public void setCondition_check(String condition_check) {
		this.condition_check = condition_check;
	}

	public String getCheck_cond() {
		return check_cond;
	}

	public void setCheck_cond(String check_cond) {
		this.check_cond = check_cond;
	}

	public String getAction_name() {
		return action_name;
	}

	public void setAction_name(String action_name) {
		this.action_name = action_name;
	}

	public String getRoot_menu() {
		return root_menu;
	}

	public void setRoot_menu(String root_menu) {
		this.root_menu = root_menu;
	}
	
	
	public String getRedirect_url() {
		return redirect_url;
	}

	public void setRedirect_url(String redirect_url) {
		this.redirect_url = redirect_url;
	}
	
	

}
