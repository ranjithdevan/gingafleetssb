package com.mainmethod.springboot.gingafleets.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Vehicle_maintain_lines")
public class Vehicle_maintain_lines extends BaseEntity {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
@Column(name="vehicle_id")	
private String vehicle_id;


	

@Column(name="vehicle_no")
private String 	vehicle_no;
@Column(name="vehicle_type")
private String 	vehicle_type;
@Column(name="sub_act_group")
private String 	sub_act_group;
@Column(name="sub_act_name")
private String 	sub_act_name;
@Column(name="period_kms")
private String 	period_kms;
@Column(name="sub_act_code")
private String sub_act_code;
@Column(name="period_days")
private String period_days;
@Column(name="remind_kms")
private String 	remind_kms;
@Column(name="remind_days")
private String 	remind_days;
@Column(name="date")
private String 	 date;
@Column(name="cur_kms")
private String 	cur_kms;
@Column(name="EXP_date")
private String 	EXP_date;
@Column(name="temp_From_date")
private String 	temp_From_date;
@Column(name="temp_exp_date")
private String 	temp_exp_date;
@Column(name="gatepass_inno")
private String 	gatepass_inno;
@Column(name="jobcard_no")
private String 	jobcard_no;
@Column(name="serial_no")
private String 	serial_no;
@Column(name="typeof_service")
private String typeof_service;
@Column(name="kilometer")
private String kilometer;
@Column(name="problem")
private String problem;
@Column(name="rectified")
private String rectified;
@Column(name="remarks")
private String remarks;

@Column(name="temp_cur_kms")
private String temp_cur_kms;
@Column(name="add_amt")
private String 	add_amt;

public Vehicle_maintain_lines() {
	super();
	// TODO Auto-generated constructor stub
}

public Vehicle_maintain_lines(int id, String vehicle_id, String vehicle_no, String vehicle_type, String sub_act_group,
		String sub_act_name, String period_kms, String sub_act_code, String period_days, String remind_kms,
		String remind_days, String date, String cur_kms, String eXP_date, String temp_From_date, String temp_exp_date,
		String gatepass_inno, String jobcard_no, String serial_no, String typeof_service, String kilometer,
		String problem, String rectified, String remarks, String temp_cur_kms, String add_amt) {
	super();
	this.id = id;
	this.vehicle_id = vehicle_id;
	this.vehicle_no = vehicle_no;
	this.vehicle_type = vehicle_type;
	this.sub_act_group = sub_act_group;
	this.sub_act_name = sub_act_name;
	this.period_kms = period_kms;
	this.sub_act_code = sub_act_code;
	this.period_days = period_days;
	this.remind_kms = remind_kms;
	this.remind_days = remind_days;
	this.date = date;
	this.cur_kms = cur_kms;
	EXP_date = eXP_date;
	this.temp_From_date = temp_From_date;
	this.temp_exp_date = temp_exp_date;
	this.gatepass_inno = gatepass_inno;
	this.jobcard_no = jobcard_no;
	this.serial_no = serial_no;
	this.typeof_service = typeof_service;
	this.kilometer = kilometer;
	this.problem = problem;
	this.rectified = rectified;
	this.remarks = remarks;
	this.temp_cur_kms = temp_cur_kms;
	this.add_amt = add_amt;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}

/*public String getVehicle_id() {
	return vehicle_id;
}
public void setVehicle_id(String vehicle_id) {
	this.vehicle_id = vehicle_id;
}*/
public String getVehicle_no() {
	return vehicle_no;
}
public void setVehicle_no(String vehicle_no) {
	this.vehicle_no = vehicle_no;
}
public String getVehicle_type() {
	return vehicle_type;
}
public void setVehicle_type(String vehicle_type) {
	this.vehicle_type = vehicle_type;
}
public String getSub_act_group() {
	return sub_act_group;
}
public void setSub_act_group(String sub_act_group) {
	this.sub_act_group = sub_act_group;
}
public String getSub_act_name() {
	return sub_act_name;
}
public void setSub_act_name(String sub_act_name) {
	this.sub_act_name = sub_act_name;
}
public String getPeriod_kms() {
	return period_kms;
}
public void setPeriod_kms(String period_kms) {
	this.period_kms = period_kms;
}
public String getSub_act_code() {
	return sub_act_code;
}
public void setSub_act_code(String sub_act_code) {
	this.sub_act_code = sub_act_code;
}
public String getPeriod_days() {
	return period_days;
}
public void setPeriod_days(String period_days) {
	this.period_days = period_days;
}
public String getRemind_kms() {
	return remind_kms;
}
public void setRemind_kms(String remind_kms) {
	this.remind_kms = remind_kms;
}
public String getRemind_days() {
	return remind_days;
}
public void setRemind_days(String remind_days) {
	this.remind_days = remind_days;
}
public String getDate() {
	return date;
}
public void setDate(String date) {
	this.date = date;
}
public String getCur_kms() {
	return cur_kms;
}
public void setCur_kms(String cur_kms) {
	this.cur_kms = cur_kms;
}
public String getEXP_date() {
	return EXP_date;
}
public void setEXP_date(String eXP_date) {
	EXP_date = eXP_date;
}
public String getTemp_From_date() {
	return temp_From_date;
}
public void setTemp_From_date(String temp_From_date) {
	this.temp_From_date = temp_From_date;
}
public String getTemp_exp_date() {
	return temp_exp_date;
}
public void setTemp_exp_date(String temp_exp_date) {
	this.temp_exp_date = temp_exp_date;
}
public String getGatepass_inno() {
	return gatepass_inno;
}
public void setGatepass_inno(String gatepass_inno) {
	this.gatepass_inno = gatepass_inno;
}
public String getJobcard_no() {
	return jobcard_no;
}
public void setJobcard_no(String jobcard_no) {
	this.jobcard_no = jobcard_no;
}
public String getSerial_no() {
	return serial_no;
}
public void setSerial_no(String serial_no) {
	this.serial_no = serial_no;
}
public String getTemp_cur_kms() {
	return temp_cur_kms;
}
public void setTemp_cur_kms(String temp_cur_kms) {
	this.temp_cur_kms = temp_cur_kms;
}
public String getAdd_amt() {
	return add_amt;
}
public void setAdd_amt(String add_amt) {
	this.add_amt = add_amt;
}
public String getTypeof_service() {
	return typeof_service;
}
public void setTypeof_service(String typeof_service) {
	this.typeof_service = typeof_service;
}
public String getKilometer() {
	return kilometer;
}
public void setKilometer(String kilometer) {
	this.kilometer = kilometer;
}
public String getProblem() {
	return problem;
}
public void setProblem(String problem) {
	this.problem = problem;
}
public String getRectified() {
	return rectified;
}
public void setRectified(String rectified) {
	this.rectified = rectified;
}
public String getRemarks() {
	return remarks;
}
public void setRemarks(String remarks) {
	this.remarks = remarks;
}
/*public List<Vehicle> getVehicle() {
	return vehicle;
}
public void setVehicle(List<Vehicle> vehicle) {
	this.vehicle = vehicle;
}*/
public String getVehicle_id() {
	return vehicle_id;
}
public void setVehicle_id(String vehicle_id) {
	this.vehicle_id = vehicle_id;
}





}
