package com.mainmethod.springboot.gingafleets.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "form_lang_label")
public class Form_lang_label {
	
	@Column(name="label_name")
	private String label_name;
	
	@Column(name="language_code")
	private String language_code;
	
	@Column(name="rel_lang_label_name")
	private String	rel_lang_label_name;
	
	
	
	@Id
	@Column(name="id")
	private int	id;

	public String getLabel_name() {
		return label_name;
	}

	public void setLabel_name(String label_name) {
		this.label_name = label_name;
	}

	public String getLanguage_code() {
		return language_code;
	}

	public void setLanguage_code(String language_code) {
		this.language_code = language_code;
	}

	public String getRel_lang_label_name() {
		return rel_lang_label_name;
	}

	public void setRel_lang_label_name(String rel_lang_label_name) {
		this.rel_lang_label_name = rel_lang_label_name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	
	

}
