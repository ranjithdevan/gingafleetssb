package com.mainmethod.springboot.gingafleets.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Load_manifest")
public class Load_manifest extends BaseEntity{
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@Column(name="manifest_code")
	private String	manifest_code;
	@Column(name="branch_code")
	private String	branch_code;
	@Column(name="delivery_branch_code")
	private String	delivery_branch_code;
	@Column(name="manifest_alias")
	private String manifest_alias;
	@Column(name="manifest_date")
	private String manifest_date;
	@Column(name="manifest_time")
	private String manifest_time;
	@Column(name="vehicle_id")
	private String vehicle_id;
	@Column(name="driv_code")
	private String driv_code;
	@Column(name="manifest_cong_type")
	private String manifest_cong_type;
	@Column(name="manifest_inc_head_code")
	private String manifest_inc_head_code;
	@Column(name="manifest_cost_center_code")
	private String manifest_cost_center_code;
	@Column(name="customer_code")
	private String customer_code;
	@Column(name="customer_name")
	private String customer_name;
	
	@Column(name="customer_adds")
	private String customer_adds;
	@Column(name="loc_from")
	private String	loc_from;
	@Column(name="consignor_code")
	private String	consignor_code;
	@Column(name="consignor_name")
	private String	consignor_name;
	@Column(name="consignor_adds")
	private String	consignor_adds;
	@Column(name="loc_to")
	private String	loc_to;
	@Column(name="consignee_code")
	private String	consignee_code;
	@Column(name="consignee_name")
	private String	consignee_name;
	@Column(name="material_name")
	private String	material_name;
	@Column(name="quantity")
	private String	quantity;
	@Column(name="packing_type")
	private String	packing_type;
	@Column(name="freight_amt")
	private String	freight_amt;
	@Column(name="ser_tax")
	private String	ser_tax;
	@Column(name="adv_receive")
	private String	adv_receive;
	@Column(name="bal_freight")
	private String	bal_freight;
	@Column(name="load_charge")
	private String	load_charge;
	@Column(name="other_charge")
	private String	other_charge;
	@Column(name="lr_no")
	private String	lr_no;
	@Column(name="remarks")
	private String	remarks;
	@Column(name="employee_id")
	private String	employee_id;
	@Column(name="manifest_type")
	private String	manifest_type;
	@Column(name="vehilce_type")
	private String	vehilce_type;
	@Column(name="movement_no")
	private String	movement_no;
	@Column(name="vehicle_no")
	private String	vehicle_no;
	@Column(name="route_no")
	private String	route_no;
	@Column(name="calc_type")
	private String	calc_type;
	@Column(name="delivery_add1")
	private String	delivery_add1;
	@Column(name="delivery_add2")
	private String	delivery_add2;
	@Column(name="no_of_articles")
	private String	no_of_articles;
	@Column(name="values_good")
	private String	values_good;
	@Column(name="driver_code_frt")
	private String	driver_code_frt;
	@Column(name="driver_name_frt")
	private String	driver_name_frt;
	@Column(name="driver_code_sec")
	private String	driver_code_sec;
	@Column(name="driver_name_sec")
	private String	driver_name_sec;
	@Column(name="cleaner_name")
	private String	cleaner_name;
	@Column(name="licences_no")
	private String	licences_no;
	@Column(name="contact_no")
	private String	contact_no;
	@Column(name="way_type")
	private String	way_type;
	@Column(name="actual")
	private String	actual;
	@Column(name="fuel_con")
	private String	fuel_con;
	@Column(name="extra")
	private String	extra;
	@Column(name="total")
	private String	total;
	@Column(name="rate_ton")
	private String	rate_ton;
	@Column(name="add_frieght")
	private String	add_frieght;
	@Column(name="crossing")
	private String	crossing;
	@Column(name="total_hire")
	private String	total_hire;
	@Column(name="mrk_freight")
	private String	mrk_freight;
	@Column(name="adv_freight")
	private String	adv_freight;
	@Column(name="hire_crossing")
	private String	hire_crossing;
	@Column(name="cust_invoice_no")
	private String	cust_invoice_no;
	@Column(name="cust_invoice_date")
	private String	cust_invoice_date;
	@Column(name="self_ref_code")
	private String	self_ref_code;
	@Column(name="flag")
	private String	flag;
	@Column(name="gc_no")
	private String	gc_no;
	@Column(name="stionary_charge")
	private String	stionary_charge;
	
	@Column(name="manifest_status")
	private String	manifest_status;
	@Column(name="cash_head")
	private String	cash_head;
	@Column(name="cash_head_code")
	private String	cash_head_code;
	@Column(name="advance_code")
	private String	advance_code;
	@Column(name="advance_no")
	private String	advance_no;
	@Column(name="adv_date")
	private String	adv_date;
	@Column(name="adv_acc_type")
	private String	adv_acc_type;
	@Column(name="cheque_no")
	private String	cheque_no;
	@Column(name="cheque_date")
	private String	cheque_date;
	@Column(name="bank_cash_acc_code")
	private String	bank_cash_acc_code;
	@Column(name="no_of_passeng_pick_time")
	private String	no_of_passeng_pick_time;
	@Column(name="no_of_pass_pick")
	private String	no_of_pass_pick;
	@Column(name="no_of_pass_drop")
	private String	no_of_pass_drop;
	@Column(name="start_km")
	private String	start_km;
	@Column(name="end_km")
	private String	end_km;
	@Column(name="running_km")
	private String	running_km;
	@Column(name="veh_type")
	private String	veh_type;
	@Column(name="route_kms")
	private String	route_kms;
	@Column(name="min_total_hr")
	private String	min_total_hr;
	@Column(name="total_ded")
	private String	total_ded;
	@Column(name="total_bata")
	private String	total_bata;
	@Column(name="total_other")
	private String	total_other;
	@Column(name="net_freight")
	private String	net_freight;
	@Column(name="supplier_code")
	private String	supplier_code;
	@Column(name="supplier_name")
	private String	supplier_name;
	@Column(name="crd_adv_head")
	private String	crd_adv_head;
	@Column(name="deb_adv_head")
	private String	deb_adv_head;
	@Column(name="cr")
	private String	cr;
	@Column(name="dr")
	private String	dr;
	@Column(name="movement")
	private String	movement;
	@Column(name="cust_name")
	private String	cust_name;
	@Column(name="op_type")
	private String	op_type;
	@Column(name="cons_type")
	private String	cons_type;
	@Column(name="fuel_qty")
	private String	fuel_qty;
	@Column(name="activity_code")
	private String	activity_code;
	@Column(name="activity_name")
	private String	activity_name;
	@Column(name="kms")
	private String	kms;
	@Column(name="dc_no")
	private String dc_no;
	@Column(name="ma_no")
	private String	ma_no;
	@Column(name="be_no")
	private String	be_no;
	@Column(name="tripmani_no")
	private String	tripmani_no;
	@Column(name="tripmani_code")
	private String	tripmani_code;
	@Column(name="mov_type")
	private String	mov_type;
	@Column(name="route_type")
	private String	route_type;
	@Column(name="st_cft")
	private String	st_cft;
	@Column(name="st_ton")
	private String st_ton;
	@Column(name="movement_code")
	private String movement_code;
	@Column(name="movement_alias_no")
	private String	movement_alias_no;
	@Column(name="cheq_date")
	private String	cheq_date;
	@Column(name="cheq_num")
	private String	cheq_num;
	@Column(name="trip_status")
	private String	trip_status;
	@Column(name="route")
	private String route;
	@Column(name="sec_driver_name_frt")
	private String sec_driver_name_frt;
	@Column(name="sec_driver_code_frt")
	private String sec_driver_code_frt;
	@Column(name="sec_contact_no")
	private String sec_contact_no;
	@Column(name="sec_licences_no")
	private String sec_licences_no;
	@Column(name="trip_type")
	private String trip_type;
	@Column(name="Road")
	private String	Road;
	@Column(name="employee_sec_name")
	private String	employee_sec_name;
	@Column(name="driver_sec_code")
	private String	driver_sec_code;
	@Column(name="trans_no")
	private String	trans_no;
	@Column(name="cons_cst")
	private String	cons_cst;
	@Column(name="lr_person")
	private String lr_person;
	@Column(name="cust_invoice_value")
	private String cust_invoice_value;
	@Column(name="eway_bill")
	private String eway_bill;
	@Column(name="debit_inc_head_code")
    private String debit_inc_head_code;
	@Column(name="manifest_code_center")
    private String manifest_code_center;
	@Column(name="no_of_pkgs")
    private String no_of_pkgs;
	@Column(name="location_from")
    private String location_from;
	@Column(name="loaction_to")
    private String loaction_to;
	@Column(name="acc_details")
    private String acc_details;
	@Column(name="eway_bill_date")
    private String eway_bill_date;
	@Column(name="approval_status")
    private String approval_status;
	@Column(name="voucher_code")
    private String voucher_code;
	@Column(name="gatepass_approval")
    private String gatepass_approval;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getManifest_code() {
		return manifest_code;
	}
	public void setManifest_code(String manifest_code) {
		this.manifest_code = manifest_code;
	}
	public String getBranch_code() {
		return branch_code;
	}
	public void setBranch_code(String branch_code) {
		this.branch_code = branch_code;
	}
	public String getDelivery_branch_code() {
		return delivery_branch_code;
	}
	public void setDelivery_branch_code(String delivery_branch_code) {
		this.delivery_branch_code = delivery_branch_code;
	}
	
	public String getManifest_alias() {
		return manifest_alias;
	}
	public void setManifest_alias(String manifest_alias) {
		this.manifest_alias = manifest_alias;
	}
	public String getManifest_date() {
		return manifest_date;
	}
	public void setManifest_date(String manifest_date) {
		this.manifest_date = manifest_date;
	}
	public String getManifest_time() {
		return manifest_time;
	}
	public void setManifest_time(String manifest_time) {
		this.manifest_time = manifest_time;
	}
	public String getVehicle_id() {
		return vehicle_id;
	}
	public void setVehicle_id(String vehicle_id) {
		this.vehicle_id = vehicle_id;
	}
	public String getDriv_code() {
		return driv_code;
	}
	public void setDriv_code(String driv_code) {
		this.driv_code = driv_code;
	}
	public String getManifest_cong_type() {
		return manifest_cong_type;
	}
	public void setManifest_cong_type(String manifest_cong_type) {
		this.manifest_cong_type = manifest_cong_type;
	}
	public String getManifest_inc_head_code() {
		return manifest_inc_head_code;
	}
	public void setManifest_inc_head_code(String manifest_inc_head_code) {
		this.manifest_inc_head_code = manifest_inc_head_code;
	}
	public String getManifest_cost_center_code() {
		return manifest_cost_center_code;
	}
	public void setManifest_cost_center_code(String manifest_cost_center_code) {
		this.manifest_cost_center_code = manifest_cost_center_code;
	}
	public String getCustomer_code() {
		return customer_code;
	}
	public void setCustomer_code(String customer_code) {
		this.customer_code = customer_code;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getCustomer_adds() {
		return customer_adds;
	}
	public void setCustomer_adds(String customer_adds) {
		this.customer_adds = customer_adds;
	}
	public String getLoc_from() {
		return loc_from;
	}
	public void setLoc_from(String loc_from) {
		this.loc_from = loc_from;
	}
	public String getConsignor_code() {
		return consignor_code;
	}
	public void setConsignor_code(String consignor_code) {
		this.consignor_code = consignor_code;
	}
	public String getConsignor_name() {
		return consignor_name;
	}
	public void setConsignor_name(String consignor_name) {
		this.consignor_name = consignor_name;
	}
	public String getConsignor_adds() {
		return consignor_adds;
	}
	public void setConsignor_adds(String consignor_adds) {
		this.consignor_adds = consignor_adds;
	}
	public String getMaterial_name() {
		return material_name;
	}
	public void setMaterial_name(String material_name) {
		this.material_name = material_name;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getPacking_type() {
		return packing_type;
	}
	public void setPacking_type(String packing_type) {
		this.packing_type = packing_type;
	}
	public String getFreight_amt() {
		return freight_amt;
	}
	public void setFreight_amt(String freight_amt) {
		this.freight_amt = freight_amt;
	}
	public String getSer_tax() {
		return ser_tax;
	}
	public void setSer_tax(String ser_tax) {
		this.ser_tax = ser_tax;
	}
	public String getAdv_receive() {
		return adv_receive;
	}
	public void setAdv_receive(String adv_receive) {
		this.adv_receive = adv_receive;
	}
	public String getBal_freight() {
		return bal_freight;
	}
	public void setBal_freight(String bal_freight) {
		this.bal_freight = bal_freight;
	}
	public String getLoad_charge() {
		return load_charge;
	}
	public void setLoad_charge(String load_charge) {
		this.load_charge = load_charge;
	}
	public String getOther_charge() {
		return other_charge;
	}
	public void setOther_charge(String other_charge) {
		this.other_charge = other_charge;
	}
	public String getLr_no() {
		return lr_no;
	}
	public void setLr_no(String lr_no) {
		this.lr_no = lr_no;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getEmployee_id() {
		return employee_id;
	}
	public void setEmployee_id(String employee_id) {
		this.employee_id = employee_id;
	}
	public String getManifest_type() {
		return manifest_type;
	}
	public void setManifest_type(String manifest_type) {
		this.manifest_type = manifest_type;
	}
	public String getVehilce_type() {
		return vehilce_type;
	}
	public void setVehilce_type(String vehilce_type) {
		this.vehilce_type = vehilce_type;
	}
	public String getMovement_no() {
		return movement_no;
	}
	public void setMovement_no(String movement_no) {
		this.movement_no = movement_no;
	}
	public String getRoute_no() {
		return route_no;
	}
	public void setRoute_no(String route_no) {
		this.route_no = route_no;
	}
	public String getCalc_type() {
		return calc_type;
	}
	public void setCalc_type(String calc_type) {
		this.calc_type = calc_type;
	}
	public String getDelivery_add1() {
		return delivery_add1;
	}
	public void setDelivery_add1(String delivery_add1) {
		this.delivery_add1 = delivery_add1;
	}
	public String getDelivery_add2() {
		return delivery_add2;
	}
	public void setDelivery_add2(String delivery_add2) {
		this.delivery_add2 = delivery_add2;
	}
	public String getNo_of_articles() {
		return no_of_articles;
	}
	public void setNo_of_articles(String no_of_articles) {
		this.no_of_articles = no_of_articles;
	}
	public String getValues_good() {
		return values_good;
	}
	public void setValues_good(String values_good) {
		this.values_good = values_good;
	}
	public String getDriver_code_frt() {
		return driver_code_frt;
	}
	public void setDriver_code_frt(String driver_code_frt) {
		this.driver_code_frt = driver_code_frt;
	}
	public String getDriver_name_frt() {
		return driver_name_frt;
	}
	public void setDriver_name_frt(String driver_name_frt) {
		this.driver_name_frt = driver_name_frt;
	}
	public String getDriver_code_sec() {
		return driver_code_sec;
	}
	public void setDriver_code_sec(String driver_code_sec) {
		this.driver_code_sec = driver_code_sec;
	}
	public String getDriver_name_sec() {
		return driver_name_sec;
	}
	public void setDriver_name_sec(String driver_name_sec) {
		this.driver_name_sec = driver_name_sec;
	}
	public String getCleaner_name() {
		return cleaner_name;
	}
	public void setCleaner_name(String cleaner_name) {
		this.cleaner_name = cleaner_name;
	}
	public String getLicences_no() {
		return licences_no;
	}
	public void setLicences_no(String licences_no) {
		this.licences_no = licences_no;
	}
	public String getContact_no() {
		return contact_no;
	}
	public void setContact_no(String contact_no) {
		this.contact_no = contact_no;
	}
	public String getWay_type() {
		return way_type;
	}
	public void setWay_type(String way_type) {
		this.way_type = way_type;
	}
	public String getActual() {
		return actual;
	}
	public void setActual(String actual) {
		this.actual = actual;
	}
	public String getFuel_con() {
		return fuel_con;
	}
	public void setFuel_con(String fuel_con) {
		this.fuel_con = fuel_con;
	}
	public String getExtra() {
		return extra;
	}
	public void setExtra(String extra) {
		this.extra = extra;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public String getRate_ton() {
		return rate_ton;
	}
	public void setRate_ton(String rate_ton) {
		this.rate_ton = rate_ton;
	}
	public String getAdd_frieght() {
		return add_frieght;
	}
	public void setAdd_frieght(String add_frieght) {
		this.add_frieght = add_frieght;
	}
	public String getCrossing() {
		return crossing;
	}
	public void setCrossing(String crossing) {
		this.crossing = crossing;
	}
	public String getTotal_hire() {
		return total_hire;
	}
	public void setTotal_hire(String total_hire) {
		this.total_hire = total_hire;
	}
	public String getMrk_freight() {
		return mrk_freight;
	}
	public void setMrk_freight(String mrk_freight) {
		this.mrk_freight = mrk_freight;
	}
	public String getAdv_freight() {
		return adv_freight;
	}
	public void setAdv_freight(String adv_freight) {
		this.adv_freight = adv_freight;
	}
	public String getHire_crossing() {
		return hire_crossing;
	}
	public void setHire_crossing(String hire_crossing) {
		this.hire_crossing = hire_crossing;
	}
	public String getCust_invoice_no() {
		return cust_invoice_no;
	}
	public void setCust_invoice_no(String cust_invoice_no) {
		this.cust_invoice_no = cust_invoice_no;
	}
	public String getCust_invoice_date() {
		return cust_invoice_date;
	}
	public void setCust_invoice_date(String cust_invoice_date) {
		this.cust_invoice_date = cust_invoice_date;
	}
	public String getSelf_ref_code() {
		return self_ref_code;
	}
	public void setSelf_ref_code(String self_ref_code) {
		this.self_ref_code = self_ref_code;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getGc_no() {
		return gc_no;
	}
	public void setGc_no(String gc_no) {
		this.gc_no = gc_no;
	}
	public String getStionary_charge() {
		return stionary_charge;
	}
	public void setStionary_charge(String stionary_charge) {
		this.stionary_charge = stionary_charge;
	}
	public String getManifest_status() {
		return manifest_status;
	}
	public void setManifest_status(String manifest_status) {
		this.manifest_status = manifest_status;
	}
	public String getCash_head() {
		return cash_head;
	}
	public void setCash_head(String cash_head) {
		this.cash_head = cash_head;
	}
	public String getCash_head_code() {
		return cash_head_code;
	}
	public void setCash_head_code(String cash_head_code) {
		this.cash_head_code = cash_head_code;
	}
	public String getAdvance_code() {
		return advance_code;
	}
	public void setAdvance_code(String advance_code) {
		this.advance_code = advance_code;
	}
	public String getAdvance_no() {
		return advance_no;
	}
	public void setAdvance_no(String advance_no) {
		this.advance_no = advance_no;
	}
	public String getAdv_date() {
		return adv_date;
	}
	public void setAdv_date(String adv_date) {
		this.adv_date = adv_date;
	}
	public String getAdv_acc_type() {
		return adv_acc_type;
	}
	public void setAdv_acc_type(String adv_acc_type) {
		this.adv_acc_type = adv_acc_type;
	}
	public String getCheque_no() {
		return cheque_no;
	}
	public void setCheque_no(String cheque_no) {
		this.cheque_no = cheque_no;
	}
	public String getCheque_date() {
		return cheque_date;
	}
	public void setCheque_date(String cheque_date) {
		this.cheque_date = cheque_date;
	}
	public String getBank_cash_acc_code() {
		return bank_cash_acc_code;
	}
	public void setBank_cash_acc_code(String bank_cash_acc_code) {
		this.bank_cash_acc_code = bank_cash_acc_code;
	}
	public String getNo_of_passeng_pick_time() {
		return no_of_passeng_pick_time;
	}
	public void setNo_of_passeng_pick_time(String no_of_passeng_pick_time) {
		this.no_of_passeng_pick_time = no_of_passeng_pick_time;
	}
	public String getNo_of_pass_pick() {
		return no_of_pass_pick;
	}
	public void setNo_of_pass_pick(String no_of_pass_pick) {
		this.no_of_pass_pick = no_of_pass_pick;
	}
	public String getNo_of_pass_drop() {
		return no_of_pass_drop;
	}
	public void setNo_of_pass_drop(String no_of_pass_drop) {
		this.no_of_pass_drop = no_of_pass_drop;
	}
	public String getStart_km() {
		return start_km;
	}
	public void setStart_km(String start_km) {
		this.start_km = start_km;
	}
	public String getEnd_km() {
		return end_km;
	}
	public void setEnd_km(String end_km) {
		this.end_km = end_km;
	}
	public String getRunning_km() {
		return running_km;
	}
	public void setRunning_km(String running_km) {
		this.running_km = running_km;
	}
	public String getVeh_type() {
		return veh_type;
	}
	public void setVeh_type(String veh_type) {
		this.veh_type = veh_type;
	}
	public String getRoute_kms() {
		return route_kms;
	}
	public void setRoute_kms(String route_kms) {
		this.route_kms = route_kms;
	}
	public String getMin_total_hr() {
		return min_total_hr;
	}
	public void setMin_total_hr(String min_total_hr) {
		this.min_total_hr = min_total_hr;
	}
	public String getTotal_ded() {
		return total_ded;
	}
	public void setTotal_ded(String total_ded) {
		this.total_ded = total_ded;
	}
	public String getTotal_bata() {
		return total_bata;
	}
	public void setTotal_bata(String total_bata) {
		this.total_bata = total_bata;
	}
	public String getTotal_other() {
		return total_other;
	}
	public void setTotal_other(String total_other) {
		this.total_other = total_other;
	}
	public String getNet_freight() {
		return net_freight;
	}
	public void setNet_freight(String net_freight) {
		this.net_freight = net_freight;
	}
	public String getSupplier_code() {
		return supplier_code;
	}
	public void setSupplier_code(String supplier_code) {
		this.supplier_code = supplier_code;
	}
	public String getSupplier_name() {
		return supplier_name;
	}
	public void setSupplier_name(String supplier_name) {
		this.supplier_name = supplier_name;
	}
	public String getCrd_adv_head() {
		return crd_adv_head;
	}
	public void setCrd_adv_head(String crd_adv_head) {
		this.crd_adv_head = crd_adv_head;
	}
	public String getDeb_adv_head() {
		return deb_adv_head;
	}
	public void setDeb_adv_head(String deb_adv_head) {
		this.deb_adv_head = deb_adv_head;
	}
	public String getCr() {
		return cr;
	}
	public void setCr(String cr) {
		this.cr = cr;
	}
	public String getDr() {
		return dr;
	}
	public void setDr(String dr) {
		this.dr = dr;
	}
	public String getMovement() {
		return movement;
	}
	public void setMovement(String movement) {
		this.movement = movement;
	}
	public String getCust_name() {
		return cust_name;
	}
	public void setCust_name(String cust_name) {
		this.cust_name = cust_name;
	}
	public String getOp_type() {
		return op_type;
	}
	public void setOp_type(String op_type) {
		this.op_type = op_type;
	}
	public String getCons_type() {
		return cons_type;
	}
	public void setCons_type(String cons_type) {
		this.cons_type = cons_type;
	}
	public String getFuel_qty() {
		return fuel_qty;
	}
	public void setFuel_qty(String fuel_qty) {
		this.fuel_qty = fuel_qty;
	}
	public String getActivity_code() {
		return activity_code;
	}
	public void setActivity_code(String activity_code) {
		this.activity_code = activity_code;
	}
	public String getActivity_name() {
		return activity_name;
	}
	public void setActivity_name(String activity_name) {
		this.activity_name = activity_name;
	}
	public String getKms() {
		return kms;
	}
	public void setKms(String kms) {
		this.kms = kms;
	}
	public String getDc_no() {
		return dc_no;
	}
	public void setDc_no(String dc_no) {
		this.dc_no = dc_no;
	}
	public String getMa_no() {
		return ma_no;
	}
	public void setMa_no(String ma_no) {
		this.ma_no = ma_no;
	}
	public String getBe_no() {
		return be_no;
	}
	public void setBe_no(String be_no) {
		this.be_no = be_no;
	}
	public String getTripmani_no() {
		return tripmani_no;
	}
	public void setTripmani_no(String tripmani_no) {
		this.tripmani_no = tripmani_no;
	}
	public String getTripmani_code() {
		return tripmani_code;
	}
	public void setTripmani_code(String tripmani_code) {
		this.tripmani_code = tripmani_code;
	}
	public String getMov_type() {
		return mov_type;
	}
	public void setMov_type(String mov_type) {
		this.mov_type = mov_type;
	}
	public String getRoute_type() {
		return route_type;
	}
	public void setRoute_type(String route_type) {
		this.route_type = route_type;
	}
	public String getSt_cft() {
		return st_cft;
	}
	public void setSt_cft(String st_cft) {
		this.st_cft = st_cft;
	}
	public String getSt_ton() {
		return st_ton;
	}
	public void setSt_ton(String st_ton) {
		this.st_ton = st_ton;
	}
	public String getMovement_code() {
		return movement_code;
	}
	public void setMovement_code(String movement_code) {
		this.movement_code = movement_code;
	}
	public String getMovement_alias_no() {
		return movement_alias_no;
	}
	public void setMovement_alias_no(String movement_alias_no) {
		this.movement_alias_no = movement_alias_no;
	}
	public String getCheq_date() {
		return cheq_date;
	}
	public void setCheq_date(String cheq_date) {
		this.cheq_date = cheq_date;
	}
	public String getCheq_num() {
		return cheq_num;
	}
	public void setCheq_num(String cheq_num) {
		this.cheq_num = cheq_num;
	}
	public String getTrip_status() {
		return trip_status;
	}
	public void setTrip_status(String trip_status) {
		this.trip_status = trip_status;
	}
	public String getRoad() {
		return Road;
	}
	public void setRoad(String road) {
		Road = road;
	}
	public String getEmployee_sec_name() {
		return employee_sec_name;
	}
	public void setEmployee_sec_name(String employee_sec_name) {
		this.employee_sec_name = employee_sec_name;
	}
	public String getDriver_sec_code() {
		return driver_sec_code;
	}
	public void setDriver_sec_code(String driver_sec_code) {
		this.driver_sec_code = driver_sec_code;
	}
	public String getTrans_no() {
		return trans_no;
	}
	public void setTrans_no(String trans_no) {
		this.trans_no = trans_no;
	}
	public String getCons_cst() {
		return cons_cst;
	}
	public void setCons_cst(String cons_cst) {
		this.cons_cst = cons_cst;
	}
	public String getLr_person() {
		return lr_person;
	}
	public void setLr_person(String lr_person) {
		this.lr_person = lr_person;
	}
	public String getCust_invoice_value() {
		return cust_invoice_value;
	}
	public void setCust_invoice_value(String cust_invoice_value) {
		this.cust_invoice_value = cust_invoice_value;
	}
	public String getEway_bill() {
		return eway_bill;
	}
	public void setEway_bill(String eway_bill) {
		this.eway_bill = eway_bill;
	}
	public String getDebit_inc_head_code() {
		return debit_inc_head_code;
	}
	public void setDebit_inc_head_code(String debit_inc_head_code) {
		this.debit_inc_head_code = debit_inc_head_code;
	}
	public String getManifest_code_center() {
		return manifest_code_center;
	}
	public void setManifest_code_center(String manifest_code_center) {
		this.manifest_code_center = manifest_code_center;
	}
	public String getNo_of_pkgs() {
		return no_of_pkgs;
	}
	public void setNo_of_pkgs(String no_of_pkgs) {
		this.no_of_pkgs = no_of_pkgs;
	}
	public String getLocation_from() {
		return location_from;
	}
	public void setLocation_from(String location_from) {
		this.location_from = location_from;
	}
	public String getLoaction_to() {
		return loaction_to;
	}
	public void setLoaction_to(String loaction_to) {
		this.loaction_to = loaction_to;
	}
	public String getAcc_details() {
		return acc_details;
	}
	public void setAcc_details(String acc_details) {
		this.acc_details = acc_details;
	}
	public String getEway_bill_date() {
		return eway_bill_date;
	}
	public void setEway_bill_date(String eway_bill_date) {
		this.eway_bill_date = eway_bill_date;
	}
	public String getApproval_status() {
		return approval_status;
	}
	public void setApproval_status(String approval_status) {
		this.approval_status = approval_status;
	}
	public String getVoucher_code() {
		return voucher_code;
	}
	public void setVoucher_code(String voucher_code) {
		this.voucher_code = voucher_code;
	}
	public String getGatepass_approval() {
		return gatepass_approval;
	}
	public void setGatepass_approval(String gatepass_approval) {
		this.gatepass_approval = gatepass_approval;
	}
	public String getRoute() {
		return route;
	}
	public void setRoute(String route) {
		this.route = route;
	}
	public String getSec_driver_name_frt() {
		return sec_driver_name_frt;
	}
	public void setSec_driver_name_frt(String sec_driver_name_frt) {
		this.sec_driver_name_frt = sec_driver_name_frt;
	}
	public String getSec_driver_code_frt() {
		return sec_driver_code_frt;
	}
	public void setSec_driver_code_frt(String sec_driver_code_frt) {
		this.sec_driver_code_frt = sec_driver_code_frt;
	}
	public String getSec_contact_no() {
		return sec_contact_no;
	}
	public void setSec_contact_no(String sec_contact_no) {
		this.sec_contact_no = sec_contact_no;
	}
	public String getSec_licences_no() {
		return sec_licences_no;
	}
	public void setSec_licences_no(String sec_licences_no) {
		this.sec_licences_no = sec_licences_no;
	}
	public String getTrip_type() {
		return trip_type;
	}
	public void setTrip_type(String trip_type) {
		this.trip_type = trip_type;
	}
	public String getVehicle_no() {
		return vehicle_no;
	}
	public void setVehicle_no(String vehicle_no) {
		this.vehicle_no = vehicle_no;
	}
	public String getLoc_to() {
		return loc_to;
	}
	public void setLoc_to(String loc_to) {
		this.loc_to = loc_to;
	}
	public String getConsignee_code() {
		return consignee_code;
	}
	public void setConsignee_code(String consignee_code) {
		this.consignee_code = consignee_code;
	}
	public String getConsignee_name() {
		return consignee_name;
	}
	public void setConsignee_name(String consignee_name) {
		this.consignee_name = consignee_name;
	}
	
	

}
