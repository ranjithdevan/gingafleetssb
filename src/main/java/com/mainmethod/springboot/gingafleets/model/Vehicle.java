package com.mainmethod.springboot.gingafleets.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;



@Entity
@Table(name = "vehicle")
public class Vehicle extends BaseEntity {
	
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	
	@Column(name="vehicle_id")
	private String vehicle_id;
    
	/*@OneToMany(fetch=FetchType.LAZY)
	@JoinColumn(name="vehicle_id")
	private List<Vehicle_maintain_lines> vehicle_maintain_lines;*/
	
	@Column(name="vehicle_no")
	private String vehicle_no;
	
	
	@Column(name="alias_name")
	private String alias_name;
	
	@Column(name="kms_run")
	private String kms_run;
	
	@Column(name="start_km")
	private String start_km;
	
	
	@Column(name="make")
	private String make;
	
	
	@Column(name="model")
	private String model;
	
	
	@Column(name="color")
	private String color;
	
	
	@Column(name="vehicle_type")
	private String vehicle_type;
	
	
	@Column(name="vehicle_caegory")
	private String vehicle_caegory;
	
	
	@Column(name="branch")
	private String branch;
	
	
	@Column(name="owner_code")
	private String owner_code;
	
	
	@Column(name="owner_name")
	private String owner_name;
	
	
	@Column(name="driver_code")
	private String driver_code;
	
	
	@Column(name="truck_type")
	private String truck_type;
	
	
	@Column(name="driver_name")
	private String driver_name;
	
	
	@Column(name="co_drivername")
	private String co_drivername;
	
	
	@Column(name="class_veh")
	private String class_veh;
	
	@Column(name="date_purch")
	private String date_purch;
	
	@Column(name="date_reg")
	private String date_reg;
	
	@Column(name="date_sale")
	private String date_sale;
	
	@Column(name="gross_vehiclewt")
	private String gross_vehiclewt;
	
	@Column(name="body_color")
	private String body_color;
	
	@Column(name="size_rearaxle")
	private String size_rearaxle;
	
	@Column(name="size_frontaxle")
	private String size_frontaxle;
	
	@Column(name="fuel_type")
	private String fuel_type;
	
	@Column(name="tank_capacity")
	private String tank_capacity;
	
	
	@Column(name="acc_name")
	private String acc_name;
	
	@Column(name="seat")
	private String seat;
	
	
	@Column(name="unload_wt")
	private String unload_wt;
	
	
	@Column(name="wt_manuf")
	private String wt_manuf;
	
	
	@Column(name="lease_veh")
	private String lease_veh;
	
	@Column(name="engin_no")
	private String engin_no;
	
	@Column(name="chassis_no")
	private String chassis_no;
	
	@Column(name="horse_power")
	private String horse_power;
	
	@Column(name="cubic_cap")
	private String cubic_cap;
	
	
	@Column(name="makers_class")
	private String makers_class;
	
	@Column(name="makers_name")
	private String makers_name;
	
	
	@Column(name="month_menuf")
	private String month_menuf;
	
	
	@Column(name="migrate_state")
	private String migrate_state;
	
	
	@Column(name="type_body")
	private String type_body;
	
	
	@Column(name="no_cyclinder")
	private String no_cyclinder;
	
	
	
	@Column(name="remark")
	private String remark;
	
	
	@Column(name="veh_serialno")
	private String veh_serialno;
	
	
	
	@Column(name="container_dimen")
	private String container_dimen;
	
	
	@Column(name="dedica_sect")
	private String dedica_sect;
	
	
	@Column(name="veh_ref_code")
	private String veh_ref_code;
	
	@Column(name="center_name")
	private String center_name;
	
	
	@Column(name="address1")
	private String address1;
	

	@Column(name="address2")
	private String address2;
	
	
	
	@Column(name="trailer_type")
	private String trailer_type;
	
	
	@Column(name="trailer_no")
	private String trailer_no;
	
	
	@Column(name="vehmain_but")
	private String vehmain_but;
	
	
	@Column(name="self_ref_code")
	private String self_ref_code;
	
	
	
	@Column(name="licen_product_code")
	private String licen_product_code;
	
	
	
	@Column(name="licen_exp_date")
	private String licen_exp_date;
	
	
	
	@Column(name="warranty_NO")
	private String warranty_NO;
	
	
	@Column(name="Expi_Date")
	private String Expi_Date;
	
	
	@Column(name="Expi_Kms")
	private String Expi_Kms;
	
	@Column(name="emi_from_date")
	private String emi_from_date;
	
	
	@Column(name="emi_to_date")
	private String emi_to_date;
	
	
	@Column(name="emi_amount")
	private String emi_amount;
	
	
	@Column(name="vtsunituname")
	private String vtsunituname;
	
	
	
	@Column(name="vtsunitcode")
	private String vtsunitcode;
	
	
	
	@Column(name="speed_status")
	private String speed_status;
	
	
	@Column(name="mileage")
	private String mileage;
	
	
	@Column(name="manufacturer_model")
	private String manufacturer_model;
	
	
	@Column(name="veh_costing")
	private String veh_costing;
	
	
	
	@Column(name="no_years")
	private String no_years;
	
	
	@Column(name="inv_no")
	private String inv_no;
	
	
	@Column(name="inv_date")
	private String inv_date;
	
	
	@Column(name="acc_vendor")
	private String acc_vendor;
	
	
	@Column(name="inv_val")
	private String inv_val;
	
	
	@Column(name="acc_vendorname")
	private String acc_vendorname;
	
	
	@Column(name="fuel_card_no")
	private String fuel_card_no;
	
	
	@Column(name="fuel_card_vendor")
	private String fuel_card_vendor;
	
	
	
	
	@Column(name="fuel_card_vend_code")
	private String fuel_card_vend_code;
	
	
	
	@Column(name="center_id")
	private String center_id;
	
	@Column(name="vehicle_status")
	private String vehicle_status;
	
	@Column(name="gp_status")
	private String gp_status;
	
	/*@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	@JoinColumn(name = "vehicle_id")
	@JsonManagedReference
	private List<Vehicle_maintain_lines> vehicleMaintainLines=new ArrayList<>(0);*/
	
	@OneToMany(targetEntity=Vehicle_maintain_lines.class,cascade = CascadeType.ALL)
	private List<Vehicle_maintain_lines> vehicleMaintainLines =new ArrayList<>(0);
	


	public String getVehicle_id() {
		return vehicle_id;
	}


	public void setVehicle_id(String vehicle_id) {
		this.vehicle_id = vehicle_id;
	}


	public List<Vehicle_maintain_lines> getVehicleMaintainLines() {
		return vehicleMaintainLines;
	}


	public void setVehicleMaintainLines(List<Vehicle_maintain_lines> vehicleMaintainLines) {
		this.vehicleMaintainLines = vehicleMaintainLines;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	/*public String getVehicle_id() {
		return vehicle_id;
	}


	public void setVehicle_id(String vehicle_id) {
		this.vehicle_id = vehicle_id;
	}*/


	public String getVehicle_no() {
		return vehicle_no;
	}


	public void setVehicle_no(String vehicle_no) {
		this.vehicle_no = vehicle_no;
	}


	public String getAlias_name() {
		return alias_name;
	}


	public void setAlias_name(String alias_name) {
		this.alias_name = alias_name;
	}


	public String getKms_run() {
		return kms_run;
	}


	public void setKms_run(String kms_run) {
		this.kms_run = kms_run;
	}


	public String getStart_km() {
		return start_km;
	}


	public void setStart_km(String start_km) {
		this.start_km = start_km;
	}


	public String getMake() {
		return make;
	}


	public void setMake(String make) {
		this.make = make;
	}


	public String getModel() {
		return model;
	}


	public void setModel(String model) {
		this.model = model;
	}


	public String getColor() {
		return color;
	}


	public void setColor(String color) {
		this.color = color;
	}


	public String getVehicle_type() {
		return vehicle_type;
	}


	public void setVehicle_type(String vehicle_type) {
		this.vehicle_type = vehicle_type;
	}


	public String getVehicle_caegory() {
		return vehicle_caegory;
	}


	public void setVehicle_caegory(String vehicle_caegory) {
		this.vehicle_caegory = vehicle_caegory;
	}


	public String getBranch() {
		return branch;
	}


	public void setBranch(String branch) {
		this.branch = branch;
	}


	public String getOwner_code() {
		return owner_code;
	}


	public void setOwner_code(String owner_code) {
		this.owner_code = owner_code;
	}


	public String getOwner_name() {
		return owner_name;
	}


	public void setOwner_name(String owner_name) {
		this.owner_name = owner_name;
	}


	public String getDriver_code() {
		return driver_code;
	}


	public void setDriver_code(String driver_code) {
		this.driver_code = driver_code;
	}


	public String getTruck_type() {
		return truck_type;
	}


	public void setTruck_type(String truck_type) {
		this.truck_type = truck_type;
	}


	public String getDriver_name() {
		return driver_name;
	}


	public void setDriver_name(String driver_name) {
		this.driver_name = driver_name;
	}


	public String getCo_drivername() {
		return co_drivername;
	}


	public void setCo_drivername(String co_drivername) {
		this.co_drivername = co_drivername;
	}


	public String getClass_veh() {
		return class_veh;
	}


	public void setClass_veh(String class_veh) {
		this.class_veh = class_veh;
	}


	public String getDate_purch() {
		return date_purch;
	}


	public void setDate_purch(String date_purch) {
		this.date_purch = date_purch;
	}


	public String getDate_reg() {
		return date_reg;
	}


	public void setDate_reg(String date_reg) {
		this.date_reg = date_reg;
	}


	public String getDate_sale() {
		return date_sale;
	}


	public void setDate_sale(String date_sale) {
		this.date_sale = date_sale;
	}


	public String getGross_vehiclewt() {
		return gross_vehiclewt;
	}


	public void setGross_vehiclewt(String gross_vehiclewt) {
		this.gross_vehiclewt = gross_vehiclewt;
	}


	public String getBody_color() {
		return body_color;
	}


	public void setBody_color(String body_color) {
		this.body_color = body_color;
	}


	public String getSize_rearaxle() {
		return size_rearaxle;
	}


	public void setSize_rearaxle(String size_rearaxle) {
		this.size_rearaxle = size_rearaxle;
	}


	public String getSize_frontaxle() {
		return size_frontaxle;
	}


	public void setSize_frontaxle(String size_frontaxle) {
		this.size_frontaxle = size_frontaxle;
	}


	public String getFuel_type() {
		return fuel_type;
	}


	public void setFuel_type(String fuel_type) {
		this.fuel_type = fuel_type;
	}


	public String getTank_capacity() {
		return tank_capacity;
	}


	public void setTank_capacity(String tank_capacity) {
		this.tank_capacity = tank_capacity;
	}


	public String getAcc_name() {
		return acc_name;
	}


	public void setAcc_name(String acc_name) {
		this.acc_name = acc_name;
	}


	public String getSeat() {
		return seat;
	}


	public void setSeat(String seat) {
		this.seat = seat;
	}


	public String getUnload_wt() {
		return unload_wt;
	}


	public void setUnload_wt(String unload_wt) {
		this.unload_wt = unload_wt;
	}


	public String getWt_manuf() {
		return wt_manuf;
	}


	public void setWt_manuf(String wt_manuf) {
		this.wt_manuf = wt_manuf;
	}


	public String getLease_veh() {
		return lease_veh;
	}


	public void setLease_veh(String lease_veh) {
		this.lease_veh = lease_veh;
	}


	public String getEngin_no() {
		return engin_no;
	}


	public void setEngin_no(String engin_no) {
		this.engin_no = engin_no;
	}


	public String getChassis_no() {
		return chassis_no;
	}


	public void setChassis_no(String chassis_no) {
		this.chassis_no = chassis_no;
	}


	public String getHorse_power() {
		return horse_power;
	}


	public void setHorse_power(String horse_power) {
		this.horse_power = horse_power;
	}


	public String getCubic_cap() {
		return cubic_cap;
	}


	public void setCubic_cap(String cubic_cap) {
		this.cubic_cap = cubic_cap;
	}


	public String getMakers_class() {
		return makers_class;
	}


	public void setMakers_class(String makers_class) {
		this.makers_class = makers_class;
	}


	public String getMakers_name() {
		return makers_name;
	}


	public void setMakers_name(String makers_name) {
		this.makers_name = makers_name;
	}


	public String getMonth_menuf() {
		return month_menuf;
	}


	public void setMonth_menuf(String month_menuf) {
		this.month_menuf = month_menuf;
	}


	public String getMigrate_state() {
		return migrate_state;
	}


	public void setMigrate_state(String migrate_state) {
		this.migrate_state = migrate_state;
	}


	public String getType_body() {
		return type_body;
	}


	public void setType_body(String type_body) {
		this.type_body = type_body;
	}


	public String getNo_cyclinder() {
		return no_cyclinder;
	}


	public void setNo_cyclinder(String no_cyclinder) {
		this.no_cyclinder = no_cyclinder;
	}


	public String getRemark() {
		return remark;
	}


	public void setRemark(String remark) {
		this.remark = remark;
	}


	public String getVeh_serialno() {
		return veh_serialno;
	}


	public void setVeh_serialno(String veh_serialno) {
		this.veh_serialno = veh_serialno;
	}


	public String getContainer_dimen() {
		return container_dimen;
	}


	public void setContainer_dimen(String container_dimen) {
		this.container_dimen = container_dimen;
	}


	public String getDedica_sect() {
		return dedica_sect;
	}


	public void setDedica_sect(String dedica_sect) {
		this.dedica_sect = dedica_sect;
	}


	public String getVeh_ref_code() {
		return veh_ref_code;
	}


	public void setVeh_ref_code(String veh_ref_code) {
		this.veh_ref_code = veh_ref_code;
	}


	public String getCenter_name() {
		return center_name;
	}


	public void setCenter_name(String center_name) {
		this.center_name = center_name;
	}


	public String getAddress1() {
		return address1;
	}


	public void setAddress1(String address1) {
		this.address1 = address1;
	}


	public String getAddress2() {
		return address2;
	}


	public void setAddress2(String address2) {
		this.address2 = address2;
	}


	public String getTrailer_type() {
		return trailer_type;
	}


	public void setTrailer_type(String trailer_type) {
		this.trailer_type = trailer_type;
	}


	public String getTrailer_no() {
		return trailer_no;
	}


	public void setTrailer_no(String trailer_no) {
		this.trailer_no = trailer_no;
	}


	public String getVehmain_but() {
		return vehmain_but;
	}


	public void setVehmain_but(String vehmain_but) {
		this.vehmain_but = vehmain_but;
	}


	public String getSelf_ref_code() {
		return self_ref_code;
	}


	public void setSelf_ref_code(String self_ref_code) {
		this.self_ref_code = self_ref_code;
	}


	public String getLicen_product_code() {
		return licen_product_code;
	}


	public void setLicen_product_code(String licen_product_code) {
		this.licen_product_code = licen_product_code;
	}


	public String getLicen_exp_date() {
		return licen_exp_date;
	}


	public void setLicen_exp_date(String licen_exp_date) {
		this.licen_exp_date = licen_exp_date;
	}


	public String getWarranty_NO() {
		return warranty_NO;
	}


	public void setWarranty_NO(String warranty_NO) {
		this.warranty_NO = warranty_NO;
	}


	public String getExpi_Date() {
		return Expi_Date;
	}


	public void setExpi_Date(String expi_Date) {
		Expi_Date = expi_Date;
	}


	public String getExpi_Kms() {
		return Expi_Kms;
	}


	public void setExpi_Kms(String expi_Kms) {
		Expi_Kms = expi_Kms;
	}


	public String getEmi_from_date() {
		return emi_from_date;
	}


	public void setEmi_from_date(String emi_from_date) {
		this.emi_from_date = emi_from_date;
	}


	public String getEmi_to_date() {
		return emi_to_date;
	}


	public void setEmi_to_date(String emi_to_date) {
		this.emi_to_date = emi_to_date;
	}


	public String getEmi_amount() {
		return emi_amount;
	}


	public void setEmi_amount(String emi_amount) {
		this.emi_amount = emi_amount;
	}


	public String getVtsunituname() {
		return vtsunituname;
	}


	public void setVtsunituname(String vtsunituname) {
		this.vtsunituname = vtsunituname;
	}


	public String getVtsunitcode() {
		return vtsunitcode;
	}


	public void setVtsunitcode(String vtsunitcode) {
		this.vtsunitcode = vtsunitcode;
	}


	public String getSpeed_status() {
		return speed_status;
	}


	public void setSpeed_status(String speed_status) {
		this.speed_status = speed_status;
	}


	public String getMileage() {
		return mileage;
	}


	public void setMileage(String mileage) {
		this.mileage = mileage;
	}


	public String getManufacturer_model() {
		return manufacturer_model;
	}


	public void setManufacturer_model(String manufacturer_model) {
		this.manufacturer_model = manufacturer_model;
	}


	public String getVeh_costing() {
		return veh_costing;
	}


	public void setVeh_costing(String veh_costing) {
		this.veh_costing = veh_costing;
	}


	public String getNo_years() {
		return no_years;
	}


	public void setNo_years(String no_years) {
		this.no_years = no_years;
	}


	public String getInv_no() {
		return inv_no;
	}


	public void setInv_no(String inv_no) {
		this.inv_no = inv_no;
	}


	public String getInv_date() {
		return inv_date;
	}


	public void setInv_date(String inv_date) {
		this.inv_date = inv_date;
	}


	public String getAcc_vendor() {
		return acc_vendor;
	}


	public void setAcc_vendor(String acc_vendor) {
		this.acc_vendor = acc_vendor;
	}


	public String getInv_val() {
		return inv_val;
	}


	public void setInv_val(String inv_val) {
		this.inv_val = inv_val;
	}


	public String getAcc_vendorname() {
		return acc_vendorname;
	}


	public void setAcc_vendorname(String acc_vendorname) {
		this.acc_vendorname = acc_vendorname;
	}


	public String getFuel_card_no() {
		return fuel_card_no;
	}


	public void setFuel_card_no(String fuel_card_no) {
		this.fuel_card_no = fuel_card_no;
	}


	public String getFuel_card_vendor() {
		return fuel_card_vendor;
	}


	public void setFuel_card_vendor(String fuel_card_vendor) {
		this.fuel_card_vendor = fuel_card_vendor;
	}


	public String getFuel_card_vend_code() {
		return fuel_card_vend_code;
	}


	public void setFuel_card_vend_code(String fuel_card_vend_code) {
		this.fuel_card_vend_code = fuel_card_vend_code;
	}


	public String getCenter_id() {
		return center_id;
	}


	public void setCenter_id(String center_id) {
		this.center_id = center_id;
	}


	public String getImport_method() {
		return import_method;
	}


	public void setImport_method(String import_method) {
		this.import_method = import_method;
	}


	public String getEi_token_code() {
		return ei_token_code;
	}


	public void setEi_token_code(String ei_token_code) {
		this.ei_token_code = ei_token_code;
	}


	@Column(name="import_method")
	private String import_method;
	
	
	@Column(name="ei_token_code")
	private String ei_token_code;


	public String getVehicle_status() {
		return vehicle_status;
	}


	public void setVehicle_status(String vehicle_status) {
		this.vehicle_status = vehicle_status;
	}


	public String getGp_status() {
		return gp_status;
	}


	public void setGp_status(String gp_status) {
		this.gp_status = gp_status;
	}


	
	
	

}
