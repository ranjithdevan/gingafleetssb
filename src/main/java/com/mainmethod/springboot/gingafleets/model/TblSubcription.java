package com.mainmethod.springboot.gingafleets.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tblsubcription")
public class TblSubcription  {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name="pack_id")
	private String pack_id;
	
	@Column(name="Menu_id")
	private String Menu_id;
	
	@Column(name="Cust_Code")
	private String Cust_Code;
	
	@Column(name="Operation_Id")
	private String Operation_Id;
	
	@Column(name="TripSheetcount")
	private String TripSheetcount;
	
	@Column(name="StartDate")
	private String StartDate;
	
	@Column(name="EnDate")
	private String EnDate;
	
	
	@Column(name="LastRenewalDate")
	private String LastRenewalDate;
	
	@Column(name="Created_By")
	private String Created_By;
	
	@Column(name="Created_Date")
	private String Created_Date;
	
	@Column(name="Active")
	private String Active;
	
	@Column(name="Last_Modified_By")
	private String Last_Modified_By;
	
	@Column(name="Last_Modified_Date")
	private String Last_Modified_Date;
	
	@Column(name="Grace_Period")
	private String Grace_Period;
	
	@Column(name="Gst_NO")
	private String Gst_NO;
	
	@Column(name="coupon_no")
	private String coupon_no;
	
	@Column(name="package_period")
	private String package_period;
	
	@Column(name="branch_count")
	private String branch_count;
	
	@Column(name="user_count")
	private String user_count;
	
	@Column(name="pack_slab_id")
	private String pack_slab_id;

	@Column(name="end_date")
	private String end_date;
	
	//@Column(name="last_renewal_date")
	//private String last_renewal_date;
	
	//@Column(name="start_date")
	//private String start_date;
	
	@Column(name="company_code")
	private String company_code;
	
	
	@Column(name="Center_Code")
	private String Center_Code;


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getPack_id() {
		return pack_id;
	}


	public void setPack_id(String pack_id) {
		this.pack_id = pack_id;
	}


	public String getMenu_id() {
		return Menu_id;
	}


	public void setMenu_id(String menu_id) {
		Menu_id = menu_id;
	}


	public String getCust_Code() {
		return Cust_Code;
	}


	public void setCust_Code(String cust_Code) {
		Cust_Code = cust_Code;
	}


	public String getOperation_Id() {
		return Operation_Id;
	}


	public void setOperation_Id(String operation_Id) {
		Operation_Id = operation_Id;
	}


	public String getTripSheetcount() {
		return TripSheetcount;
	}


	public void setTripSheetcount(String tripSheetcount) {
		TripSheetcount = tripSheetcount;
	}


	public String getStartDate() {
		return StartDate;
	}


	public void setStartDate(String startDate) {
		StartDate = startDate;
	}


	public String getEnDate() {
		return EnDate;
	}


	public void setEnDate(String enDate) {
		EnDate = enDate;
	}


	public String getLastRenewalDate() {
		return LastRenewalDate;
	}


	public void setLastRenewalDate(String lastRenewalDate) {
		LastRenewalDate = lastRenewalDate;
	}


	public String getCreated_By() {
		return Created_By;
	}


	public void setCreated_By(String created_By) {
		Created_By = created_By;
	}


	public String getCreated_Date() {
		return Created_Date;
	}


	public void setCreated_Date(String created_Date) {
		Created_Date = created_Date;
	}


	public String getActive() {
		return Active;
	}


	public void setActive(String active) {
		Active = active;
	}


	public String getLast_Modified_By() {
		return Last_Modified_By;
	}


	public void setLast_Modified_By(String last_Modified_By) {
		Last_Modified_By = last_Modified_By;
	}


	public String getLast_Modified_Date() {
		return Last_Modified_Date;
	}


	public void setLast_Modified_Date(String last_Modified_Date) {
		Last_Modified_Date = last_Modified_Date;
	}


	public String getGrace_Period() {
		return Grace_Period;
	}


	public void setGrace_Period(String grace_Period) {
		Grace_Period = grace_Period;
	}


	public String getGst_NO() {
		return Gst_NO;
	}


	public void setGst_NO(String gst_NO) {
		Gst_NO = gst_NO;
	}


	public String getCoupon_no() {
		return coupon_no;
	}


	public void setCoupon_no(String coupon_no) {
		this.coupon_no = coupon_no;
	}


	public String getPackage_period() {
		return package_period;
	}


	public void setPackage_period(String package_period) {
		this.package_period = package_period;
	}


	public String getBranch_count() {
		return branch_count;
	}


	public void setBranch_count(String branch_count) {
		this.branch_count = branch_count;
	}


	public String getUser_count() {
		return user_count;
	}


	public void setUser_count(String user_count) {
		this.user_count = user_count;
	}


	public String getPack_slab_id() {
		return pack_slab_id;
	}


	public void setPack_slab_id(String pack_slab_id) {
		this.pack_slab_id = pack_slab_id;
	}


	public String getEnd_date() {
		return end_date;
	}


	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}


	/*public String getLast_renewal_date() {
		return last_renewal_date;
	}


	public void setLast_renewal_date(String last_renewal_date) {
		this.last_renewal_date = last_renewal_date;
	}*/


	/*public String getStart_date() {
		return start_date;
	}


	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}*/


	public String getCompany_code() {
		return company_code;
	}


	public void setCompany_code(String company_code) {
		this.company_code = company_code;
	}


	public String getCenter_Code() {
		return Center_Code;
	}


	public void setCenter_Code(String center_Code) {
		Center_Code = center_Code;
	}
	
	
	

}
