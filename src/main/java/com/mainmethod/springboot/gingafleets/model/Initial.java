package com.mainmethod.springboot.gingafleets.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "initial")
public class Initial extends BaseEntity {

	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name="form_name")
	private String form_name;
	
	@Column(name="type")
	private String type;
	
	@Column(name="value")
	private String value;
	
	@Column(name="current_value")
	private String current_value;
	
	@Column(name="module_code")
	private String module_code;
	
	@Column(name="temp_flag")
	private String temp_flag;
	
	@Column(name="remarks")
	private String remarks;
	
	@Column(name="user_prefix")
	private String user_prefix;
	
	@Column(name="no_series_desc")
	private String no_series_desc;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getForm_name() {
		return form_name;
	}

	public void setForm_name(String form_name) {
		this.form_name = form_name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getCurrent_value() {
		return current_value;
	}

	public void setCurrent_value(String current_value) {
		this.current_value = current_value;
	}

	public String getModule_code() {
		return module_code;
	}

	public void setModule_code(String module_code) {
		this.module_code = module_code;
	}

	public String getTemp_flag() {
		return temp_flag;
	}

	public void setTemp_flag(String temp_flag) {
		this.temp_flag = temp_flag;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getUser_prefix() {
		return user_prefix;
	}

	public void setUser_prefix(String user_prefix) {
		this.user_prefix = user_prefix;
	}

	public String getNo_series_desc() {
		return no_series_desc;
	}

	public void setNo_series_desc(String no_series_desc) {
		this.no_series_desc = no_series_desc;
	}

	@Override
	public String toString() {
		return "Initial [id=" + id + ", form_name=" + form_name + ", type=" + type + ", value=" + value
				+ ", current_value=" + current_value + ", module_code=" + module_code + ", temp_flag=" + temp_flag
				+ ", remarks=" + remarks + ", user_prefix=" + user_prefix + ", no_series_desc=" + no_series_desc + "]";
	} 
	
	
}
