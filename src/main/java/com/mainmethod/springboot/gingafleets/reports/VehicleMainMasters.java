package com.mainmethod.springboot.gingafleets.reports;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;

import com.mainmethod.springboot.gingafleets.common.tripsheet.PersonResultSetExtractorDropDownCustomerActivityNameMaster;

public class VehicleMainMasters implements RowMapper {
	
	@Override
    public Object mapRow(ResultSet rs, int line) throws SQLException {
		
        PersonResultSetExtractorDropDownReportsheet extractor = new PersonResultSetExtractorDropDownReportsheet();
        return extractor.extractData(rs);
    }

}
