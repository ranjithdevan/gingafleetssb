package com.mainmethod.springboot.gingafleets.reports;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;

import com.mainmethod.springboot.gingafleets.common.tripsheet.PersonResultSetExtractorDropDownCashBank;

public class CashBankDropDownCustomerGrp implements RowMapper {
	
	@Override
    public Object mapRow(ResultSet rs, int line) throws SQLException {
		
        PersonResultSetExtractorDropDownCashBankGrp extractor = new PersonResultSetExtractorDropDownCashBankGrp();
        return extractor.extractData(rs);
    }

}
