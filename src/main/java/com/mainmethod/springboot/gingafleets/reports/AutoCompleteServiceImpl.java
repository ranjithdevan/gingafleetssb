package com.mainmethod.springboot.gingafleets.reports;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.mainmethod.springboot.gingafleets.common.frontdata.DropDownResultSet;
import com.mainmethod.springboot.gingafleets.common.frontdata.SubgroupDropDown;
import com.mainmethod.springboot.gingafleets.common.tripsheet.ActivityNameDropDownMainMasters;
import com.mainmethod.springboot.gingafleets.common.tripsheet.CashBankDropDownCustomer;
import com.mainmethod.springboot.gingafleets.dto.Acc_master;
import com.mainmethod.springboot.gingafleets.dto.Maintenance;
import com.mainmethod.springboot.gingafleets.dto.Selection;
import com.mainmethod.springboot.gingafleets.model.Load_manifest;

public class AutoCompleteServiceImpl implements AutoCompleteService {
	
private DataSource dataSource;
	
	
	public void setDataSource(DataSource ds) {	
        dataSource = ds;	
    }

	@Override
	public List<Load_manifest> vehicleno(List<String> vehicle) {
		String companycode="";
		String vehicle_no="";
		String vehicle_type="";
		String newReferenceQuery="";
		JdbcTemplate select = new JdbcTemplate(dataSource);
		 String[] obj = null;
		 String toExceute="";
		 String finalQuery="";
		 if(vehicle.size()==3) {
		 companycode=vehicle.get(2);
		 vehicle_no=vehicle.get(0);
		 vehicle_type=vehicle.get(1);
		//JdbcTemplate select = new JdbcTemplate(dataSource);
		if(vehicle_type.equals("")) {
			vehicle_type=" ";
		    }
		if(companycode.equals("")) {
			companycode="";
		}
		  
		vehicle_no=vehicle_no.replaceAll("\\s+", " ");
			 if(vehicle_no.equals(" ")) {
				 vehicle_no="";
			 } 
			 String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no='0201V008222506'", String.class);
			 if(vehicle_no.isEmpty() && companycode !=null && vehicle_type !=null ) {
				 newReferenceQuery=refrenceQuery.replace("'%?%'", "'%%'");
				 finalQuery=newReferenceQuery.replace("'&^'", "?");
				 toExceute=finalQuery.replace("'?%'", "?");
				 obj =  new String[] {companycode,vehicle_type+"%"};
			 }
			 else {
				 newReferenceQuery=refrenceQuery.replace("'%?%'", "?");
			     System.out.println("newReferenceQuery------>"+newReferenceQuery);
			     finalQuery=newReferenceQuery.replace("'&^'", "?");
			     toExceute =finalQuery.replace("?%", "?");
			     System.out.println("finalQuery------>"+toExceute); 
			     obj =  new String[] {companycode,"%"+vehicle_no+"%",vehicle_type+"%"};
			
			 
			 }
			 if((vehicle_no.isEmpty() || companycode !=null  && vehicle_type!=null))  {
				 return  (List<Load_manifest>)  select.query(toExceute,
						 obj,
						 new VehicleMainMasters());
				  }
			 else if	(companycode !=null && vehicle_no !=null && vehicle_type!=null) {
				  return  (List<Load_manifest>)  select.query(toExceute,
							 obj,
							 new VehicleMainMasters());
				  
			  }
		    
		 }
		 return Collections.EMPTY_LIST;
	}

	@Override
	public List<Acc_master> CustomerGrps(List<String> CustomerGrp) {
		String company="";
		String CashBankCustGrop="";
		JdbcTemplate select = new JdbcTemplate(dataSource);
		 String[] obj = null;
		 if(CustomerGrp.size()==2) {
				
			 company=CustomerGrp.get(1);
			 CashBankCustGrop=CustomerGrp.get(0);
		if(company.equals("")) {
			company=" ";
		    }
		CashBankCustGrop=CashBankCustGrop.replaceAll("\\s+", " ");
		 if(CashBankCustGrop.equals(" ")) {
			 CashBankCustGrop="";
		 } 
		 String newReferenceQuery="";
		 String finalQuery="";
		  String refrenceQuery = (String) select.queryForObject("select rule from live_saas_design.reference WHERE Code_no='0201V00597'", String.class);
		  System.out.println("refrenceQuery------->"+refrenceQuery);
		  if(CashBankCustGrop.isEmpty() && company !=null) {
		      newReferenceQuery=refrenceQuery.replace("'?%'", "'%%'");
		     System.out.println("newReferenceQuery------>"+newReferenceQuery);
		     finalQuery=newReferenceQuery.replace("'&^'", "?");
		     System.out.println("finalQuery------>"+finalQuery);
		     obj =  new String[] {company};
		  }
		  else {
			  newReferenceQuery=refrenceQuery.replace("'?%'", "?");
			     System.out.println("newReferenceQuery------>"+newReferenceQuery);
			     finalQuery=newReferenceQuery.replace("'&^'", "?");
			     System.out.println("finalQuery------>"+finalQuery);
			     obj =  new String[] {company,CashBankCustGrop+"%"};
		  }
		  if((CashBankCustGrop.isEmpty() || company !=null))  {
				 return  (ArrayList)  select.query(finalQuery,
						 obj,
						 new CashBankDropDownCustomerGrp());
				  }
				  else if	(company !=null && company !=null) {
					  return  (ArrayList)  select.query(finalQuery,
								 obj,
								 new CashBankDropDownCustomerGrp());
					  
				  }  
		  }
		 return Collections.EMPTY_LIST;
	}
	
}
