package com.mainmethod.springboot.gingafleets.reports;

import java.util.List;

import com.mainmethod.springboot.gingafleets.dto.Acc_master;
import com.mainmethod.springboot.gingafleets.model.Load_manifest;

public interface AutoCompleteService {
	
	
	List<Load_manifest> vehicleno(List<String> vehicle);
	
	List<Acc_master> CustomerGrps(List<String> CustomerGrp);

}
