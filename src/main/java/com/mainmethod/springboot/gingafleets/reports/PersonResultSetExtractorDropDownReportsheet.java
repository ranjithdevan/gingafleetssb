package com.mainmethod.springboot.gingafleets.reports;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.mainmethod.springboot.gingafleets.dto.Maintenance;
import com.mainmethod.springboot.gingafleets.model.Load_manifest;

public class PersonResultSetExtractorDropDownReportsheet implements ResultSetExtractor {
	//List<Load_manifest> maint_acts=new ArrayList<>();
	@Override
	public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
		
		Load_manifest maint_acts_masters=new Load_manifest();
		if((rs.getString(1) !=null && (!rs.getString(1).isEmpty()))){
		maint_acts_masters.setVehicle_no(rs.getString(1));
		System.out.println("rs.get1------>"+rs.getString(1));
		///maint_acts.add(maint_acts_masters);
		}
		return maint_acts_masters;
	}
}