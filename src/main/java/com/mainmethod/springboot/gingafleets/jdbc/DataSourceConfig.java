package com.mainmethod.springboot.gingafleets.jdbc;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;


@Component
@Service
public class DataSourceConfig {
	public DataSource connectDataSource() {
	DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
    dataSource.setUrl("jdbc:mysql://164.52.195.134:3306/live_saas_trans?useSSL=false");
    dataSource.setUsername("tms");
    dataSource.setPassword("5d$Th}q%9KST");
    return dataSource;
	}

}
