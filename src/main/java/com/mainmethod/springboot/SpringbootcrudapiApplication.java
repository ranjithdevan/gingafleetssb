package com.mainmethod.springboot;


import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


//created  main method on 07.01.2021
@SpringBootApplication
public class SpringbootcrudapiApplication extends SpringBootServletInitializer  {

	
	public static void main(String[] args) {
		System.out.println("main method");
		
		SpringApplication.run(SpringbootcrudapiApplication.class, args);
	}
	
	@Override
	 protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	  return application.sources(SpringbootcrudapiApplication.class);
	 }
	

}
